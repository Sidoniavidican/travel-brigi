$( document ).ready(function() {
    var url = window.location.href;

    $('.type>.cs-options>ul>li').each(function(){
        var type = $(this).text();
        type  = type.toLowerCase().slice(1);
        type = type.replace(' ', '-');
        if(url.includes(type))
        {
            var value = $(this).attr('data-value');
            var type = parseInt(value)+2;
            var text = $(".type>.cs-options>ul>li:nth-child("+type+')').text();
            $(".type>.cs-placeholder").text(text);
            $(".type>.cs-options>ul>li:nth-child("+type+')').addClass('cs-selected'); 
        } 

    });

    //add class curent__item_red to Diverse
    var diverse = 0;
    $('.type>.cs-options>ul>li:nth-child(n+10)').each(function(){
        var type = $(this).text();
        type  = type.toLowerCase().slice(1);
        type = type.replace(' ', '-');
        if(url.includes(type)){
            $('.diverse').addClass('curent__item_red');
            diverse = 1;
        } 
        if(diverse == 0)
        {
           $('.diverse').removeClass('curent__item_red');
        }
    });


    //add class curent__item_red to button selected
    for(var i=1; i<8; i++)
    {
        if(url.includes('categorie='+i)){
                $('#button'+i).addClass('curent__item_red');
        }  
    }


   //home 
   $( '#exTab1 > ul > li:nth-child(1)').addClass('active');
   $( '#exTab1 > div > div:nth-child(1)').addClass('active');



   //
})