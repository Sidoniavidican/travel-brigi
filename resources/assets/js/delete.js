$(document).ready(function () {

	//delete photo from offer
    $(".deletePhoto").click(function(e){
		confirmOnDelete();
		var id = $(this).attr('id');
        $.ajax({
              method: "delete",
              url: "/delete-photo/"+id,
              data: {
                'id': $(this).attr('id')  
              },
              headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
              },
              success: function(data){	
                	$('#photo'+id).remove();	
		      }
        });
    });
    //delete photo from album
    $(".deletePhotoAlbum").click(function(e){
		confirmOnDelete();
		var id = $(this).attr('id');
        $.ajax({
              method: "delete",
              url: "/delete-album-photo/"+id,
              data: {
                'id': $(this).attr('id')  
              },
              headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
              },
              success: function(data){	
                	$('#photo'+id).remove();	
		      }
        });
    });

    //delete album
    $(".deleteAlbum").click(function(e){
		confirmOnDelete();
		var id = $(this).attr('id');
        $.ajax({
              method: "delete",
              url: "/admin/albums/"+id,
              data: {
                'id': $(this).attr('id')  
              },
              headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
              },
              success: function(data){	
                	$('#album'+id).remove();	
                	$.notify('Albumul a fost sters cu success!' ,{ position:"bottom center",  className:'success' });  
		      }
        });
    });

    //delete offer
    $(".deleteOffer").click(function(e){
		confirmOnDelete();
		var id = $(this).attr('id');
        $.ajax({
              method: "delete",
              url: "/admin/offers/"+id,
              data: {
                'id': $(this).attr('id')  
              },
              headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
              },
              success: function(data){	
                	$('#offer'+id).remove();
                	$.notify('Oferta a fost stearsa cu success!' ,{ position:"bottom center",  className:'success' });  	
		      }
        });
    });

    //delete transport
    $(".deleteTransport").click(function(e){
		confirmOnDelete();
	    var id = $(this).attr('id');
	    $.ajax({
	        method: "delete",
	        url: "/admin/transports/"+id,
	        data: {
	            'id': $(this).attr('id')  
	        },
	        headers: {
	           'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
	        },
	        success: function(data){	
	            $('#transport'+id).remove();	
	            $.notify('Transportul a fost sters cu success!' ,{ position:"bottom center",  className:'success' });  
			}
	    });
    });

    //delete newsletter
    $(".deleteNewsletter").click(function(e){
		confirmOnDelete();
	    var id = $(this).attr('id');
	    $.ajax({
	        method: "delete",
	        url: "/admin/newsletters/"+id,
	        data: {
	            'id': $(this).attr('id')  
	        },
	        headers: {
	           'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
	        },
	        success: function(data){	
	            $('#newsletter'+id).remove();
	            $.notify('Newsletter-ul a fost sters cu success!' ,{ position:"bottom center",  className:'success' });	
			}
	    });
    });

    //delete hotel
    $(".deleteHotel").click(function(e){
    	confirmOnDelete();
	    var id = $(this).attr('id');
	    $.ajax({
	        method: "delete",
	        url: "/admin/hotels/"+id,
	        data: {
	            'id': $(this).attr('id')  
	        },
	        headers: {
	           'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
	        },
	        success: function(data){	
	            $('#hotel'+id).remove();	
	            $.notify('Hotelul a fost sters cu success!' ,{ position:"bottom center",  className:'success' });  
			}
	    });
    });

    //delete list
    $(".deleteList").click(function(e){
      confirmOnDelete();
      var id = $(this).attr('id');
      $.ajax({
          method: "delete",
          url: "/admin/newsletters-list/"+id,
          data: {
              'id': $(this).attr('id')  
          },
          headers: {
             'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          },
          success: function(data){  
              $('#list'+id).remove();  
              $.notify('Lista a fost sters cu success!' ,{ position:"bottom center",  className:'success' });  
      }
      });
    });

    function confirmOnDelete()
    {
    	if(!confirm('Sunteti sigura ca vreti sa stergeti?'))
    	{
	        e.preventDefault();
	        return false;
	    }
    }

})