circle = document.getElementById("circle");
first = document.getElementById("1");
second = document.getElementById("2");
third = document.getElementById("3");
forth = document.getElementById("4");
fifth = document.getElementById("5");
sixth = document.getElementById("6");
seventh = document.getElementById("7");
brigitte = document.getElementById("brigitte");
travel = document.getElementById("travel");
groupPins = document.getElementById("logo");


function static()
{
	/*tl = new TimelineMax();

	  tl.set([first, second, third, forth, fifth, sixth, seventh], {visibility:"visible"})
	  .from(first, 0.7, {rotation:"+45", x:"+10", y:"-25", transformOrigin:"50% 100% 0", ease:Back.easeOut}, "first")
	  .from(second, 0.7, {rotation:"+90", x:"+10", y:"-75", transformOrigin:"50% 100% 0", ease:Back.easeOut}, "first")
	  .from(third, 0.7, {rotation:"+135", x:"+10", y:"-150", transformOrigin:"50% 100% 0", ease:Back.easeOut}, "first")
	  .from(forth, 0.7, {rotation:"+180", x:"+10", y:"-200", transformOrigin:"50% 100% 0", ease:Back.easeOut}, "first")
	  .from(fifth, 0.7, {rotation:"+225", x:"+10", y:"-150", transformOrigin:"50% 100% 0", ease:Back.easeOut}, "first")
	  .from(sixth, 0.7, {rotation:"+270", x:"+10", y:"-75", transformOrigin:"50% 100% 0", ease:Back.easeOut}, "first")
	  .from(seventh, 0.7, {rotation:"+315", x:"+10", y:"-25", transformOrigin:"50% 100% 0", ease:Back.easeOut}, "first")*/

	tl2 = new TimelineMax();

	tl2.set([circle, first, second, third, forth, fifth, sixth, seventh], {visibility:"visible"})
	  .from(first, 0.7, {rotation:"-45", transformOrigin:"50% 100% 0", ease:Back.easeOut}, "first+=0.5")
	  .from(second, 0.7, {rotation:"-90", transformOrigin:"50% 100% 0", ease:Back.easeOut}, "first+=0.5")
	  .from(third, 0.7, {rotation:"-135", transformOrigin:"50% 100% 0", ease:Back.easeOut}, "first+=0.5")
	  .from(forth, 0.7, {rotation:"-180", transformOrigin:"50% 100% 0", ease:Back.easeOut}, "first+=0.5")
	  .from(fifth, 0.7, {rotation:"-225", transformOrigin:"50% 100% 0", ease:Back.easeOut}, "first+=0.5")
	  .from(sixth, 0.7, {rotation:"-270", transformOrigin:"50% 100% 0", ease:Back.easeOut}, "first+=0.5")
	  .from(seventh, 0.7, {rotation:"-315", transformOrigin:"50% 100% 0", ease:Back.easeOut}, "first+=0.5")
	  .to(circle, 0.7, {opacity:0}, "first+=0.5")

	  .to(first, 0.7, {rotation:"-45", transformOrigin:"50% 100% 0", ease:Back.easeOut}, "first+=2")
	  .to(second, 0.7, {rotation:"-90", transformOrigin:"50% 100% 0", ease:Back.easeOut}, "first+=2")
	  .to(third, 0.7, {rotation:"-135", transformOrigin:"50% 100% 0", ease:Back.easeOut}, "first+=2")
	  .to(forth, 0.7, {rotation:"-180", transformOrigin:"50% 100% 0", ease:Back.easeOut}, "first+=2")
	  .to(fifth, 0.7, {rotation:"-225", transformOrigin:"50% 100% 0", ease:Back.easeOut}, "first+=2")
	  .to(sixth, 0.7, {rotation:"-270", transformOrigin:"50% 100% 0", ease:Back.easeOut}, "first+=2")
	  .to(seventh, 0.7, {rotation:"-315", transformOrigin:"50% 100% 0", ease:Back.easeOut}, "first+=2")
	  .to(circle, 0.7, {opacity:1}, "first+=2.5")
}

static();

