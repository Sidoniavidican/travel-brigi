$( document ).ready(function() {

    if ($(document).find(".advanced").length > 0) 
    {
        $('.cs-options>ul>li').click(function(){  
            GetUrlParameters();
        });

        $('.change').change(function(){  
            GetUrlParameters();
        });

        //to set transport and masa true at search
        var url = window.location.href;
        if(!url.includes('transport='))
        {
            transport = 1;
            masa = 1;
            $('.masa').prop('checked', true);
            $('.transport').prop('checked', true);
        }  

        //get offers on change
        function GetUrlParameters()
        {
            var masa = 0;
            if($('.masa').is(':checked'))
                 masa = 1;
            var transport = 0;

            if($('.transport').is(':checked'))
                var transport = 1;

            var search = $('#search').val();
            var minPret = range.noUiSlider.get()[0];
            if(minPret.includes('k'))
                minPret = minPret.substr(0, minPret.length-1) * 1000;

            var pret = range.noUiSlider.get()[1];
            if(pret.includes('k'))
                pret = pret.substr(0, pret.length-1) * 1000;
            
            var minDurata = range2.noUiSlider.get()[0];
            var durata = range2.noUiSlider.get()[1];
            
            var perioada = 'toate';
            $('.perioada>.cs-options>ul>li').each(function(){
                if($(this).hasClass('cs-selected'))
                {       
                    perioada = $(this).attr('data-value'); 
                }                
            });

            var categorie = 'toate';
            $('.type>.cs-options>ul>li').each(function(){
                if($(this).hasClass('cs-selected'))
                {       
                    categorie = $(this).attr('data-value');
                }                  
            });

            var stars = '';
            $('.stars2').children().each(function() {
                if($(this).hasClass('selected'))
                {
                    stars = stars + $(this).attr('id');
                    stars = stars.replace('s','');
                    stars = stars +','; 
                }
            });
            if(stars == '')
                stars = "toate";
            var url = window.location.href;
            if(categorie != 'toate')
            {
                var category = $('#type'+categorie).text(); 
                category = category.replace(' ','').toLowerCase();
                category = category.replace(' ','-').toLowerCase(); 

                if(url.includes('search'))
                {
                    var url = '/search?search='+search+'&categorie='+categorie+'&pret-maxim='+pret+'&pret-minim='+minPret+'&durata-maxima='+durata+'&durata-minima='+minDurata+ '&perioada='+perioada+'&masa='+masa+'&transport='+transport+'&stele='+stars;         
                }
                else
                {
                    var url = '/'+category+'/filter?filter=&categorie='+categorie+'&pret-maxim='+pret+'&pret-minim='+minPret+'&durata-maxima='+durata+'&durata-minima='+minDurata+ '&perioada='+perioada+'&masa='+masa+'&transport='+transport+'&stele='+stars;         
                }
            }
            else 
                var url = '/search?search='+search+'&categorie='+categorie+'&pret-maxim='+pret+'&pret-minim='+minPret+'&durata-maxima='+durata+'&durata-minima='+minDurata+ '&perioada='+perioada+'&masa='+masa+'&transport='+transport+'&stele='+stars;         
           
            window.location = url; 
        }

        //set advance search filter values
        if(url.includes('perioada'))
        {

            $('.stars').click(function(){  
                if($(this).hasClass('selected'))
                    $(this).removeClass('selected');
                else
                    $(this).addClass('selected');
                GetUrlParameters();
            });

            var value = GetUrl(url, 'masa');
            var masa = value.slice(5); 
            if(masa == 1)
                $('.masa').prop('checked', true);
            else
                $('.masa').prop('checked', false);

            var value = GetUrl(url, 'transport');
            var transport = value.slice(10);
            if(transport == 1)
                $('.transport').prop('checked', true);

            else
                $('.transport').attr('checked', false);

            var value = GetUrl(url, 'categorie');    
            if(!value.includes('toate'))
            {               
                var type = parseInt(value.slice(10))+2;
                var text = $(".type>.cs-options>ul>li:nth-child("+type+')').text();
                $(".type>.cs-placeholder").text(text);
                $(".type>.cs-options>ul>li:nth-child("+type+')').addClass('cs-selected');
            }
          
            var value = GetUrl(url, 'perioada');
            if(!value.includes('toate'))
            {
                var perioada = parseInt(value.slice(9))+2;
                var text = $(".perioada>.cs-options>ul>li:nth-child("+perioada+')').text();
                $(".perioada>.cs-placeholder").text(text);
                $(".perioada>.cs-options>ul>li:nth-child("+perioada+')').addClass('cs-selected');
            }

            var value = GetUrl(url, 'stele');
            var stars = value.slice(6);
            var starsArray = stars.split(',');

            for(var i=0; i<starsArray.length; i++)
            {
                $('#s'+starsArray[i]).addClass('selected');
                $('#s'+starsArray[i]+'>span').removeClass('silver');
            }    
            if(type == 2)
            {
                $(".transport").prop('checked', false);
                $(".transport").attr("disabled", true);
            }
        }

        function GetUrl(url, val)
        {
            var urlValue = url.split('&');
            var result = '';
            for(var i=0; i<urlValue.length; i++)
            {
                var str = urlValue[i];
                if(str.includes(val))
                    result = urlValue[i];
            }
            return result;
        }

        //for price and duration range
        var url = window.location.href;
        var maxPret = parseInt($('#maxPret').val());
        maxPret = Math.ceil(maxPret/1000)*1000;

        if (isNaN(maxPret))
            maxPret = 2000;

        var noUiSlider = require('nouislider');
        var wNumb = require('wNumb');
        var range = document.getElementById('range');

        range.style.height = '10px';
        range.style.width = '300px';
        range.style.margin = '0 auto 10px';

        var url = window.location.href;
        if(url.includes('pret'))
        {
            var value = GetUrl(url, 'pret-minim');
            var minP = value.slice(11);
            value = GetUrl(url, 'pret-maxim');
            var maxP = value.slice(11);
        }
        else
        {
            var minP = 5;
            var maxP = maxPret; 
        }

        noUiSlider.create(range, {
            start: [ minP, maxP], // 4 handles, starting at...
            snap: true,
            connect: true, // Display a colored bar between the handles
            behaviour: 'tap-drag', // Move handle on tap, bar is draggable
            tooltips: true,
            format: wNumb({
                decimals: 0,
                edit: function( value, original ) {
                    if ( value > 999 ) {
                        return  value/1000 +'k';
                    }
                    else 
                        return value;
                }
            }),
            range: {
                'min': 5,
                '5%': 100,  
                '10%': 200,
                '15%': 300,
                '20%': 400,
                '25%': 500,
                '30%': 600,
                '35%': 700,
                '40%': 800,
                '45%': 900,
                '50%': 1000,
                '55%': 1100,
                '60%': 1200,
                '65%': 1300,
                '70%': 1400,
                '75%': 1500,
                '80%': 1600,
                '85%': 1700,
                '90%': 1800,
                '95%': 1900,
                'max': maxPret
            }
        });

        var url = window.location.href;
        if(url.includes('durata'))
        {
            var value = GetUrl(url, 'durata-minima');
            var minD = value.slice(12);
            value = GetUrl(url, 'durata-maxima');
            var maxD = value.slice(12);
        }
        else
        {
            var minD = 1;
            var maxD = 24;  
        }

        var noUiSlider2 = require('nouislider');
        var range2 = document.getElementById('range2');

        range2.style.height = '10px';
        range2.style.width = '200px';
        range2.style.margin = '0 auto 10px';


        noUiSlider2.create(range2, {
            start: [ minD, maxD ], // 4 handles, starting at...
            margin: 1, // Handles must be at least 300 apart
            limit: 23, // ... but no more than 600
            connect: true, // Display a colored bar between the handles
            behaviour: 'tap-drag', // Move handle on tap, bar is draggable
            step: 1,
            tooltips: true,
            format: wNumb({
                decimals: 0
            }),
            range: {
                'min': 1,
                'max': 24
            }
        });

        range.noUiSlider.on('change', function(values){  
            GetUrlParameters();  
        });

        range2.noUiSlider.on('change', function(values){  
            GetUrlParameters();  
        });
    }
});
