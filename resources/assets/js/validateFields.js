$(document).ready(function () {

        $.validator.addMethod("hasType", 
            function(value, element) {                        
          		if($('#type').val()=='1,2' || $('#type').val()=='2,1' || $('#type').val()=='1' || $('#type').val()=='2')
          		{
              		return false;
          		}
          		else 
          			return true;
            }, "You need another type."
        );

       	$.validator.addMethod("compare", 
            function(value, element) {                      
          		if( $('#end_date').val() < $('#begin_date').val() )
          		{
              		return false;
          		}
          		else 
          			return true;
            }, "The date is not bigger that begin_date."
        );

       	$.validator.addMethod("compareLM", 
            function(value, element) {   
            	if( ($('#EB_date').val() != null) && ($('#LM_date').val() != null) && ($('#LM_date').val() < $('#EB_date').val()) )
            	{
                 	return false;
            	}  
              	else 
              		return true;             
            }, "The date is not bigger that early booking date."
        );

  		$('#addOfferValidate').validate({
  			rules:
  			{
  				'type[]': {hasType: true},
  				end_date: {compare: true},
  				LM_date: {compareLM: true}
  			}

        });

        $('#editOfferValidate').validate({ 
        	rules:
        	{
  				'type[]': {hasType: true},
  				end_date: {compare: true},
  				LM_date: {compareLM: true}
  			}
        });

        $('#transportValidate').validate({
        });
		
		    $('#addPeriodValidate').validate({ 
        });
        
        $('#addDaysValidate').validate({ 
        });

		    $('#addAlbumValidate').validate({ 
        });
        
		    $('#editAlbumValidate').validate({ 
        });  

        $('#addHotelValidate').validate({ 
        });     	

			  $('#searchValidate').validate({ 
        });

        $('#addNewsletterValidate').validate({ 
        });
   

			var offerS = $('.offer-success').attr('name');
			if(offerS != null)
      {
       		$.notify(offerS ,{ position:"bottom center",  className:'success' }); 
			}

			//begin date and end date validation
			if($('#begin_date').val() != null)
				$('.dateRangePickerEnd')
					        .datepicker({
					        	format: 'yyyy-mm-dd' ,
					            startDate:  $('#begin_date').val()
					        })        
					        .on('changeDate', function(e) {
			            		$('.dateRangeForm').validate('revalidateField', 'date');
			    });

		  $('#begin_date').on('change', function(){
				$('.dateRangePickerEnd').datepicker("remove");
				$('.dateRangePickerEnd')
					        .datepicker({
					        	format: 'yyyy-mm-dd' ,
					            startDate:  $('#begin_date').val()
					        })        
					        .on('changeDate', function(e) {
			            		$('.dateRangeForm').validate('revalidateField', 'date');
			    });
		})

});