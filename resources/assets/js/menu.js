


 $(document).ready(function () {
    /**
	 * Sticky Header 
	 */
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        $("#wrapper").toggleClass("show-menu");
    }); 

	$(window).scroll(function() { 
	    if ($(this).scrollTop() > 1){  
	        $('#masthead').addClass("sticky");
	    }
	    else{
	        $('#masthead').removeClass("sticky");
	    }
    });
    var item = $('.js-onebyone');

			$.fn.animateIn = function(options) {
			  var self = $(this);
			  
			  var settings = $.extend({
			      cname: 'class',
			  }, options );
			  
			  var action = function() {
			    (function animate(counter) { 
			  
			      setTimeout(function() {

			      self.eq(counter).addClass(settings.cname);

			        counter++;

			        if(counter < self.length) animate(counter);

			      }, 60);

			    })(0);
			  };
			  action();
			};

			item.animateIn({
			  cname: 'show' 
			});

});


      