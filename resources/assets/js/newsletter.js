$( document ).ready(function() {

    function HideShow(){
        if($('#offers-selected').children().length > 0)
            $('.offers-selected').removeClass('hide');
        else 
            $('.offers-selected').addClass('hide');
    }

    function SelectedList(){
        var selected = '';
        $('#offers-selected').children().each(function (){
                  var offerId = $(this).attr('id');
                  offerId = offerId.substring(8);
                  selected = selected + ',' + offerId;      
        })
        $('#selectedList').prop('value', selected);
    }

    $(document).on('click', '.off', function() {  
        var id = $(this).attr('id');
        if ( ! $(this).hasClass('selectedOffer') )
        {
            $(this).addClass('selectedOffer');
            var btn = document.createElement("div");  
            btn.setAttribute('id','selected'+id);   
            btn.setAttribute('class', 'off-selected');  
            btn.setAttribute('type', 'button');         
            getOfferName(id, 'selected'+id);    
            document.getElementById("offers-selected").appendChild(btn);
        }   
        HideShow(); 
        SelectedList();
    });

    $(document).on('click', '.off-selected', function() {  
        var id = $(this).attr('id');
        id = id.substring(8);
        {
            $('#'+id).removeClass('selectedOffer')
            $('#selected'+id).remove(); 
        }
        HideShow();
        SelectedList();
    });

    function getOfferName(id, idSelected) {
        $.ajax({
              method: "post",
              url: "/getOfferName",
              data: {
                'id':id   
              },
              headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
              },
              success: function(data){  
                  document.getElementById(idSelected).innerHTML = data.name;    
              }
        });
    }

    $('#type_id').change(function()
      {
            $('#offer-list').children().remove(); 
            $.ajax({
                  method: "post",
                  url: "/getTypeOffers",
                  data: {
                    'id': $(this).val()  
                  },
                  headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                  },
                  success: function(data){  
                      var  offers = data.offers.split(',');
                      if(data.offers.length > 0)
                      {
                          for(var i = 0; i < offers.length; i++)
                          {      
                                  var btn = document.createElement("button");        
                                  btn.setAttribute('id', offers[i]);   
                                  btn.setAttribute('class', 'off');  
                                  btn.setAttribute('type', 'button'); 

                                  $('#offers-selected').children().each(function (){
                                      var idSelected = $(this).attr('id');
                                      idSelected = idSelected.substring(8);
                                      if(idSelected == offers[i])
                                      {
                                          btn.setAttribute('class', 'off selectedOffer');
                                      }
                                  });     
                                  getOfferName(offers[i], offers[i]);                                
                                  document.getElementById("offer-list").appendChild(btn);                                                           
                          }  
                      }              
                  }
            });
      });
})