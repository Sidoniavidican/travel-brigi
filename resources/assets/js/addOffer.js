
    $(document).ready(function () {
        var submitted = false;

        var url = window.location.href;
        if(url.includes('offers/create'))
        {     
            $(window).on('beforeunload', function(){        
                $.ajax({
                      method: "delete",
                      url: "/delete-offer-photo-temporary",
                      data: { 
                      },
                      headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                      },
                      success: function(data){    
                      }
                });
                return "Any changes will be lost";
             });

            // Form Submit
            $(document).on("submit", "form", function(event){
                $(window).off('beforeunload');
            });
        }

        $('.selectpicker').selectpicker({
            countSelectedText: function (numSelected, numTotal) {
              return (numSelected == 1) ? "{0} selectat" : "{0} selectate ";
          }
        });
 
        $('#type').change(function(){
                Types();
                earlyBookingLastMinute();
        });


        var url = window.location.href;
        if(url.includes('edit') && url.includes('offers'))
        {
            //for early booking and last minute discount
            if ($(document).find("#type").length > 0) 
            {
                var type = $('#type').val(); 
                if(type.includes('1'))
                {
                    $('.discount').removeClass('hide');
                    $('.lastMinute').removeClass('hide');
                }
                if(type.includes('2'))
                {
                    $('.discount').removeClass('hide');
                    $('.earlyBooking').removeClass('hide');
                }   
            }                               
        }
    
        function earlyBookingLastMinute()
        {
           var type = $('#type').val(); 
           if(type.includes('1'))
           {
                $('.discount').removeClass('hide');
                $('.lastMinute').removeClass('hide');

                $('#LM_date').prop('required', 'required');
                $('#LM_discount').prop('required', 'required');
           }
           else
           {
               $('.lastMinute').addClass('hide');
               $('#LM_discount').prop('required', null);
               $('#LM_date').prop('required', null);
           }
           if(type.includes('2'))
           {
                $('.discount').removeClass('hide');
                $('.earlyBooking').removeClass('hide');

                $('#EB_date').prop('required', 'required');
                $('#EB_discount').prop('required', 'required');
           }
           else
           {
               $('.earlyBooking').addClass('hide');       
               $('#EB_discount').prop('required', null);
               $('#EB_date').prop('required', null);
           }

           if(! (type.includes('1') || type.includes('2')))
           {
                $('.discount').addClass('hide');
           }
        }

        function Types()
        {
            var type = $('#type').val(); 
            if(type == '')
            {   
               $('.offers').prop('disabled', false);
               $('.accomodation').prop('disabled', false);
            }
            else if(type == 0)
            {
                $('.offers').prop('disabled', true);        
                $('.offer').hide();

                $('#price').prop('required', null);
                $('#begin_date').prop('required', null);
                $('#end_date').prop('required', null);
                $('#days').prop('required', null);

                $('#price').prop('value', null);
                $('#begin_date').prop('value', null);
                $('#end_date').prop('value', null);
                $('#days').prop('value', null);
            }
            else 
            {      
                $('.accomodation').prop('disabled', true); 
                //
                $('.offer').show();

                $('#price').prop('required', 'required');
                $('#begin_date').prop('required', 'required');
                $('#end_date').prop('required', 'required');
                $('#days').prop('required', 'required');
            }
        } 
  });


    var Dropzone = require("dropzone");

/*    Dropzone.options.update = {
            autoProcessQueue: false,
            uploadMultiple: true,
            parallelUploads: 100,
            maxFiles: 10,
            maxFilesize: 16,
    };*/

    Dropzone.options.upload = {
            autoProcessQueue: true,
            uploadMultiple: false,
            parallelUploads: 1,
            maxFiles: 1,
            maxFilesize: 16,
    };



   