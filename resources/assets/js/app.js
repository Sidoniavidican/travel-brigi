
/**
* First we will load all of this project's JavaScript dependencies which
* includes Vue and other libraries. It is a great starting point when
* building robust, powerful web applications using Vue and Laravel.
*/

require('./bootstrap');

require('./addOffer');
require('./edit-offer');
require('./addAlbum');
require('./addTransport');

require('./menu');
require('./search');
require('./type');
require('./delete');
require('./newsletter');
require('./notify');
require('./instagram');
require('./validateFields');
require('./rangePriceDuration');
//require('semantic-ui/dist/semantic.min');


/**
* Next, we will create a fresh Vue application instance and attach it to
* the page. Then, you may begin adding components to this application
* or customize the JavaScript scaffolding to fit your unique needs.
*/

Vue.component('example', require('./components/Example.vue'));

/*const app = new Vue({
   el: '#app'
});*/

/*Google Maps*/ 

if ( $( '#map' ).length ) {
	    var map = new GMaps( { 
		div: "#map", 
		lat: 47.0532108, 
		lng: 21.9279359,
		zoom: 15,
		zoomControl: false,
		scrollwheel: false,
		navigationControl: false,
		mapTypeControl: false,
		scaleControl: false,
		draggable: true,
		disableDoubleClickZoom: false, 
		disableDefaultUI: false,
		maptype: "ROADMAP",
	} );
	map.addMarker( {
		lat: 47.0532108, 
		lng: 21.9279359
	} ); 
} 

/*Carousel*/ 

    $('#myCarousel').carousel({
        interval: 3000 
    });

    $('[id^=carousel-selector-]').click( function(){
        var id = this.id.substr(this.id.lastIndexOf()); 
        var id = parseInt(id);
        $('#myCarousel').carousel(id);
    });

/*Select*/  
    (function() {
		[].slice.call( document.querySelectorAll( 'select.cs.cs-select' ) ).forEach( function(el) {	
			new SelectFx(el);
		} );
	})(); 

	//subscribe validate
	var _subscribe = $('#subscribe');
	_subscribe.validate( {
		rules: {
		    email: { 
		        required: true,
		        email: true 
		    }
		}
    });
	//


    var _contact_form = $( '#contactform');  
    var _popup_form = $( '#popup');
    var _popup_cerere = $( '#cererepopup'); 

	_contact_form.validate( {
		rules: {
			name: { 
		        required: true,
		    },
		    email: { 
		        required: true,
		        email: true 
		    },
		    phone: {
		        required: true,
		        digits: true
		    },
		    message: {
		        required: true
		    } 
		}
    }); 
    _popup_form.validate( {
		rules: {
			name: { 
		        required: true,
		    }, 
		    email: { 
		        required: true,
		        email: true 
		    },
		    phone: {
		        required: true,
		        digits: true
		    },
		    message: {
		    	required: true
		    }
		}
    });
  _popup_cerere.validate( {
		rules: {
			name: { 
		        required: true,
		    },
		    email: { 
		        required: true,
		        email: true 
		    },
		    phone: {
		        required: true,
		        digits: true
		    },
		    message: {
		    	required: true
		    }
		}
    }); 

$(".right-corder-container-button").hover(function() {
    $(".long-text").addClass("show-long-text");   
}, function () {
    $(".long-text").removeClass("show-long-text");
});

$(document).ready(function() {
    $('.dateRangePicker')
        .datepicker({
            format: 'yyyy-mm-dd' ,
            startDate: '+0d' 
        })
        .on('changeDate', function(e) {
            $('.dateRangeForm').validate('revalidateField', 'date');
        });

});
