
@extends('layouts.app')
@section('title')Tichete Vacanta @stop 
@section('description') Brigitte Travel - Tichete Vacanta!@stop 
@section('content')     
<div class="container">
  <div class="clearfix">
      <div class="col-md-4 padd-7">
      <div class="modal-about" data-toggle="modal" data-target="#myModal1">    
        <img src="/images/edt.jpg" alt="Brevet Turism" class="img-responsive">
      </div>
    </div>
      <div class="col-md-4 padd-7">
      <div class="modal-about" data-toggle="modal" data-target="#myModal2">    
        <img src="/images/cdt.jpg" alt="Licenta Turism" class="img-responsive">
      </div>
    </div>
    <div class="col-md-4 padd-7">
      <div class="modal-about" data-toggle="modal" data-target="#myModal3">    
        <img src="/images/voucher-turis.jpg" alt="certificat" class="img-responsive">
      </div>
    </div>
  </div>
 
  <div id="myModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <img src="/images/edt.jpg" alt="Brevet de turism" class="img-responsive">
      </div>
  </div>
  <div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <img src="/images/cdt.jpg" alt="Licenta de turism" class="img-responsive">
     </div>
  </div>
  <div id="myModal3" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <img src="/images/voucher-turis.jpg" alt="Polita de Asigurare" class="img-responsive">
     </div> 
  </div> 
  <div class="section-about">
    <div class="clearfix">
      <div class="col-md-12">
          <h1 class="heading-small bold"> Tichete Vacanta </h1>
          <p class="margin-top-20"><span class="bold">Tichetele de vacanta</span> sunt cunoscute si sub denumirea de <span class="bold">vouchere de vacanta</span> sunt bonuri valorice asemanatoare cu tichetele de masa.</p>
          <p>Acestea pot fi acordate de angajatori salariatilor si pot fi utilizare pentru achitarea vacantei in Romania atat pentru beneficiar cat si pentru membrii familiei sale.</p>
          <p class="margin-top-20">Fiind instrumente de plata, <b>tichetele de vacanta</b> sunt nominale si netransmisibile. </p>
          <p> Acestea pot fi acordate de catre toate companiile, indiferent de sectorul de activitate sau de capitalului social, singura conditie fiind ca aceste companii sa fi inregistrat profit sau, dupa caz, venit, in anul financiar anterior.</p>
          <p class="margin-top-20"><span class="bold">Tichetele de vacanta</span> sunt deductibile si scutite de taxe si impozite atat pentru angajator cat si pentru salariat, de aceea sunt o varianta ideala pentru stimularea si recompensarea angajatilor.</p>

          <p class="bold margin-top-20">La Brigitte Travel puteti plati cu Tichete de Vacanta emise de:</p>
          <p><a href="http://upromania.ro/clienti/produse/tichete-de-vacanta/tichete-de-vacanta-435/" target="_blank">- Cheque Vacances</a></p>
          <p><a href="https://www.edenred.ro/ro/produse-si-servicii/beneficii-extra-salariale/vouchere-de-vacanta" target="blank">- Edenred</a></p>
          <p><a href="http://ro.sodexo.com/en/home.html" target="_blank">- Sodexo</a></p>
          <p class="bold margin-top-20">ATENTIE!</p>
          <p>Tichetele de vacanta se folosesc numai pentru vacante in Romania. </p>
          <p style="padding-bottom: 30px;">Beneficiarii le vor putea folosi pentru plata cazarii, a meselor, a transportului, a tratamentelor balneare sau a oricarui alt servicu turistic desfasurat pe teritoriul Romaniei. Aceste servicii pot fi achizitionate prin intermediul agentiilor de turism licentiate.</p>
      </div>
    </div>  
  </div>
</div>
<div class="news">
@component('subscriber.subscribe')@endcomponent
</div> 
@endsection  