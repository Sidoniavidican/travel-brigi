@extends('layouts.app')
@section('content')
<div class="relative adm-margin">
   <div class="container">
       <div class="row">
	        <div class="col-sm-4">
			    <div class="box-adm clearfix">
				    <div class="left-cont">
				  		<p class="bold">Vacante</p>
				  		<p class="silver small">{!!$offersNr!!} nr. total de vacante</p>
				  		<p><span class="big silver">{!!$oldOffers!!}</span><span class="top silver">expirate</span></p>
				  		@if($oldOffers > 0)
				  	</div>
				  	<div class="right-cont">
				  		<span class="icon-Home_Vacante"></span>
				  	</div>
				  	<div class="clearfix col-sm-12 no-padd">
				  		<a href="/admin/offers/expired" class="red"> Editeaza </a> 
				  		@endif
				  	</div>
			    </div>
		    </div> 

		    <div class="col-sm-4">
			    <div class="box-adm clearfix">
				    <div class="left-cont">
				  		<p class="bold">Subscriberi</p>
				  		<p class="silver small">{!!$subscribers!!} nr. total de subscribers</p>
				  		<p><span class="big silver">{!!$newSubscribers!!} </span><span class="top silver">Noi</span></p>
				  	</div>
				  	<div class="right-cont">
				  		<span class="icon-Home_Visitor"></span>
				  	</div>
			
			    </div>
		    </div>

		    <div class="col-sm-4">
			    <div class="box-adm clearfix">
				    <div class="left-cont">
				  		<p class="bold">Cereri de oferte</p>
				  		<p class="silver small">{!!$offerRequests!!} nr. total de cereri de oferte</p>
				  		<p><span class="big silver">{!!$newOfferRequests!!} </span><span class="top silver">Noi</span></p>
				  	</div>
				  	<div class="right-cont">
				  		<span class="icon-Home_Useri"></span>
				  	</div>
			
			    </div>
		    </div>
	    </div>

		<div class="row margin-top-30">
		    <div class="col-sm-4">
			    <div class="box-adm avion clearfix">
				    <div class="left-cont">
				  		<p class="bold">Cereri de bilete</p>
				  		<p class="silver small">{!!$flyRequests!!} nr. total de cereri de bilete</p>
				  		<p><span class="big silver">{!!$newFlyRequests!!} </span><span class="top silver">Noi</span></p>
				  	</div>
				  	<div class="right-cont">
				  		<span class="icon-Home_CereriAvion"></span>
				  	</div>
			    </div>
			</div>
		    <div class="col-sm-8">
			    <div class="box-adm clearfix">
			  		<p class="bold">Vacantele cu cele mai multe cereri</p> 
			  		@foreach($bestOffersRequest as $offer)
			  			<div class="">
			  			    <div class="col-sm-11 no-padd"><div class="lists">{!!$offer->name!!}</div></div>
			  				<div class="col-sm-1 no-padd center"><div class="red-box lists">{!!count($offer->requests)!!}</div></div>
			  			</div>
			  		@endforeach
		        </div> 
	        </div>
		</div>
	</div>
</div>


  </div>
@endsection

 