@extends('layouts.app')
@section('title') Brigitte Travel @stop
@section('description')Fa parte din familia Brigitte Travel si calatoreste cu noi! @stop
@section('content')
  <div class="container">
    <div class="content">
      <div class="grid main-page-home"> 
        @foreach($offers as $offer)
         <div class="col-sm-6 col-md-4">
            <a href ="/oferta/{!!$offer->id!!}">
              <div class="inner-content">
                <h1>
                  {!! str_limit($offer->name, 20)!!}
                </h1>
                <p class="white">{!! Carbon\Carbon::parse($offer->begin_date)->format('d.m.Y') !!} - {!! Carbon\Carbon::parse($offer->end_date)->format('d.m.Y') !!} - {!!$offer->days!!} zile</p>
                @if($offer->from != null)
                    <div class="transport">
                      <p>Plecarea din: {!!$offer->from!!}</p> 
                    </div>
                @endif
                <div class="inner-right-text">
                    @if(strchr($offer->type,'3'))
                      <div class="right-text"><span>Circuit</span></div>
                    @else
                      @if(strchr($offer->type,'4'))
                          <div class="right-text"><span>Sejur</span></div>
                      @endif
                    @endif
                </div>
              </div>
              <figure class="effect-julia">
                  @if(isset($offer->photos[0]))
                    <img src="/images/offers/{{ $offer->id }}/{{ $offer->photos[0]->path }}"/>
                  @else 
                    <img src="/images/poza3.jpg" alt="img21"/>
                  @endif  
                   <figcaption>
                      <div>
                        <p class="white">{!! str_limit($offer->description, 240)!!}</p>
                      </div>
                   </figcaption>          
              </figure>
              <div class="inner-content-bottom">
                  @include('offer.offerPrice')
	          @if(strchr($offer->type, '1') && Carbon\Carbon::now() > Carbon\Carbon::parse($offer->LM_date))
		                    <div class="early"><span>Last Minute</span></div>
		  @elseif(strchr($offer->type, '2') && Carbon\Carbon::now() < Carbon\Carbon::parse($offer->EB_date))
		                    <div class="early"><span>Early Booking</span></div>
		  @endif 	 
                  
              </div>
            </a>
         </div>
        @endforeach
      </div>
    </div>
  </div>
@component('subscriber.subscribe')@endcomponent
  <div class="container">
    <div class="center">
      <h3 class="center heading-gray">Vreti sa ne vedeti in excursii? Uitati-va prin albumele foto!</h3>
    </div>
    <div class="inner-albums clearfix">
    @foreach($albums as $album)
      <div class="col-sm-6 albums">   
        <a href="album/{{$album->id}}">
          <div class="col-sm-6 no-padd">
            <img src="images/gallery/{{$album->id}}/{!!$album->photos[0]->path!!}">
            <img src="images/gallery/{{$album->id}}/{!!$album->photos[1]->path!!}">
          </div>
            <div class="col-sm-wide">
                <div class="col-sm-cover" style="background-image: url('/images/gallery/{{$album->id}}/{!!$album->photos[2]->path!!}')">
                </div>
            </div>
            <div class="button-upload">
              <button type=""><span class="icon-PhotoAlbum"></span></button>
            </div>
           <div class="layer-black"></div>
         </a>
      </div>
    @endforeach
    </div> 
    <div class="section-parteneri"> 
      <div class="padding-head">
        <h3 class="heading-gray center" title="Multumim partenerilor nostri!">Multumim partenerilor nostri!</h3>
      </div>
      <div class="inner-part">
        <ul class="centered inner-first">
          <li><img src="/images/paralela45.jpg" alt="paralela45"/></li>
          <li><img src="/images/citycenter.png" alt="citycenter"/></li>
          <li><img src="/images/mementobus.jpg" alt="mementobus"/></li>
          <li><img src="/images/christiantour.png" alt="christiantour"/></li> 
          <li><img src="/images/omnia-turism.jpg" alt="omniaturism"/></li>
          <li><img src="/images/green-travel.jpg" alt="green travel"/></li>
          <li><img src="/images/budvar-tours.png" alt="budvar-tours"/></li>
          <li><img src="/images/tours-grand.jpg" alt="tours grand"/></li>
          <li><img src="/images/unitravel.jpg" alt="unitravel"/></li> 
        </ul> 
        <ul class="centered inner-second middle">
          <li><img src="/images/turkish-airlines.jpg" alt="turkish-airlines"/></li>
          <li><img src="/images/Lufthansa.png" alt="Lufthansa"/></li>
          <li><img src="/images/olympic.jpg" alt="olympic"/></li>
          <li><img src="/images/portugal.png" alt="portugal"/></li>
          <li><img src="/images/aeroflot.png" alt="aeroflot"/></li>
          <li><img src="/images/air-france.jpg" alt="air-france"/></li>
          <li><img src="/images/alitalia_logo_detail.jpg" alt="alitalia_logo_detail"/></li>
          <li><img src="/images/austrian.png" alt="austrian"/></li>
          <li><img src="/images/blue-air.jpg" alt="Blue Air"/></li>
          <li><img src="/images/British-Airways-logo.png" alt="British-Airways-logo"/></li>
        </ul>
        <ul class="centered inner-second">
          <li><img src="/images/tarom.png" alt="tarom"/></li>
          <li><img src="/images/corendon.png" alt="corendon"/></li>
          <li><img src="/images/EL-AL-Airlines.jpg" alt="EL-AL-Airlines"/></li> 
          <li><img src="/images/KLM_logo.svg.png" alt="KLM_logo"/></li>
          <li><img src="/images/lot-logo.gif" alt="lot-logo"/></li>
          <li><img src="/images/Qatar_Airways_Logo.svg.png" alt="Qatar_Airways_Logo"/></li>
          <li><img src="/images/Ryanair_logo.png" alt="Ryanair_logo"/></li>
          <li><img src="/images/Swiss-Airline-Logo.jpg" alt="Swiss-Airline-Logo"/></li>
          <li><img src="/images/wizz.png" alt="wizz-Airways-logo"/></li>
          <li><img src="/images/easyjet.gif" alt="wizz-Airways-logo"/></li> 
        </ul>
      </div>
    </div>
  </div> 
  <div class="cover-middle cover-last clearfix">
    <div class="container"> 
      <div class="inner-middle-content center">
        <span class="white icon-Instagram"></span>
        <h3>Fa parte din familia Brigitte Travel folosind #brigittetravel pe Instagram! </h3>
        <p class="white">Urca pozele tale preferate din excursiile Brigitte Travel pe Instagram folosind hastag-ul respectiv, si aveti sansa sa apareti pe contul nostru!</p>

          <div class="instagram-photos" >
            <div class="col-sm-6 col-md-4">
              <div class="insta-col">
                <figure class="effect-bubba instagram-photo-1">
             
                  <figcaption></figcaption>     
                </figure>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="insta-col">
                <figure class="effect-bubba instagram-photo-2">
    
                  <figcaption></figcaption>     
                </figure>
              </div>
            </div>
            <div class="col-sm-12 col-md-4">
              <div class="insta-col">
                <figure class="effect-bubba instagram-photo-3">
       
                  <figcaption></figcaption>     
                </figure>
              </div>
            </div>
          </div>
      </div> 
    </div>
    <div class="gradient"></div>
  </div>
<!-- /container -->
@endsection

 