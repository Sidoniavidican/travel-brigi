 
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- Title -->
   <title>@yield('title')</title>
   <!-- Description -->
   <meta name="description" content="@yield('description')">  
   <!-- CSRF Token -->
   <meta name="csrf-token" content="{{ csrf_token() }}">

   <title>{{ config('app.name', 'Brigitte Travel') }}</title> 
   <link rel="shortcut icon" href="/images/favicon.png"/>
   <!-- Styles -->

   <link href="{{ asset('css/app.css') }}" rel="stylesheet">

   <!-- Scripts -->
   <script>
       window.Laravel = {!! json_encode([
           'csrfToken' => csrf_token(),
       ]) !!};
   </script>
</head>
<body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-97408587-1', 'auto');
  ga('send', 'pageview');

</script>
@if (Request::is('/') || (Request::is('contact')) || (Request::is('despre-noi')) || (Request::is('albume')) || (Request::is('tichete')) || (Request::is('cazari')) || (Request::is('transporturi')) || (Request::is('termeni-si-conditii')) || (Request::is('confidentialitate')) ) 
<div class="img-fixed">
  <a href="http://www.firmadeincredere.ro/show,18033" target="_blank">
      <img  src="/images/firma-de-incredere.png" class="firmaincredere">  
  </a>
</div>
<a href="/tichete"> 
  <div class="img-fixed-tickete">
    <div class="box-green">
      <p><span class="font-15">Acceptam</span> tichete vacanta</p>
    </div>
    <img src="/images/edt.jpg" alt="edt">
    <img src="/images/cdt.jpg" alt="cdt">
    <img src="/images/voucher-turis.jpg" alt="voucher-turis">
  </div>
</a>
@endif 
    <div id="app">
        @if(Auth::check() && auth()->user()->id=='1' && strpos(url()->current(), 'admin'))
           @include('header')
           <div class="container">
               <div class="main-content">
                   <div style="padding: 40px"> </div>
                   @yield('content')
               </div>
           </div>
        @else
           @include('header2')
           @yield('content')
           @include('footer')
        @endif
       
   </div>


   <!-- Scripts -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzimuvomajo6NN3wxX76IIWdD6SsKi09c"></script>
  <script src="{{ asset('js/app.js') }}"></script>


  @yield('scripts')
</body>
</html>
