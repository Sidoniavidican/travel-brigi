<div class="inner-menu-wrap clearfix">
<div class="menu-outer">
    <ul class="list-inline">
        @if(isset($types))
          @foreach($types as $type)
            @if(Request::is(str_finish('admin/', str_replace(' ', '-', strtolower($type->name)))))
                @include('offerHeader')
            @endif
          @endforeach
        @endif

        @if(Request::is('admin/add-offer') || Route::currentRouteName() == 'edit' || Request::is('admin/transports') || Request::is('admin/add-transport') || Request::is('admin/offers'))
            @include('offerHeader')
        @endif
        @if(Request::is('admin/newsletter') || Request::is('admin/newsletters'))
            <li class="{!! Request::is('admin/newsletters') || Request::is('admin/newsletters/create') ? 'curent__item subscribe_curent' : '' !!}"><a href="{{ url('/admin/newsletters') }}">Newsletters</a></li>
        @endif
        @if(Request::is('admin/subscribers'))
            <li class="{!! Request::is('admin/subscribers')  ? 'curent__item subscribe_curent' : '' !!}"><a href="{{ url('/admin/subscribers') }}">Subscribers</a></li>
        @endif
        @if(Request::is('admin/albums')|| Request::is('admin/add-album') ||  Route::currentRouteName() == 'edit-album' )
            <li class="{!! Request::is('admin/albums') || Request::is('admin/add-album') ||  Route::currentRouteName() == 'edit-album'  ? 'curent__item subscribe_curent' : '' !!}"><a href="{{ url('/admin/albums') }}">Albums</a></li> 
        @endif 
    </ul> 
</div>
  <div class="shadow"></div> 
  <div id="wrapper" class="toggled">
    <div class="menu-inner-wide"> 
    <ul>
      <li class="sidebar-brand">
        <a href="#menu-toggle" id="menu-toggle"> <i class="icon-DropdownEnd open-menu" aria-hidden="true"></i><i class="icon-Close close-menu" aria-hidden="true"></i></a> 
      </li>
    </ul>  
    <div id="sidebar-wrapper" class="clearfix"> 
      <div class="sidebar-nav">
          <div class="logo">
            <a href="{{ url('/admin') }}">
              <img src="/images/LogoBrigitte.svg" alt="logo application" />
            </a> 
          </div> 
        <div class="second-wrap clearfix">
          <ul class="nav navbar-nav navbar-right">
          <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="icon-Edit admin"><p> Administrator</p></li>
                            <li class="dropdown"> 

                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li class="logout">
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
            </ul>
            <ul class="nav navbar-nav"> 
                <li class="icon {!! Request::is('admin') ? 'curent__item__menu' : '' !!}"><a href="{{ url('/admin')}}">
                  <i class="js-onebyone icon-menu icon-HomeSVG"></i><span class="js-onebyone-link">Acasa</span><span class="sr-only"></span></a>  
                </li>
                  
                <li class="icon {!!Request::is('admin/add-offer') || Request::is('admin/edit-offer') || Request::is('admin/offers') || Request::is('admin/early-booking') || Request::is('admin/cazari') ||  Request::is('admin/last-minute') || Request::is('admin/transports') || Request::is('admin/circuite') || Request::is('admin/sejururi') || Request::is('admin/exotic') || Request::is('admin/city-break') || Request::is('cazari') || Request::is('senior-voyage')? 'curent__item__menu' : '' !!}"><a href="{{ url('/admin/offers') }}"><i class="js-onebyone icon-menu icon-Oferte"></i><span class="js-onebyone-link">Oferte</span></a></li>  
                
                <li class="icon {!! Request::is('admin/newsletters') || Request::is('admin/newsletter') ? 'curent__item__menu' : '' !!}"><a href="{{ url('admin/newsletters') }}"><i class="js-onebyone icon-menu small-icn icon-Email"></i><span class="js-onebyone-link">Newsletter</span></a></li>
  
                <li class="icon {!! Request::is('admin/subscribers') ? 'curent__item__menu' : '' !!}"><a href="{{ url('/admin/subscribers') }}"><i class="js-onebyone icon-menu icon-GestionareClienti"></i><span class="js-onebyone-link">Evidenta Clienti</span></a></li>
                
                <li class="icon {!! Request::is('admin/albums') || Request::is('edit-album') ? 'curent__item__menu' : '' !!}"><a href="{{ url('/admin/albums') }}"><i class="js-onebyone icon-menu icon-AlbumeFoto"></i><span class="js-onebyone-link"> Albume Foto</span></a></li>

                   <li class="icon {!! Request::is('admin/hotels') || Request::is('edit-album') ? 'curent__item__menu' : '' !!}"><a href="{{ url('/admin/hotels') }}"><i class="js-onebyone icon-menu icon-Hotel" style="font-size: 20px;"></i><span class="js-onebyone-link"> Hoteluri</span></a></li>
				
                <li class="designed-admin">
                  <div class="designed center clearfix">
                    <a class="powered"  href="https://glabit.com/" target="_blank"> <img src="/images/glabit.png" alt="glabit logo"></a>
                    <p>Designed by</p>
                    <a class="powered" href="https://glabit.com/" target="_blank"> GlabIT</a>
                  </div>
                </li> 

             </ul>        
            </div> 
      </div>
    </div>
  </div>
</div>
</div> 