@extends('layouts.app')
@section('content')  

	<a href="/admin/newsletters-lists/create">
		<button type="button" class="yellow-btn">
			<span class="icon-Plus" aria-hidden="true"></span>
		</button> 
	</a>
	<div>
		@foreach($lists as $list)
			<div class="row" id="list{!!$list->id!!}">
				<div class="col-sm-6">
					{!!$list->name!!} - {!!$list->stats->member_count!!} subscriberi
				</div>
				<div class="col-sm-6">
					@if($list->id != '3a03dcfd70')
						{{ Form::submit('Sterge', ['class' => 'btn btn-danger deleteList', 'id' => $list->id]) }}
					@endif
			    </div>
			</div>
		@endforeach
	</div>
@endsection
