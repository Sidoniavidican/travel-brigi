@extends('layouts.app')
@section('content')
               
	<div class="admin-offer adm-offer-height">
	    <div class="panel-news-550  ">
	        <h4 class="heading-small">
	            <strong>{!!$newsletter->name!!} </strong>
	        </h4>	
	        <div class="margin-top-20">
	        <p>{!!$newsletter->message!!}</p>   
	        </div>         
		    <div class="row">
		    	@foreach ($newsletter->newsletterOffers as $newsletterOffer)
		            <div class="col-sm-4 margin-top-10 hero-clear">
			        	<div class="hero">						
							<img src="/images/newsletters/{{ $newsletterOffer->newsletter->id }}/{{ $newsletterOffer->path }}"/>
						</div>
						<div class="offer-details">   
						    <h1 class="heading-small head-news">{!!$newsletterOffer->name!!}</h1>
						    <p class="date small">
						    	{!! Carbon\Carbon::parse($newsletterOffer->begin_date)->format('d.m.Y') !!} - 
						    	{!! Carbon\Carbon::parse($newsletterOffer->end_date)->format('d.m.Y') !!}
						    	</p>
						    <p class="small"><span class="icon-Location font-20"></span>{!!str_replace(',', ', ', $newsletterOffer->country)!!}</p> 
						    <div class="price">
							    @if($newsletterOffer->price != null)
									@if($newsletterOffer->currency==0)
										<p>
											<span class="red small">de la </span> <span class="big-pr red">
												@if($newsletterOffer->discount != null)
													<strong> 
														&#8364;{!!$newsletterOffer->price * (100-$newsletterOffer->discount) / 100 !!}
													</strong> 
													<strike>
														<span class="small-pr-offer"> &#8364;{!!$newsletterOffer->price!!}</span>
													</strike>
													<div class="news-type-offer">
														@if($newsletterOffer->discount_type == 1)
															Last Minute
														@else 
															Early Booking
														@endif
														{!!$newsletterOffer->discount!!}%
													</div>
												@else
												<span class="small-pr-offer"> &#8364;{!!$newsletterOffer->price!!}</span> 
												@endif
											</span>
										</p>
									@else
										<p>
											<span class="red">de la </span><span class="big-pr red"> 
												@if($newsletterOffer->discount != null)
													<strong> 
														{!!(int)($newsletterOffer->price * (100-$newsletterOffer->discount) / 100)  !!} RON
													</strong> 

													<span class="small-pr-offer"> 
														<strike>{!!$newsletterOffer->price!!} RON
														</strike> 
													</span> 
													<div class="news-type-offer">
														@if($newsletterOffer->discount_type == 1)
															Last Minute
														@else 
															Early Booking
														@endif
														{!!$newsletterOffer->discount!!}%
													</div>
												@else
													<span class="small-pr-offer">{!!$newsletterOffer->price!!} RON</span>
												@endif

											</span>
										</p>
									@endif
						     	@endif
						    </div>
						
						</div>
			        </div>
				@endforeach
			</div>
	    </div>
	 </div>

@endsection