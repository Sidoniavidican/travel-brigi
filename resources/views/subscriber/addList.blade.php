@extends('layouts.app')
@section('content')
  
<div class="relative adm-margin">  	
	<div class="admin-offer admin-offer-simple clearfix">

		{{ Form::open(['method' => 'POST', 'route' => ['newsletters-lists.store'] ]) }}
		{{ csrf_field() }}
				<div class="row margin-top-20">
					<div class="col-sm-12">
						<span class="label-adm">{{ Form::label('name', 'Nume lista:') }}</span>	
						<span class="filed-adm">{{ Form::text('name', null, ['required']) }} </span>
					</div>
				</div>

				<div class="fixed-footer">
			   		 <div class="relative-footer">
						 <div class="margin-top-20 right-btn m-right" style="padding-bottom: 20px;">
							{{ Form::submit('Adauga lista', ['class' => 'btn btn-primary']) }}
						</div>
					</div>
				</div>
		{{ Form::close()}}
	</div>
</div>
@endsection