<div class="cover-middle clearfix">
  <div class="container">
    <div class="inner-middle-content center">
      <h2 class="white"> Vacanta ta de vis e doar la un click distanta </h2>
      <p class="white">Aboneaza-te la newsletter pentru ultimele noutati, oferte speciale si surprize!</p>

       {{ Form::open(['method' => 'POST', 'route' => ['subscribe'], 'id'=>'subscribe']) }}
          {{ csrf_field() }}
          <div class="input-subscribe relative clearfix">
            <input type="text" placeholder="Email address" name="email" />
            <button type="submit"><div class="icon-subscribe icon-Send"></div></button>
          </div>
         {{ Form::close()}}

   </div>
  </div>
  <div class="gradient"></div>
</div> 