@extends('layouts.app')
@section('content')
  
<div class="relative adm-margin">   
  <div class="admin-offer admin-offer-simple clearfix newsletter">
  <div class="inner-news-padd">
    {!! Form::open(array('route' => 'sendCompaign', 'class'=>'form-horizontal', 'id'=>'addNewsletterValidate')) !!}
    <fieldset>
      <div class="form-group">
          <span class="label-adm"><label control-label" for="subject"> Titlu Newsletter * </label></span>
          <span class="filed-adm"><input id="subject" name="subject" type="text"  required class="form-control"></span>
      </div>

      <!-- Message body -->
      <div class="form-group">
        <span class="label-adm"><label class="control-label" for="message"> <span class="">Descriere *</span> </label></span>
         <span class="filed-adm"><textarea class="form-control" style="max-height: 50px;" id="message" name="message" required rows="5"></textarea></span>
      </div>

      <div class="form-group offers-selected hide">
        <span class="label-adm">
          <label class="control-label"> Oferte selectate</label>
        </span>
        <span class="filed-adm"> 
          <div class="" id="offers-selected" class="clearfix"></div>
          <input type="hidden" name="offers" id="selectedList">
        </span>
      </div>

      <div class="form-group">
        <span class="label-adm">
          <label class="control-label" for="type"> Categorie oferte: </label>
        </span>
        <span class="filed-adm">
          <div class="">
              <select id="type_id" class="form-control">
                    <option disabled selected> Alege </option>
                    @foreach($types as $type )
                        <option value="{{ $type->id }}" id="{{ $type->id }}" >{{ $type->name }}</option>
                    @endforeach
              </select>
            </div>
        </span>
      </div>

      <div class="form-group" id="offer">
          <span class="label-adm"><label class="control-label" for="offer" > Oferte selectate</label></span>
          <span class="filed-adm">
            <div class="" id="offer-list">
            </div>
          </span>
      </div>

      <div class="right">
        <div class="col-md-12 text-right no-padd">
          <button type="submit" class="send2">Trimite Newsletter</button>
        </div>
      </div>

    </fieldset>
    {!! Form::close() !!}
    </div>
</div>
</div>
@endsection