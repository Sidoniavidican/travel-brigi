@extends('layouts.app')
@section('content')
<div class="relative adm-margin">  	
	<div class="subscribers cover-middle clearfix">

		<div>
			Adauga subscriber:
	       {{ Form::open(['method' => 'POST', 'route' => ['subscribe'], 'id'=>'subscribe']) }}
	          {{ csrf_field() }}
	          <div class="input-subscribe relative clearfix margin-top-20">
	            <input type="text" placeholder="Email address" name="email" />
	            <button type="submit"><div class="icon-subscribe icon-Send"></div></button>
	          </div>
	         {{ Form::close()}}
	    </div>

		<table style="width:100%">
		    <thead>
			<tr>
			    <th>Email</th>
			    <th>Prenume</th> 
			    <th>Nume</th> 
			    <th>Telefon</th>
			    <th>Domiciliu</th>
			    <th></th>
			</tr>
			</thead>
			@foreach($subscribers as $subscriber)
				<tr>
					<td>
						{!!$subscriber->email!!}
					</td>
					<td>
						<a href="#" id="firstName" class="edit-subscriber" data-pk="{!!$subscriber->id!!}" data-url="/edit-subscriber">
							{!!$subscriber->firstName!!}
						</a> 
					</td>
					<td>
						<a href="#" id="lastName" class="edit-subscriber" data-pk="{!!$subscriber->id!!}" data-url="/edit-subscriber">
							{!!$subscriber->lastName!!}
						</a> 	
					</td>
					<td>
						<a href="#" id="phone" class="edit-subscriber" data-pk="{!!$subscriber->id!!}" data-url="/edit-subscriber">
							{!!$subscriber->phone!!}
						</a> 
					</td>
					<td>
						<a href="#" id="address" class="edit-subscriber" data-pk="{!!$subscriber->id!!}" data-url="/edit-subscriber">
							{!!$subscriber->address!!}
						</a> 
					</td>
					<td>
					{{ Form::open(['method' => 'DELETE', 'route' => ['delete-subscriber', $subscriber->id]]) }}
						{{ Form::submit('Sterge', ['class' => 'btn btn-danger small-x']) }}	
					{{Form::close()}}
					</td>
				</tr>
			@endforeach
		</table>
	</div>
</div>
@endsection

@section('scripts')
    <script>  
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
    </script>
@endsection 