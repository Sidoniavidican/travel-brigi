@extends('layouts.app')
@section('content')

@if (Session::has('offer-success'))
    <p class="offer-success" name="{{ Session::get('offer-success') }}"></p>
@endif

<div class="relative adm-margin">  	
		<a href="/admin/newsletters/create">
			<button type="button" class="yellow-btn">
				<span class="icon-Plus" aria-hidden="true"></span>
			</button> 
		</a>
	<div class="subscribers clearfix">
    		<table style="width:100%">
			<thead>
				<tr>
				    <th>Subiect</th>
				    <th>Mesaj</th> 
				    <th></th> 
				    <th></th>
				</tr>
			</thead>
            @foreach($newsletters as $newsletter)
                <tr id="newsletter{!!$newsletter->id!!}">
			        <td >
			        	<strong>
		                    {!! str_limit($newsletter->name, 20)!!}	  
		                </strong>
			        </td>
			        <td>
		                    {!! str_limit($newsletter->message, 20)!!}	             	                
			        </td>
			        <td align="right">
		                 <a href="/admin/newsletters/{!!$newsletter->id!!}" class="btn btn-primary "> Vezi detalii </a> 	                
			        </td>
			         <td align="right">
			       		{{ Form::submit('Sterge', ['class' => 'btn btn-danger deleteNewsletter small-x', 'id' => $newsletter->id]) }} 
			        </td>
		        </tr>
	        @endforeach
	    </table>
	</div>
</div>
@endsection