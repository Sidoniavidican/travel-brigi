@extends('layouts.app')
@section('content')  

@if (Session::has('offer-success'))
    <p class="offer-success" name="{{ Session::get('offer-success') }}"></p>
@endif

<a href="/admin/hotels/create">
    <button type="button" class="yellow-btn">
		<span class="icon-Plus" aria-hidden="true"></span> 
	</button>
</a> 
<div class="container">
  <div class="inner-options">
    <div class="description-box">
      <table class="table-hotels period-offer border">
        <thead> 
           <tr class="right-tr">
            <th style="left:10px; position: relative;"> Id </th>
            <th> Nume Hotel </th>
            <th> Confort </th>
            <th> Poza </th>
            <th> Sterge </th>
          </tr>
        </thead>
        <tbody>
        @foreach($hotels as $hotel)
                <tr class="tr-period"  id="hotel{!!$hotel->id!!}"> 
                    <td data-title="Id: ">
                      {!!$hotel->id!!}
                    </td>  
                    <td data-title="Perioada: ">
                      {!!$hotel->name!!}
                    </td> 
                    <td data-title="Confort: ">
                        <span class="yell">
                          <span class="icon-Star"> {!!$hotel->confort!!}</span> 
                        </span>               
                    </td>
                    <td data-title="Poza: ">
                      <div class="modal-about" data-toggle="modal" data-target="#myhotel">                          
                        @if($hotel->path != null)
                              <img src="/images/hotels/{!!$hotel->id!!}/{!!$hotel->path!!}" alt="hotel">
                        @else
                              <img src="/images/noimagehotel.png" alt="hotel">
                        @endif
                      </div>
                    </td>
                    <td data-title="Poza: ">
                      <button class="btn-hotel deleteHotel" type="button" id="{{$hotel->id}}"><span class="icon-Delete"></span></button>               
                    </td>
                </tr>   
            @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

@endsection
