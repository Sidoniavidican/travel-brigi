@extends('layouts.app')
@section('content')
  
<div class="relative adm-margin">  	
	<div class="admin-offer admin-offer-simple clearfix">

		{{ Form::open(['method' => 'POST', 'route' => ['hotels.store'], 'id'=>'addHotelValidate']) }}
		{{ csrf_field() }}
				<div class="row margin-top-20">
					<div class="col-sm-12">
						<span class="label-adm">{{ Form::label('name', 'Nume:') }}</span>	
						<span class="filed-adm">{{ Form::text('name', null, ['required']) }} </span>
					</div>

					<div class="col-sm-12 ">
		               {{ Form::label('confort', 'Confort:') }}
		               <span class="filed-adm">{{ Form::select('confort',['1'=>'1', '2'=>'2', '3'=>'3', '4'=>'4', '5'=>'5'] , '0', ['class' => '', 'id'=>'confort']) }}</span>
					</div>	
				</div>

				<div class="fixed-footer">
			   		 <div class="relative-footer">
						 <div class="margin-top-20 right-btn m-right" style="padding-bottom: 20px;">
							{{ Form::submit('Continua', ['class' => 'btn btn-primary addOffer']) }}
						</div>
					</div>
				</div>
		{{ Form::close()}}
	
		<div class="margin-top-20"> 
				<label > Adauga Poza: </label> 
			        {{ Form::open(['method' => 'POST', 'route' => ['store-hotel-photo'], 'class' => 'dropzone', 'id' => 'upload' ]) }}
					{{ csrf_field() }}
					<input name="id" value="1" type="hidden" id="id">		      
				{{ Form::close() }}
		</div>

	</div>

</div>
@endsection