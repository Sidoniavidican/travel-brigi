<div class="navbar-offer">
<div class="navbar"> 
  <div class="navbar-header">
    <button type="button" class="btn-right categ-btn" data-toggle="collapse" data-target=".navbar-collapse">
    Categorii <span class="icon-ArrowDown" style="position:relative; top:2px;"></span>
    </button>
  </div>
  <div class="navbar-collapse collapse">
  <ul class="nav navbar-nav">
              <li class="{!! Request::is('admin/offers') ? 'curent__item' : '' !!}"><a href="{{ url('/admin/offers') }}">Toate</a></li>
              <li class="{!! Request::is('admin/early-booking') ? 'curent__item' : '' !!}"><a href="{{ url('/admin/early-booking') }}">EARLY BOOKING</a></li>
              <li class="{!! Request::is('admin/last-minute') ? 'curent__item' : '' !!}"><a href="{{ url('/admin/last-minute') }}">LAST MINUTE</a></li>
              <li class="{!! Request::is('admin/circuite') ? 'curent__item' : '' !!}"><a href="{{ url('/admin/circuite') }}">CIRCUITE</a></li>
              <li class="{!! Request::is('admin/sejururi') ? 'curent__item' : '' !!}"><a href="{{ url('/admin/sejururi') }}">SEJURURI</a></li>
              <li class="{!! Request::is('admin/exotic') ? 'curent__item' : '' !!}"><a href="{{ url('/admin/exotic') }}">EXOTIC</a></li>
              <li class="{!! Request::is('admin/city-break') ? 'curent__item' : '' !!}"><a href="{{ url('/admin/city-break') }}">CITY BREAK</a></li>
              <li class="{!! Request::is('admin/croaziere') ? 'curent__item' : '' !!}"><a href="{{ url('/admin/croaziere') }}">CROAZIERE</a></li>
              <li class="{!! Request::is('admin/cazari') ? 'curent__item' : '' !!}"><a href="{{ url('/admin/cazari') }}">CAZARI</a></li>
              <li class="{!! Request::is('admin/transports') ? 'curent__item' : '' !!}"><a href="{{ url('/admin/transports') }}">TRANSPORTURI</a></li>
              <li>
                  <div class="dropdown dropdown-offer"> 
                    <button class="btn-inner-menu dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                     <a href="#"> Diverse</a>
                          <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                      @foreach($types as $type)
                          @if ($type->id > 7)
                                <li><a href="/admin/{!!str_replace(' ', '-', strtolower($type->name))!!}">{!!$type->name!!}</a></li>
                          @endif                                   
                      @endforeach
                    </ul>
                  </div>
              </li>     
           </ul>
        </div>
</div>
</div>
              