@extends('layouts.app')
@section('title') Confidentialitate @stop 
@section('description') Brigitte Travel | Confidentialitate @stop 
@section('content') 
<div class="container">
  <div class="terms"> 
    <div class="section-about clearfix">
    <div class="col-sm-12">
      <h1 class="heading-small bold">Confidentialitate</h1>
      <p>Garantam securitatea si confidentialitatea datelor transmise prin sistemul informatic.
      Furnizarea datelor personale nu implica nici o obligatie din partea utilizatorilor iar acestia pot refuza furnizarea acestor date in orice circumstante si pot solicita oricand stergerea datelor personale din baza de date.</p>
      <p>S.C. Brigitte Travel S.R.L va respecta informatiile confidentiale pe care le-ati furnizat si NU va vinde, NU va inchiria si NU va face barter cu liste de mailing ale clientilor sai.
      S.C. Brigitte Travel S.R.L solicita nume, adrese de e-mail, numere de telefon, date de la clientii sai in scopul finalizarii comenzilor cat mai corect si rapid.</p>
      <p>
      Ne rezervam dreptul de a anula comenzile incomplete sau facute fara informatii de baza ca telefon de contact.
      Orice schimbare a acestor reguli de confidenþialitate vor fi comunicate pe site-ul www.brigittetravel.ro cu cel puþin 10 zile inainte de implementarea lor.</p>
      <p>Informatiile colectate inainte de implementarea schimbarilor vor fi asigurate in conformitate cu vechile reguli de confidentialitate</p>
      <p>
      Ne rezervam dreptul de refuza colaborarea cu clientii ce au un comportament neadecvat sau limbaj necivilizat (agresiv) sau au comenzilor livrate refuzate.
      Pentru toata marfa trimisa prin curierat rapid la comanda clientului scrisa si refuzata fara motiv, ne rezervam dreptul de a ne recupera pe cai legale prejudiciul adus (transport, cheltuieli de judecata).</p>

      </p>
      </div>
    </div>
  </div>
</div>
      
        
<div class="news">
@component('subscriber.subscribe')@endcomponent
</div> 

@endsection 