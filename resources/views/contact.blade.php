@extends('layouts.app')
@section('title') Contact @stop 
@section('description') Vacanta ta de vis e doar la un click distanta! Contacteaza-ne !@stop 
@section('content')
<div class="container">
	<div id="map"></div>
	<div class="contact clearfix">
		<div class="row">
			<div class="col-sm-6">
				<div class="box box-form">
					<h1 class="heading-small">Contact</h1>
					<p class="small-p">Completati formularul de mai jos cu toate datele necesare. Campurile cu steluta (*) sunt obligatorii</p>
					@if(Session::has('success'))
	    			<div class="alert alert-success margin-top-20" role="alert">Multumim ! Mesajul a fost trimis cu succes!</div>  
	    		    @endif
					<form action="sendform" method="post" id="contactform" class="clearfix">
		                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
		                <span class="input input--hoshi">
		                    <input class="input__field input__field--hoshi" type="text" name="name" id="input-1" />
		                    <label class="input__label input__label--hoshi input__label--hoshi-color-1" for="name">
		                        <span class="input__label-content input__label-content--hoshi">Nume &amp; Prenume*</span>
		                    </label>
		                </span>

                        <span class="input input--hoshi">
		                    <input class="input__field input__field--hoshi" type="text" name="email" id="input-0" />
		                    <label class="input__label input__label--hoshi input__label--hoshi-color-1" for="email"> 
		                        <span class="input__label-content input__label-content--hoshi">Email*</span>
		                    </label>
		                </span>

		                <span class="input input--hoshi">
		                    <input class="input__field input__field--hoshi" type="text" name="phone" id="input-4" />
		                    <label class="input__label input__label--hoshi input__label--hoshi-color-1" for="phone"> 
		                        <span class="input__label-content input__label-content--hoshi">Telefon*</span>
		                    </label>
		                </span>

		                <span class="input input--hoshi">
		                    <input class="input__field input__field--hoshi" type="text" name="message" id="input-3" />
		                    <label class="input__label input__label--hoshi input__label--hoshi-color-1" for="mesaj"> 
		                        <span class="input__label-content input__label-content--hoshi">Mesaj*</span>
		                    </label>
		                </span>
                        <button  type="submit" class="right-btn margin-top-10" data-toggle="modal" data-target="#requestmodal">Trimite Mesaj</button> 
                    </form>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="box box-details">
					<h2 class="heading-small">Date Contact</h2>
					<ul class="margin-top-40">
					    <li><span class="icn icon-Location"></span><a href="https://www.google.ro/maps/place/Strada+Avram+Iancu+1,+Oradea/@47.05363,21.9258995,17z/data=!4m5!3m4!1s0x474647e860ad7ed1:0xd70101f5173d8e4f!8m2!3d47.05363!4d21.9280882" target="blank">Str. Avram Iancu, 1, Oradea, Bihor, Oradea</a><li>
						<li><span class="icn icon-Phone"></span><a href="tel:0259 434 413">0259 434 413</a> / <a href="tel:0745 928 715">0745 928 715</a></li>
						<li><span class="icn icon-Email"></span><a href="mailto:info@brigittetravel.ro">info@brigittetravel.ro</a></li>
						<li><span class="icn icn2 icon-Orar"></span>Luni - Vineri : 09:00 - 17:00 / Sambata - Duminica: inchis</li>
						<li class="icons"><a href=""><span class="icon-Facebook"></span></a></li>
						<li class="icons"><a href=""><span class="icon-Instagram"></span></a></li> 
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="news">
	@component('subscriber.subscribe')@endcomponent
</div>
@endsection