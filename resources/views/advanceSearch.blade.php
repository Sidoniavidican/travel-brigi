<div class="container">	
	<div class="advanced hero-forms clearfix"> 
		<div class="box-centered">
			<div class="small-hero-select">		
				<div class="clearfix">
					@if(Route::currentRouteName() == 'search') 
						<input type="hidden" name="search" value="{{$search}}" id="search">
						<div class="col-sm-12 search-red">
							<p class="white">Rezultate pentru : <i>{!!$search!!}</i></p>
						</div>
					@endif
					@if(str_contains(Route::currentRouteName(), 'filter'))
						<div class="col-sm-12 search-red">
							<p class="white">Filtru : <i>{!!$search!!}</i></p>
						</div>
					@endif
				</div>
				<div class="clearfix search-1">
					<div class="col-sm-3">
						<p class="silver"> Pret (&euro;):</p> 
					    <div class="range-bar clearfix">
							<div id="range">
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<p class="silver"> Durata (zile): </p> 
					    <div class="range-bar clearfix">
							<div id="range2">
							</div>
						</div>
					</div>		
				    <div class="col-sm-3 center stars2">
						<p class="silver confort-stele"> Confort (stele): </p>  
						<button class="stars" id="s1"><span class="silver">1</span><span class="icon-Star silver"></span></button>
						<button class="stars" id="s2"><span class="silver">2</span><span class="icon-Star silver"></span></button>
						<button class="stars" id="s3"><span class="silver">3</span><span class="icon-Star silver"></span></button>
						<button class="stars" id="s4"><span class="silver">4</span><span class="icon-Star silver"></span></button> 
						<button class="stars" id="s5"><span class="silver">5</span><span class="icon-Star silver"></span></button>
					</div>
					<div class="col-sm-3">

				        <div class="switch clearfix">
				            <div class="pull-left">
				            	<p class="silver">Transport inclus:</p>
				            </div>
				            <div class="pull-right">
						        <input id="cmn-toggle-transport" class="cmn-toggle cmn-toggle-round-flat transport change" type="checkbox">
						        <label for="cmn-toggle-transport"></label>
					        </div>
				        </div>
				   
				        <div class="switch clearfix">
				            <div class="pull-left">
				            	<p class="silver">Masa inclusa:</p>
				            </div>
				            <div class="pull-right">
						        <input id="cmn-toggle-masa" class="cmn-toggle cmn-toggle-round-flat masa change" type="checkbox">
						        <label for="cmn-toggle-masa"></label>
					        </div>
				        </div>
				    </div>
				</div>
				<div class="line-bottom-s"></div>
                <div class="clearfix margin-top-40 search-select">
					<div class="col-sm-6">
						<label class="silver">Luna: </label>  
						<select class="cs cs-select cs-skin-underline perioada change" name="perioada">
							<option value="toate" id="perioada-1"> Toate</option>
							@for($i=0; $i<12; $i++)
							    <option value="{{$i}}" id="perioada{{$i}}"> {!! Carbon\Carbon::now()->addMonth($i)->formatLocalized('%B %Y') !!}
							    </option>
							@endfor
						</select>
					</div>

		            <div class="col-sm-6">
						<label class="silver">Categorii: </label>  
						<select class="cs cs-select cs-skin-underline type change" name="type">
							<option value="toate" id="type-1" selected> Toate </option>
							@foreach($types as $type)
								<option value="{!!$type->id!!}" id="type{!!$type->id!!}"> {!!$type->name!!}</option>
							@endforeach
						</select>
			    	</div>

			    </div>	
		    </div>
	    </div>

	</div>
</div>