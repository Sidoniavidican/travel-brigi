
@extends('layouts.app')
@section('title') Despre Noi @stop 
@section('description') Brigitte Travel - Despre Noi!@stop 
@section('content')     
<div class="container">
	<div class="clearfix">
	    <div class="col-md-2 padd-7">
			<div class="modal-about" data-toggle="modal" data-target="#myModal1">    
				<img src="/images/brevet-turism.jpg" alt="Brevet Turism" class="img-responsive">
			</div>
		</div>
	    <div class="col-md-2 padd-7">
			<div class="modal-about" data-toggle="modal" data-target="#myModal2">    
				<img src="/images/licenta-turism.jpg" alt="Licenta Turism" class="img-responsive">
			</div>
		</div>
		<div class="col-md-4 padd-7">
			<div class="modal-about" data-toggle="modal" data-target="#myModal5">    
				<img src="/images/certif-inregistrare.jpg" alt="certificat" class="img-responsive">
			</div>
		</div>
		<div class="col-md-2 padd-7">
			<div class="modal-about" data-toggle="modal" data-target="#myModal3">    
				<img src="/images/polita-asigurare.jpg" alt="Polita de asigurare" class="img-responsive">
			</div>
		</div>
		<div class="col-md-2 padd-7">
			<div class="modal-about" data-toggle="modal" data-target="#myModal4">    
				<img src="/images/certificat.jpg" alt="certificat" class="img-responsive">
			</div>
		</div>

	</div>
 
	<div id="myModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	    	<img src="/images/brevet-turism.jpg" alt="Brevet de turism" class="img-responsive">
	    </div>
	</div>
	<div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <img src="/images/licenta-turism.jpg" alt="Licenta de turism" class="img-responsive">
	   </div>
	</div>
	<div id="myModal3" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <img src="/images/polita-asigurare.jpg" alt="Polita de Asigurare" class="img-responsive">
	   </div>
	</div>
	<div id="myModal4" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <img src="/images/certificat.jpg" alt="certificat" class="img-responsive">
	   </div>
	</div>
	<div id="myModal5" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <img src="/images/certif-inregistrare.jpg" alt="certificat" class="img-responsive">
	   </div>
	</div>  
  

	<div class="section-about">
		<div class="clearfix">
			<div class="col-md-6">
				<h1 class="heading-small bold"> Despre noi </h1>
				<p class="bold">CINE SUNTEM ?</p>
			 	<p><span class="align">
             	Un operator -tour român, care se ocupă de vacanțele dumneavoastră, de mai bine de douăzeci de ani. Înființată în 1995 în Oradea, ca agenție  de familie, „Brigitte Travel” a devenit, cu timpul, o importantă familie a iubitorilor de călătorii. Prin seriozitate, responsabilitate și corectitudine, am rămas în topul preferințelor multora dintre turiștii care au înțeles că la baza activității noastre stă un principiu: o călătorie turistică trebuie să fie relaxare, dar și cunoaștere, participare la  evenimente diverse, surprize. Au rămas fideli agenției noastre, mulți dintre aceia care au fost clienții noștri, încă din primii ani de activitate.</span>
		        <br><span class="align">
		           Un climat de colaborare și punere în practică a unor idei și sugestii venite de la turiști,ne-au dat girul calității și competitivității. Avem deja circuite clasice în Grecia și Turcia și un număr important de colaboratori, în toate țările vizitate, pentru a putea asigura clienților noștri un sejur plăcut și pentru a-i feri. de orice neplăceri. Pe oricare traseu, turiștii beneficiază de ghid și de explicații în limbile : română, maghiară, engleză, franceză, germană.
					</span></p>
			
				<p class="bold margin-top-20">PENTRU CONFORTUL DUMNEAVOASTRĂ</p>
		        <p><span class="align">
		        Am învățat, în toți acești ani de călătorii prin Europa, cât este de important să rămâi o gazdă primitoare, pentru oaspeții tăi, turiștii; indiferent de vârstă, să-i tratezi pe toți, cu înțelegere și bunăvoință. Să diversificăm ofertele, în funcție de nevoile și cerințele lor, să le oferim servicii de cea mai bună calitate, la prețuri convenabile. Toate acestea se regăsesc în pachetul nostru de 
		        <span class="bold">SERVICII:</span></span></p>
		
				<ul class="lists-about">
				    <li> - turism internațional de grup și individual</li>
					<li> - turism specializat( luna de miere ), turism cultural ( concerte de anvergură internațională, festivaluri )</li>
					<li> - închirieri de autocare și microbuze; vânzări de bilete pentru autocar pentru destinații europene, sau diverse</li>
					<li> - programe de Weekend în special în Ungaria, la  grădină zoologică de la Nyiregyhaza, sau la diferite Festivaluri culinare</li>
					<li> - asigurare bilete de tratament, în zone montane, litoralul românesc</li>
					<li> - vizite la obiective culturale și religioase, în funcție de cerere</li>
					<li> - pachete turistice complexe: sejururi și circuite, cu integrarea cazării, a mesei, transport terestru sau aerian</li>
					<li> - asigurări medicale de călătorie</li>
					<li> - bilete de avion</li>

				</ul>
				<p class="bold">CE NE PROPUNEM ?</p>
				<p><span class="align"> Intenționăm, în funcție de cerere, ca din acest an să extindem aria de interes turistic, dincolo de hotarele Europei</span></p>
				<ul class="lists-about">
				<li> -  să facilităm clienților noști accesul la călătorii în ținuturi exotixe, cu avionul, sau croaziere.</li>
				<li> - Excursii in Delta Dunării, etc </li>
				<li> - Seniori Voyage, primăvara si toamna in destinații turistice consecrate: Dubai, Sardinia, Rusia (circuit), Portugalia, Italia, Spania.</li>
		        <p class="bold">IMPORTANT</p>
		        <p><span class="align last">Toti clienții noști fideli beneficiază de reduceri importante.</span></p> 
	        </div>
			<div class="col-md-6 no-padd">
				<div class="space-1"></div>
				<div class="space-1"></div>
			</div>
	    </div>
	</div>  
</div>
<div class="news">
@component('subscriber.subscribe')@endcomponent
</div> 
@endsection 