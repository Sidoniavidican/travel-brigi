@extends('layouts.app')
@section('title') Transporturi @stop 
@section('description') Brigitte Travel Transporturi! @stop 
@section('content')  

@if (Session::has('offer-success'))
    <p class="offer-success" name="{{ Session::get('offer-success') }}"></p>
@endif

<div class="relative">
	<div class="container">

		<a href="/admin/transports/create"> 
		    <button type="button" class="yellow-btn">
		        <span class="icon-Plus" aria-hidden="true"></span>
		    </button>
	    </a>

		<div class="main-grid">
		    <div id="container" class="clearfix">
		        <div class="grid-sizer"></div>
				@foreach($transports as $transport)
				<div class="item2" id="transport{!!$transport->id!!}">
				    <div class="box-transports"> 
					    	<h1 class="heading-gray">{!!$transport->from!!} - {!!$transport->to!!}</h1>
					    	<p>{!! Carbon\Carbon::parse($transport->date)->format('d.m.Y') !!} , {!!$transport->hour!!}</p>
					    	<span class="trans-price">€ {!!$transport->price!!}</span>
					    	<p>{!!$transport->description!!}</p>
	
						<div>
							{{ Form::submit('Sterge', ['class' => 'btn btn-danger deleteTransport', 'id' => $transport->id]) }}
						</div>
				     </div> 
				</div>
			    @endforeach
		   </div>
		</div>
	</div>
</div>
@endsection 