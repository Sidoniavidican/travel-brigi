@extends('layouts.app')

@section('content')

<div class="relative adm-margin">
<div class="admin-offer admin-offer-simple">
		{{ Form::open(['method' => 'POST', 'route' => ['transports.store'], 'class'=>'dateRangeForm', 'id' => 'transportValidate']) }}
				{{ csrf_field() }}
				<label class="margin-top-20">Date</label>
				<div class="input-group input-append date dateRangePicker">
					<span class="filed-adm"><input type="text" name="date" required id="date_transport"></span>
					<span class="input-group-addon add-on"></span>
				</div>

				<label class="margin-top-20">Ora</label>
				<span class="filed-adm"><input type="text" name="hour" required id="hour"></span> 
				<br>

				<label class="margin-top-20">Plecare</label>
				<span class="filed-adm"><input type="text" name="from" required id="from_transport" maxlength="80"> </span>
				<br>

				<label class="margin-top-20">Destinatie</label>
				<span class="filed-adm"><input type="text" name="to" required id="to_transport"  maxlength="80"></span>
				

				<label class="margin-top-20">Price</label>
				<span class="filed-adm"><input type="number" name="price" required id="price_transport" max="20000", min="5"></span>
			

				<label class="margin-top-20">Description</label>
				<span class="filed-adm"><input type="textarea" name="description" id="description_transport"></span>
				<br>
			    {{ Form::submit('Salveaza', ['class' => 'btn btn-primary right-btn small-btn margin-top-20 btn-small addTransport']) }}		 
		{{ Form::close() }}

	</div>
	</div>

@endsection 