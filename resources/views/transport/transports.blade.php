@extends('layouts.app')
@section('content')
<div class="relative">
	<div class="container">
		<div class="main-grid">
		    <div class="grid-sizer"></div>
			@foreach($transports as $transport)
			<div class="item2">
				<div class="box-transports ">
			  		<span id="transport{!!$transport->id!!}">
				    	<h1 class="heading-gray">{!!$transport->from!!} - {!!$transport->to!!}</h1>
				    	<p>	{!! Carbon\Carbon::parse($transport->date)->format('d.m.Y') !!} , {!!$transport->hour!!}</p> 
				    	<span class="trans-price">€ {!!$transport->price!!}</span>
				    	<p>{!!$transport->description!!}</p>
					</span>
			    </div>
		    </div>
	        @endforeach 
	    </div>
	</div>
</div>

@endsection