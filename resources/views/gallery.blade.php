@extends('layouts.app')

@section('content')
    @foreach($albums as $album)
    	<h2>{!!$album->name!!}</h2>
    	@foreach($album->photo as $photo)
		<img src="/gallery/{{ $photo->path }}"/>
		@endforeach
	@endforeach
@endsection