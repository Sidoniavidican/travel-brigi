<div class="tikets">
    <div class="contact modal-dialog pop-up-folder" role="document">
        <div class="modal-content">
            <div class="modal-header"> 
                <h3 class="heading-popup" id="exampleModalLabel">Trimite o cerere de oferta</h3> 
                <span class="silver small">Completati formularul de mai jos cu toate datele necesare. Campurile cu steluta (*) sunt obligatorii.</span>
            </div> 
            <div class="modal-body box box-form clearfix"> 
                <form action="/sendformrequest" method="post" id="cererepopup">  
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <input type="hidden" name="offer_id" id="id" value="{!!$offer->id!!}">
                    <span class="input input--hoshi">
                        <input class="input__field input__field--hoshi" type="text" name="name" id="input-1" />
                        <label class="input__label input__label--hoshi input__label--hoshi-color-1" for="name">
                            <span class="input__label-content input__label-content--hoshi silver
                            ">Nume &amp; Prenume *</span> 
                        </label>
                    </span>
                    <span class="input input--hoshi">
                        <input class="input__field input__field--hoshi" type="text" name="email" id="input-3" />
                        <label class="input__label input__label--hoshi input__label--hoshi-color-1" for="email"> 
                            <span class="input__label-content input__label-content--hoshi silver">Email *</span> 
                        </label>
                    </span>
                    <span class="input input--hoshi">
                        <input class="input__field input__field--hoshi" type="text" name="phone" id="input-4" />
                        <label class="input__label input__label--hoshi input__label--hoshi-color-1" for="phone"> 
                            <span class="input__label-content input__label-content--hoshi silver">Telefon *</span>
                        </label>
                    </span>
                    <span class="input input--hoshi">
                        <textarea class="input__field input__field--hoshi" type="text" name="message" id="input-5"></textarea>
                        <label class="input__label input__label--hoshi input__label--hoshi-color-1" for="avability">
                            <span class="input__label-content input__label-content--hoshi silver">Mesaj</span>
                        </label>
                    </span>

                  <div class="clearfix right margin-top-10">
                        <button type="button" class="btn btn-secondary gray-bg" data-dismiss="modal">Anuleaza</button>
                        <button  type="submit" class="btn btn-primary red-bg" data-toggle="modal" data-target="#requestmodal">Cere Oferta</button> 
                    </div>
                </form>
            </div> 
        </div>
    </div> 
</div>

  


 