@extends('layouts.app')
@section('content')

<div class="relative adm-margin">
<div class="admin-offer adm-offer-height">

	{{ Form::open(['method' => 'POST', 'route' => ['offers.store'], 'class'=>'dateRangeForm', 'id'=>'addOfferValidate']) }}
		{{ csrf_field() }}

		<div class="row margin-top-20">
			<div class="col-md-9 ">
	            <span class="label-adm">{{ Form::label('name', 'Titlu Vacanta:') }}</span>
	            <span class="filed-adm">{{ Form::text('name', null, ['required', 'maxlength'=>'80', 'id'=>'name']) }}</span>
	            <label id="name-error" class="hide">This field is required.</label>
			</div>

			<div class="col-md-3   important offer"> 
				{{ Form::label('important', 'Important:')}}
                <span class="filed-adm">{{ Form::select('important', ["Nu", "Da"], '0', ['class' => ''])}}</span>
		    </div>
		</div>

		<div class="col-ms-12 margin-top-20">  
            <span class="label-adm">{{ Form::label('description', 'Descriere:') }}</span> 
            <span class="filed-adm">{{ Form::textarea('description', null, ['rows' => 2, 'cols' => 40, 'maxlength'=>'1000']) }}</span>
		</div>
			
		<div class="row margin-top-20">
			<div class="col-md-4">
				<span class="label-adm">{{ Form::label('type[]', 'Tip:') }}</span>
				<span class="filed-adm">
					<select class="selectpicker type"  title="Selecteaza tipul" id="type" multiple name="type[]" required data-selected-text-format="count">
						@foreach($types as $type)
							@if($type->id == 0)
								 <optgroup class="accomodation">
							  		<option value="{!!$type->id!!}">{!!$type->name!!}</option>
							  	</optgroup>
						  	@else
							  	 <optgroup class="offers">
							  		<option value="{!!$type->id!!}">{!!$type->name!!}</option>
							  	</optgroup>
						  	@endif
						@endforeach
					</select>
				</span>
			</div>
		   <div class="col-md-4"> 
           		<span class="label-adm">{{ Form::label('country[]', 'Tara:') }}</span>
           		<span class="filed-adm">
           			<select class="selectpicker" id="country" title="Selecteaza tara" required multiple name="country[]" data-selected-text-format="count">
						@foreach($countries as $country)
						  	<option value="{!!$country->country!!}">{!!$country->country!!}</option>
						@endforeach
					</select>					
           		</span>             		
            </div>

			<div class="col-md-4" >
				<span class="label-adm" >{{ Form::label('', 'Hotel:') }}</span>
				<span class="filed-adm" id="selectHotel" name="selectHotel">
					<select class="selectpicker" id="hotel" multiple name="hotel[]" title="Selecteaza hotel"  data-selected-text-format="count">
						@foreach($hotels as $hotel)
						  	<option value="{!!$hotel->id!!}">{!!$hotel->name!!}</option>
						@endforeach		
					</select>				
				</span>
				<label id="hotel-error" class="hide" >This field is required.</label>
			</div>

		</div>
		
		<div class="row offer margin-top-20 discount hide">
			<div class="earlyBooking hide">
	            <div class="col-md-3">
                    <div class="input-group input-append date dateRangePicker">
				            {{ Form::label('EB_date', 'Sfarsit early booking:') }}     
					        <span class="filed-adm">{{ Form::text('EB_date', null, [ 'required', 'name'=>'EB_date']) }}</span>
			               	<span class="input-group-addon add-on"></span>
			        </div>
	            </div>
	            <div class="col-md-3">
	               		{{ Form::label('EB_discount', 'Discount early booking:') }}
		            	<span class="filed-adm">{{ Form::number('EB_discount', null, ['required','min'=>'1', 'max'=>'99']) }}</span>
	            </div>
   			</div>
   			<div class="lastMinute hide">          
	   			 <div class="col-md-3">
                    <div class="input-group input-append date dateRangePicker">
				            {{ Form::label('LM_date', 'Inceput last minute:') }}     
					            <span class="filed-adm">{{ Form::text('LM_date', null, ['required', 'name'=>'LM_date']) }}</span>
			               	<span class="input-group-addon add-on"></span>
			        </div>
	            </div>
	            <div class="col-md-3">
               		{{ Form::label('LM_discount', 'Discount last minute:') }}
	            	<span class="filed-adm">{{ Form::number('LM_discount', null, ['required', 'min'=>'1', 'max'=>'99']) }}</span>
	            </div>
	        </div>
        </div>
        


	    <div class="offer row">
	    	<div class="col-md-12 margin-top-20">
                <div class="row">
			<div class="col-md-4">               
	            <div class="input-group input-append date dateRangePicker">
		            {{ Form::label('begin_date', 'Data inceput:') }}     
			            <span class="filed-adm">{{ Form::text('begin_date', null, ['required', 'name'=>'begin_date']) }}</span>
	               	<span class="input-group-addon add-on"></span>
	            </div>
			</div>
			<div class="col-md-4">
			    <div class="input-group input-append date dateRangePickerEnd">
		            {{ Form::label('end_date', 'Data sfarsit:') }}     
			            <span class="filed-adm">{{ Form::text('end_date', null, ['required', 'name'=>'end_date']) }}</span>
	               	<span class="input-group-addon add-on"></span>
               	</div>
			</div>
			<div class="col-md-4">						
					{{ Form::label('days', 'Zile:') }}
    			<span class="filed-adm">{{ Form::number('days', null, ['required', 'min'=>'1', 'max'=>'30']) }}</span>
			</div>
				
		        </div> 
		    </div>	
	    </div>

	    <div class="row margin-top-20">
			<div class="col-md-3 offer">
                {{ Form::label('price', 'Pret:') }}
	            <span class="filed-adm">{{ Form::number('price', null, ['required', 'min'=>'5', 'max'=>'20000']) }}</span>
			</div>

			<div class="col-md-3 offer">
                {{ Form::label('currency', 'Valuta:') }}
	           	<span class="filed-adm">{{ Form::select('currency', ["Euro", "RON"], '0', ['class' => '']) }}</span>
			</div>

			<div class="col-md-6 contact offer">
                {{ Form::label('from', 'Plecare din:')}}
		        <span class="filed-adm">{{ Form::text('from', null, ['class' => '', 'maxlength'=>'80']) }}</span>
			</div>
		</div>

    	<div class="row  margin-top-20">
			<div class="col-md-3">   
				{{ Form::label('passport', 'Pasaport:') }}
               	<span class="filed-adm">{{ Form::select('passport', ["Nu", "Da"], '0', ['class' => '']) }}</span>
			</div>

			<div class="col-md-3">
	    		{{ Form::label('dinner', 'Masa:')}} 
		        <span class="filed-adm">{{ Form::select('dinner', ["Neinclusa", "Inclusa"], '1', ['class' => '']) }}</span>
			</div>	
		
			<div class="col-md-3 offer">
			    {{ Form::label('transport', 'Transport:')}}
               	<span class="filed-adm">{{ Form::select('transport', ["Neinclus", "Avion", "Autocar"], '0', ['class' => '']) }}</span>
			</div>

			<div class="col-md-3 ">
               {{ Form::label('confort', 'Confort:') }}
               <span class="filed-adm">{{ Form::select('confort',['1'=>'1', '2'=>'2', '3'=>'3', '4'=>'4', '5'=>'5'] , '0', ['class' => '', 'id'=>'confort']) }}</span>
			</div>	
		</div>

		<div class="row margin-top-20">
		  	<div class="col-md-4">      
               {{ Form::label('observation', 'Facilitati:') }}
               <span class="filed-adm">{{ Form::textarea('observation', null, ['class'=>'form-control', 'rows' => 2, 'cols' => 40, 'maxlength'=>'1000']) }}</span>
            </div>
            <div class="col-md-4">      
               {{ Form::label('includes', 'Include:')}}
               <span class="filed-adm">{{ Form::textarea('includes', null, ['class'=>'form-control', 'rows' => 2, 'cols' => 40, 'maxlength'=>'1000']) }}</span>
            </div>
            <div class="col-md-4">      
               {{ Form::label('notIncludes', 'Nu include:')}}
               <span class="filed-adm">{{ Form::textarea('notIncludes', null, ['class'=>'form-control', 'rows' => 2, 'cols' => 40, 'maxlength'=>'1000']) }}</span>
            </div>
        </div>
	    <div class="fixed-footer">
		    <div class="relative-footer">

	        <div class="margin-top-20 right-btn anuleaza">

	        	<div class="col-sm-6">
					<a href="/admin" class="btn gray"> Anuleaza</a> 
				</div>		
	            <div class="col-sm-6">	
					{{ Form::submit('Continua', ['class' => 'btn btn-primary addOffer']) }}
				</div>

			</div>
		</div>
	</div>
	{{ Form::close() }}


	@if($errors->any())
	
		        @foreach($errors->all() as $error)
		            <p>{{ $error }}</p>
		        @endforeach

	@endif

	<div class="margin-top-20"> 
			<label > Adauga Poze: </label> 
		        {{ Form::open(['method' => 'POST', 'route' => ['store-photos'], 'class' => 'dropzone', 'id' => 'update']) }}
				{{ csrf_field() }}
				<input name="id" value="temporary" type="hidden" id="id">		      
			{{ Form::close() }}
	</div>

</div> 

</div>
@endsection