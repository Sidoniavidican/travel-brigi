<table>
	<thead>
		<tr>
			<th>Perioada</th>
				<th>Single/zi</th>
				<th>Double/Zi</th>
				<th>Twin/Zi</th>
				<th>Triple/Zi</th>
		</tr>
	</thead>
	@foreach($offer->periods as $period)			
		<tr>
						     	<td  data-title="Perioada: " class="col">{!! Carbon\Carbon::parse($period->begin_date)->format('d.m.Y') !!} - {!! Carbon\Carbon::parse($period->end_date)->format('d.m.Y') !!}</td>
						     	<td data-title="Single/zi: " class="col-tr">
						              	@if($period->currency==0)
						              			&#8364;
						              	@endif
						              		{!!$period->single!!}
						              	@if($period->currency==1)
						              			RON
						              	@endif						              		
						     	</td>
						     	<td data-title="Double/zi: " class="col-tr">
						     		@if($period->currency==0)
						              			&#8364;
						       		@endif
						              		{!!$period->double!!}
						            @if($period->currency==1)
						              			RON
						            @endif	
						     	</td>
						     	<td data-title="Twin/zi: " class="col-tr">
						     	    @if($period->currency==0)
						              			&#8364;
						            @endif
						              		{!!$period->twin!!}
						            @if($period->currency==1)
						              			RON
						            @endif	

						     	</td>
						     	<td data-title="Triple/zi: " class="col-tr">
						     	    @if($period->currency==0)
						              			&#8364;
						            @endif
						              		{!!$period->triple!!}
						            @if($period->currency==1)
						              			RON
						            @endif	
						     	</td>
		</tr>
	@endforeach 
</table>