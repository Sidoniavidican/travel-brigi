@extends('layouts.app')
@section('content')

@if (Session::has('offer-success'))
    <p class="offer-success" name="{{ Session::get('offer-success') }}"></p>
@endif

<div class="relative adm-margin">  	
    <div class="admin-offer admin-offer-simple clearfix">
		{{ Form::open(['method' => 'POST', 'route' => ['periods.store', $offer->id], 'class'=>'dateRangeForm', 'id' => 'addPerriodvalidate']) }}

		{{ csrf_field() }}
			<input type="hidden" name="offer_id" value="{!!$offer->id!!}">
			<div class="row">
				<div class="col-sm-6 margin-top-20">
				    <div class="input-group input-append date dateRangePicker">
					    {{ Form::label('begin_date_period', 'Data de inceput:') }}     
						<span class="filed-adm">{{ Form::text('begin_date', null, ['required', 'name'=>'begin_date', 'id' => 'begin_date']) }}</span>
				        <span class="input-group-addon add-on"></span>
				    </div>
				</div> 
                <div class="col-sm-6 margin-top-20">
					<div class="input-group input-append date dateRangePickerEnd">
					    {{ Form::label('end_date_period', 'Data de sfarsit:') }}     
						<span class="filed-adm">{{ Form::text('end_date', null, ['required', 'name'=>'end_date', 'id' => 'end_date']) }}</span>
				        <span class="input-group-addon add-on"></span>
				    </div>
				</div>
            </div>
            <div class="row">
	        	<div class="col-sm-6 margin-top-20">
					<label>Single Pret</label>
					<span class="filed-adm"><input type="number" name="single" required id="single" max="20000" min="5"></span> 
				</div>
				<div class="col-sm-6 margin-top-20">
					<label>Double Pret</label>
					<span class="filed-adm"><input type="number" name="double" required id="double" max="20000" min="5"></span> 
				</div>
			</div>
            
            <div class="row">
                <div class="col-sm-6 margin-top-20">
					<label>Twin Pret</label>
					<span class="filed-adm"><input type="number" name="twin" required id="twin" max="20000" min="5"> </span>
				</div>
                <div class="col-sm-6 margin-top-20">
					<label>Triple Pret</label>
					<span class="filed-adm"><input type="number" name="triple" required id="triple" max="20000" min="5"></span> 
				</div>
			</div>

			<div class="row">
				<div class="col-sm-3 margin-top-20">
	                {{ Form::label('currency', 'Valuta:') }}
		           	<span class="filed-adm">{{ Form::select('currency', ["Euro", "RON"], '0', ['required', 'class' => '']) }}</span>
	           	</div>
			</div>

			
			    {{ Form::submit('Salveaza', ['class' => 'btn btn-primary right-btn small-btn2  addPeriod']) }}
			    <a href="/admin/offers" class="btn btn-primary right-btn small-btn2 m-right">Skip</a>		 

		{{ Form::close() }}


    </div>
</div>
@endsection