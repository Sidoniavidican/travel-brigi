@extends('layouts.app')

@section('content')

<div class="container">	
    <div class="box-offer clearfix"> 
	    <div id="main_area" class="area-slider">
	        <div id="slider">
	            <div class="row">
	                <div class="col-xs-12 col-sm-12" id="carousel-bounding-box">
	                    <div class="carousel slide" id="myCarousel">
	                        <div class="carousel-inner">
								@if(isset($offer->photos[0]))
					                @foreach($offer->photos as $photo)
					                    @if($photo->id == $offer->photos[0]->id)
					                        <div class="active item" data-slide-number="{!! $loop->index !!}" style="background-image:url('/images/offers/{{ $offer->id }}/{{ $photo->path }}')">
					                        </div>
					                    @else
					                        <div class="item" data-slide-number="{!! $loop->index !!}" style="background-image:url('/images/offers/{{ $offer->id }}/{{ $photo->path }}')">
					                        </div>
					                    @endif
					                @endforeach
					            @else
					                <div class="active item" style="background-image:url('/images/noimagehotel.png')">
					                </div>
	                            @endif	
	                            <div class="title-content"> 
						              <h1 class="title-h1">{!! $offer->name !!}</h1>
						              @if($offer->type != ',0,')
						             	 <p class="p-date">{!! Carbon\Carbon::parse($offer->begin_date)->format('d.m.Y') !!} - {!! Carbon\Carbon::parse($offer->end_date)->format('d.m.Y') !!} - {!!$offer->days!!} zile</p>
						              @endif
									  <span class="white details-loc"><span class="icon-Location"></span>Locatia: {!!str_replace(',', ', ', $offer->country)!!}</span>
						              @if($offer->from != null)
										<span class="white details-loc">
										<span class="icon-Transport_Avion"></span> Pleacare din: {!!$offer->from!!}</span>
									  @endif    
							    </div>
							    <div class="right-content">

									@foreach(explode(',', $offer->type) as $type) 
										@if($type>0)
											@if($types[$type]->id == '1' && Carbon\Carbon::now() > Carbon\Carbon::parse($offer->LM_date))
									    	 	<div class="box-white">
									        		<p>{{$types[$type]->name}}</p>
									        	</div>
									        @elseif($types[$type]->id == '2' && Carbon\Carbon::now() < Carbon\Carbon::parse($offer->EB_date))
								    	 		<div class="box-white">
								        			<p>{{$types[$type]->name}}</p>
								        		</div>
								    	 	@endif

								        	@if( !($types[$type]->id == '1' || $types[$type]->id == '2'))
											   	<div class="box-white">
									        		<p>{{$types[$type]->name}}</p>
									        	</div>
									        @endif
										@endif
									@endforeach
				

							    </div> 
							    @if($offer->type != ',0,')
									@include('offer.offerPrice')
							    @endif  
	                        </div>                               
	                    </div>
	                </div>
	                <div class="col-sm-4" id="carousel-text"></div>
	            </div>
	        </div><!--/Slider-->
	        <div class="bottom-icons">
				<div class="icons">
					<ul>
					<li class="right-corder-container">				
					    <button class="right-corder-container-button green-bg">
					        <span class="short-text"><span class="icon-Phone"></span></span>
					        <span class="long-text"><a href="tel:0745 476 451" class="white small">0745 476 451</a></span> 
					    </button>
					</li>
					<li class="right-corder-container">				
					    <button class="right-corder-container-button blue">
					        <span class="short-text"><span class="blue icon-Share"></span></span>
					        <span class="long-text">
					        <a href="https://www.facebook.com/sharer/sharer.php?u=http://brigittetravel.ro/offer/{!!$offer->id!!}" style="padding: 0 7px;" target="_blank">Share</a> 
					        </span>
					    </button>
					</li>

					<li class="right-corder-container">		
						<a href="" class="white" data-toggle="modal" data-target="#cerere">		
						<button class="right-corder-container-button right-corder-big">
							<span>
								Cerere Oferta 
							</span> 
						</button>
						</a>
					</li>
                        <div class="modal fade" tabindex="-1" role="dialog" id="cerere" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            @include('cerere') 
                        </div>
					</ul>
				</div>
			</div> 
	        <div class="clearfix" id="slider-thumbs">
	            <ul class="hide-bullets">
	                @if(isset($offer->photos[0]))
	            	   @foreach($offer->photos as $photo)
						    <li class="box-photo">
						        <a class="thumbnail" id="carousel-selector-{!! $loop->index !!}"><img src="/images/offers/{{ $offer->id }}/{{ $photo->path }}"/>
			                    </a>
		                    </li>  
					    @endforeach
					 @endif	
	                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
	                	<span class="icon-ArrowLeft"></span>                                       
	                </a>
	                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
	                	<span class="icon-ArrowRight"></span>
	                </a> 
	            </ul>
	        </div>
	    </div>
	    <div class="inner-options">
	        <div class="options clearfix">
		        <div class="col-sm-3 center">
				    <ul>
				        <li>
						@if($offer->transport == '0')
							<span class="icn icon-Transport_Autocar"></span> Transport: 
					        	<span class="red"> Nu </span> 
					        @elseif($offer->transport == '1')
					         	<span class="icn icon-Transport_Avion"></span> Transport: 
					        	 <span class="green"> Da  </span>
					        @else($offer->transport == '2')
					    		<span class="icn icon-Transport_Autocar"></span> Transport: 
					        	 <span class="green"> Da  </span>
					        @endif
				        </li>
				    </ul>
			    </div>
				<div class="col-sm-3 center">
				    <ul>
						<li><span class="icn icon-Cazare"></span> Confort:<span class="yell">
								@for($i = 1; $i <= $offer->confort; $i++)
									<span class="icon-Star"></span> 
								@endfor
						</li>
				    </ul>
				</div>
				<div class="col-sm-3 center">
				    <ul>
						<li>
							<span class="icn icon-Pasaport"></span> Pasaport: 
							@if($offer->passport == '1')
					        	 <span class="green"> Da  </span>
					        @else 
					        	<span class="red"> Nu </span> 
					        @endif
						</li>
				    </ul>
				</div>
				<div class="col-sm-3 center">
				    <ul>
						<li>
							<span class="icn icon-Masa"></span> Masa: 
							@if($offer->dinner == '1')
					        	 <span class="green"> Da  </span>
					        @else 
					        	<span class="red"> Nu </span> 
					        @endif
						</li>
				    </ul>
				</div>
			</div>
			<div class="description-box">
			    @if($offer->description != null)
					<p class="regular p-bott">{!! $offer->description !!}</p>
				    <div class="line-bottom"></div>
				@endif
				
				@if(isset($offer->offerPeriods[0]))
					@include('offer.offerPeriods')
				@endif	

				@if(isset($offer->offerDays[0]))
					@foreach($offer->offerDays->sortBy('day') as $day)
					<div class="days">
						<div>
							<span class="day-style">Ziua {!!$day->day!!}:</span> <p class="regular">{!!$day->description!!}</p>
							<div class="line-bottom"></div>
						</div>
					 </div>
					@endforeach
				@endif	
	               	               
				@if(isset($offer->periods[0]))
					@include('offer.periods')
				@endif	

				@include('offer.offerTabs')
			</div>
	    </div>
    </div>	
</div> 

	
@endsection