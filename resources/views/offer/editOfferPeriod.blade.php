@extends('layouts.app') 
@section('content')
<div class="relative adm-margin">  	
    <div class="admin-offer admin-offer-simple clearfix">
		<h2>Edit period for: {!!$offer_period->offer->name!!}</h2>
		{{ Form::open(['method' => 'PATCH', 'route' => ['offer-periods.update', $offer_period->id], 'class'=>'dateRangeForm', 'id' => 'addPeriodValidate']) }}
					{{ csrf_field() }}
					<input type="hidden" name="id" value="{!!$offer_period->id!!}">
					<div class="row margin-top-30">			
						<div class="col-sm-6 margin-top-20">
        				    <div class="input-group input-append date dateRangePicker">
        					    {{ Form::label('begin_date', 'Data de inceput:') }}     
        						<span class="filed-adm">
        							<input type="text" name="begin_date" id="begin_date" required value="{{$offer_period->begin_date}}">
        						</span>
        				        <span class="input-group-addon add-on"></span>
        				    </div>
        				</div> 
                        <div class="col-sm-6 margin-top-20">
        					<div class="input-group input-append date dateRangePickerEnd">
        					    {{ Form::label('end_date', 'Data de sfarsit:') }}     
        						<span class="filed-adm">
        							<input type="text" name="end_date" id="end_date" required value="{{$offer_period->end_date}}">
        						</span>
        				        <span class="input-group-addon add-on"></span>
        				    </div>
        				</div>
	                    <div class="col-sm-6 margin-top-20">
							<label>Single Price</label>
							<span class="filed-adm"><input type="number" name="price"  id="price" required value="{{$offer_period->price}}"></span>
						</div>
						<div class="col-sm-6 margin-top-20">
					             {{ Form::label('currency', 'Valuta:') }}
				           	<span class="filed-adm">{{ Form::select('currency', ["Euro", "RON"], $offer_period->currency, ['required', 'class' => '']) }}</span>
						</div>
					</div>

			{{ Form::submit('Edit', ['class' => 'btn btn-primary right-btn small-btn margin-top-20 addPeriod']) }}		 
		{{ Form::close() }}
		</div>
	</div>
@endsection