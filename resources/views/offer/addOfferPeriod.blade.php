@extends('layouts.app')
@section('content')

@if (Session::has('offer-success'))
    <p class="offer-success" name="{{ Session::get('offer-success') }}"></p>
@endif

<div class="relative adm-margin">  	
    <div class="admin-offer admin-offer-simple clearfix">
		{{ Form::open(['method' => 'POST', 'route' => ['offer-periods.store', $offer->id], 'class'=>'dateRangeForm', 'id' => 'addOfferPerriodvalidate']) }}

		{{ csrf_field() }}
			<input type="hidden" name="offer_id" value="{!!$offer->id!!}">
			<div class="row  section-off-period">
				<div class="col-sm-6 margin-top-20">
				    <span class="input-append date dateRangePicker">
						<label>Begin date</label>
						<span class="filed-adm">
							<input type="text" name="begin_date" required id="begin_date">
							<span class="input-group-addon add-on"></span>
						</span> 
					</span>
				</div>
                 
                <div class="col-sm-6 margin-top-20">
				    <span class="input-append date dateRangePickerEnd">
						<label>End date</label>
						<span class="filed-adm"><input type="text" name="end_date" required id="end_date"></span> 
						<span class="input-group-addon add-on"></span>
					</span>
				</div>
            </div>

            <div class="row">
	        	<div class="col-sm-6 margin-top-20"> 
					<label>Price</label>
					<span class="filed-adm"><input type="number" name="price" required id="price"></span> 
				</div>
				<div class="col-sm-6 margin-top-20">
	                {{ Form::label('currency', 'Valuta:') }}
		           	<span class="filed-adm">{{ Form::select('currency', ["Euro", "RON"], '0', ['required', 'class' => '']) }}</span>
	           	</div>
			</div>
            

            <div class="margin-top-20">
			    {{ Form::submit('Salveaza', ['class' => 'btn btn-primary right-btn small-btn2  addPeriod']) }}
			    <a href="/admin/offers" class="btn btn-primary right-btn small-btn2 m-right">Skip</a>
            </div>				

		{{ Form::close() }} 

		
    </div>
</div>
@endsection