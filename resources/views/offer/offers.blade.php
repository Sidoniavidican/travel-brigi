@extends('layouts.app')
@section('title') Oferte @stop 
@section('description') Brigitte Travel | Oferte @stop 
@section('content')  


@include('advanceSearch')

<div class="container">
    <div class="inner-space">
    <div class="content bottom-section relative clearfix">
    <div class="grid">
   		<input type="hidden" name="maxPrice" value="{!!$maxPrice!!}" id="maxPret">
           @foreach($offers as $offer) 
				@if($offer->type != ',0,')
                    @if(Request::is('early-booking'))
						@if(Carbon\Carbon::parse($offer->EB_date) > Carbon\Carbon::now())
							@include('offer.offersPartial')
						@endif
					@elseif(Request::is('last-minute'))
						@if(Carbon\Carbon::parse($offer->LM_date) < Carbon\Carbon::now())
							@include('offer.offersPartial')
						@endif
					@else			
						@include('offer.offersPartial')
					@endif

	            @elseif(isset($offer->periods[0]))
	           	<div class="col-sm-6 col-md-4" id="offer{!!$offer->id!!}">
	                <div class="inner-content">
	                    <h1>
	                    	<a href="/oferta/{!!$offer->id!!}">
					 {!! str_limit($offer->name, 20)!!}
				</a>
			    </h1>
	         	    @if(isset($offer->periods[0]))        
	                    	<p class="white">
	                    		{!!Carbon\Carbon::parse($offer->periods[0]->begin_date)->format('d.m.Y')!!} -
	                    		{!!Carbon\Carbon::parse($offer->periods[0]->end_date)->format('d.m.Y')!!}  - 
	                    		de la {!!$offer->periods[0]->single!!}
	                    	</p>
	                    @endif
	                   	@if(isset($offer->periods[1]))           
	                    	<p class="white">
	                    		{!!Carbon\Carbon::parse($offer->periods[1]->begin_date)->format('d.m.Y')!!} -
	                    		{!!Carbon\Carbon::parse($offer->periods[1]->end_date)->format('d.m.Y')!!}  - 
	                    		de la {!!$offer->periods[1]->single!!}
	                    	</p>
	                    @endif
	                    @if(isset($offer->periods[2]))           
	                    	<p class="white">...</p>
	                    @endif
	                   	<div class="inner-right-text">
		                    <div class="right-text"><span>Cazare<span></div>
	                    </div>
	                </div> 
	                <figure class="effect-julia">                    
	                    @if(isset($offer->photos[0]))
							<img src="/images/offers/{{ $offer->id }}/{{ $offer->photos[0]->path }}"/>
						@else 
							<img src="/images/poza3.jpg" alt="img21"/>
						@endif	
	                    <figcaption>
	                        <div>
								<p class="white">{!! str_limit($offer->description, 240)!!}></p>
	                        </div>
	                    </figcaption>           
	                </figure>
	                <div class="inner-content-bottom">
	                </div> 
	            </div>
	            @endif
            @endforeach
         </div>
        </div>
    </div>
</div>


@endsection 