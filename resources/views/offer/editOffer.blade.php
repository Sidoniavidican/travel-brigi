@extends('layouts.app')
@section('content')
  
<div class="relative adm-margin">  	
	<div class="admin-offer clearfix">
		<a href="/admin/offers/create">
		    <button type="button" class="yellow-btn">
				<span class="icon-Plus" aria-hidden="true"></span> 
			</button>
		</a>

		@if($offer->type == ',0,' )
			@include('offer.editAccomodation')
		@else
			@include('offer.editOfferPartial')
		@endif

	    <div class="margin-top-30">
			<label> Poze: </label>
			<div class= "row margin-top-20">
			    @foreach($offer->photos as $photo)
			    	<div class="col-md-3" id="photo{!!$photo->id!!}">
						{{ Form::submit('Sterge', ['class' => 'btn btn-danger deletePhoto', 'id' => $photo->id]) }}
						<img src="/images/offers/{{ $offer->id }}/{{$photo->path}}" width = "280px" height = "180px">
					</div>
				@endforeach		
			</div>

		    <div class="margin-top-20">
				<label > Adauga Poze: </label> 
			    <form action = "/store-photos"  method="POST" class="dropzone" id="update">
					{{ csrf_field() }}
					<input name="id" value="{!!$offer->id!!}" type="hidden" id="id">		      
				</form> 
		    </div>
	    </div>
	</div>
</div>

@stop

