 <div class="offer-price-section">
	 <div class="content-bottom">
	    <div class="padding"> 
			<div class="white price">
				@if(
				   ($offer->EB_discount!=null && Carbon\Carbon::now() < Carbon\Carbon::parse($offer->EB_date))
			 	|| ($offer->LM_discount!=null && Carbon\Carbon::now() > Carbon\Carbon::parse($offer->LM_date))
				  )
					<div class="prices">
								@if(isset($offer->offerPeriods[0])) 
							    	<span class="dela">de la</span>
							    @endif
								<span class="new-price">
									@if($offer->currency==0)
												&#8364;
									@endif
									@if(Carbon\Carbon::now() > Carbon\Carbon::parse($offer->LM_date))
										{!!(int)($offer->price * (100-$offer->LM_discount) /100) !!}
									@else
										{!!(int)($offer->price * (100-$offer->EB_discount) /100) !!}
									@endif
									@if($offer->currency==1)
												RON
								    @endif	
								</span>

				  		<span class="initial-price">
							<strike>
								@if($offer->currency==0)
									&#8364;
								@endif
								{!!$offer->price!!}
								@if($offer->currency==1)
									RON
								@endif	
							</strike> 
						</span>
					</div>
					<div class="prices">
					    @if(Carbon\Carbon::now() > Carbon\Carbon::parse($offer->LM_date))
					     	<div class="discount">
								{!!$offer->LM_discount!!}% reducere
						</div>
	
					    @else
						<div class="discount">
								{!!$offer->EB_discount!!}% reducere
						</div>
					   @endif		
				    </div>
				@else
					<span class="initial-price-single">

							@if(isset($offer->offerPeriods[0])) 
							    	<span class="dela">de la</span>
							    @endif
							    
							@if($offer->currency==0)
								&#8364;
							@endif
							{!!$offer->price!!}
							@if($offer->currency==1)
								RON
							@endif	
					</span>

				@endif		
			</div>
	    </div>
	</div>
</div> 