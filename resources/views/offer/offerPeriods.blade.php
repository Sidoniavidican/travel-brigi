<table class="period-offer">
	<thead>
		 <tr>
			<th>Perioada</th>
			 <th>Pret/zi</th>
			 <th> Discount</th>
		</tr>
	</thead>
	@foreach($offer->offerPeriods->reverse() as $period)			
		<tr class="tr-period">
			<td  data-title="Perioada: ">
				{!! Carbon\Carbon::parse($period->begin_date)->format('d.m.Y') !!} - {!! Carbon\Carbon::parse($period->end_date)->format('d.m.Y') !!}
			</td>
			<td data-title="Pret/zi: ">

				@if($period->offer->EB_discount!=null || $offer->LM_discount!=null)
		           	@if($period->offer->EB_discount!=null && Carbon\Carbon::now() <= Carbon\Carbon::parse($period->offer->EB_date))
						@if($period->currency==0)
							&#8364;
						@endif
							{!!(int)($period->price * (100-$offer->EB_discount) /100) !!}
							<strike>{!!$period->price!!}</strike>
						@if($period->currency==1)
							RON
			           	@endif	
						
					@elseif($period->offer->LM_discount!=null && Carbon\Carbon::now() >= Carbon\Carbon::parse($period->offer->LM_date))
						@if($period->currency==0)
							&#8364;
						@endif
							{!!(int)($period->price * (100-$offer->LM_discount) /100) !!}
							<strike>{!!$period->price!!}</strike>
						@if($period->currency==1)
							RON
			           	@endif	
			        @else
			        	@if($period->currency == 0)
							&#8364;
						@endif
							{!!$period->price!!}
						@if($period->currency==1)
							RON
			           	@endif	
			        @endif
				@else
						@if($period->currency == 0)
							&#8364;
						@endif
							{!!$period->price!!}
						@if($period->currency==1)
							RON
			           	@endif	

	           	@endif	         		
			</td>
			<td data-title="Discount: " >
				@if($period->offer->EB_discount!=null || $period->offer->LM_discount!=null)
					@if($period->offer->EB_discount!=null && Carbon\Carbon::now() <= Carbon\Carbon::parse($period->offer->EB_date))
						{!!$offer->EB_discount!!} %
						Early booking	
					@elseif($period->offer->LM_discount!=null && Carbon\Carbon::now() >= Carbon\Carbon::parse($period->offer->LM_date))
						{!!$offer->LM_discount!!} %
						Last Minute	
					@else
						-
					@endif	
				@else
					-	
				@endif
			</td>
		</tr>
	@endforeach 
</table>