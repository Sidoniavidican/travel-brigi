 {{ Form::open(['method' => 'PATCH', 'route' => ['offers.update', $offer->id], 'class'=>'dateRangeForm', 'id' => 'editOfferValidate']) }}
	    {{ csrf_field() }}
		{{ Form::hidden('id', $offer->id) }} 
		<div class="row margin-top-20">
	        <div class="col-sm-9 ">
				<span class="label-adm">{{ Form::label('name', 'Titlu:') }}</span>
				<span class="filed-adm">{{ Form::text('name', $offer->name, ['required', 'maxlength'=>'80']) }}</span>
			</div>

			<div class="col-sm-3 important">					
				<span class="label-adm">{{ Form::label('important', 'Important:')}}</span>
				<span class="filed-adm">{{ Form::select('important', ["Nu", "Da"], $offer->important, ['class' => '']) }}</span>
			</div>
        </div>

        <div class="margin-top-20">
			<span class="label-adm">{{ Form::label('description', 'Descriere:') }}</span>
			<span class="filed-adm">{{ Form::textarea('description', $offer->description, ['class'=>'form-control', 'rows' => 2, 'cols' => 40]) }}</span>
		</div> 

        <div class="margin-top-20">
	        <div class="row"> 
		        <div class="col-sm-4">
					<span class="label-adm">{{ Form::label('type[]', 'Tip:')}}</span>
					<span class="filed-adm">
						<select class="selectpicker" id="type" title="Selecteaza Tipul" multiple name="type[]" required data-selected-text-format="count">
							@foreach($types as $type)
								@if($type->id == 0)					
								@elseif(strpos($offer->type, ','.(string)($type->id).',' )!== false)
							  		<option selected value="{!!$type->id!!}">{!!$type->name!!}</option>
							  	@else
							  		<option value="{!!$type->id!!}">{!!$type->name!!}</option>
							  	@endif
							@endforeach
						</select>
					</span>
				</div>

	        	<div class="col-sm-4">
					<span class="label-adm">{{ Form::label('country[]', 'Tara:')}}</span>
					<span class="filed-adm">
			   			<select class="selectpicker" id="country"  title="Selecteaza Tara" multiple name="country[]" required data-selected-text-format="count">
							@foreach($countries as $country)
								@if(strpos($offerCountries, ','.(string)($country->id).',' )!== false)
							  		<option selected value="{!!$country->country!!}">{!!$country->country!!}</option>
							  	@else
							  		<option value="{!!$country->country!!}">{!!$country->country!!}</option>
							  	@endif
							@endforeach
						</select>
					</span>
				</div>

				<div class="col-sm-4">
					<span class="label-adm">{{ Form::label('hotel[]', 'Hotel:') }}</span>
					<span class="filed-adm">
						<select class="selectpicker" id="hotel"  title="Selecteaza Hotel" multiple name="hotel[]" data-selected-text-format="count">
							@foreach($hotels as $hotel)
								@if(strpos($offerHotels, ','.(string)($hotel->id).',' )!== false)
							  		<option selected value="{!!$hotel->id!!}">{!!$hotel->name!!}</option>
							  	@else
							  		<option value="{!!$hotel->id!!}">{!!$hotel->name!!}</option>
							  	@endif
							@endforeach
						</select>
					</span>
				</div>
			</div>
			<div class="row offer margin-top-20 discount hide">
				<div class="earlyBooking hide">
		            <div class="col-md-3">
		                    <div class="input-group input-append date dateRangePicker">
						            {{ Form::label('', 'Sfarsit early booking:') }}     
							            <span class="filed-adm">{{ Form::text('EB_date', $offer->EB_date, ['required', 'name'=>'EB_date', 'id'=>'EB_date']) }}</span>
					               	<span class="input-group-addon add-on"></span>
					        </div>
		            </div>
		            <div class="col-md-3">
		               		{{ Form::label('', 'Discount early booking:') }}
			            	<span class="filed-adm">{{ Form::number('EB_discount', $offer->EB_discount, ['required', 'min'=>'1', 'max'=>'99']) }}</span>
		            </div>
	   			</div>
	   			<div class="lastMinute hide">          
		   			 <div class="col-md-3">
		                    <div class="input-group input-append date dateRangePicker">
						            {{ Form::label('', 'Inceput last minute:') }}     
							            <span class="filed-adm">{{ Form::text('LM_date', $offer->LM_date, ['required', 'name'=>'LM_date', 'id'=>'LM_date']) }}</span>
					               	<span class="input-group-addon add-on"></span>
					        </div>
		            </div>
		            <div class="col-md-3">
		               		{{ Form::label('', 'Discount last minute:') }}
			            	<span class="filed-adm">{{ Form::number('LM_discount', $offer->LM_discount, ['required', 'min'=>'1', 'max'=>'99']) }}</span>
		            </div>
		        </div>
	        </div>
	

		</div> 
        
		<div class="margin-top-20">	
		    <div class="row">
				<div class="col-md-4">               
			        <div class="input-group input-append date dateRangePicker">
				        {{ Form::label('begin_date', 'Data inceput:') }}     
					        <span class="filed-adm">{{ Form::text('begin_date', $offer->begin_date, ['required', 'name'=>'begin_date']) }}</span>
			               	<span class="input-group-addon add-on"></span>
			        </div>
				</div>
				<div class="col-md-4">
						<div class="input-group input-append date dateRangePickerEnd">
					        {{ Form::label('end_date', 'Data sfarsit:') }}     
						     <span class="filed-adm">{{ Form::text('end_date', $offer->end_date, ['required', 'name'=>'end_date']) }}</span> 
				        <span class="input-group-addon add-on"></span>
			        </div>
				</div>
				<div class="col-md-4">						
 						{{ Form::label('days', 'Zile:') }}
	            		<span class="filed-adm">{{ Form::number('days', $offer->days, ['required', 'min'=>'1', 'max'=>'30']) }}</span>
				</div>
		    </div> 
		</div>

	    <div class="margin-top-20">
		    <div class="row">
				<div class="col-md-3 offer">
		            <span class="label-adm">{{ Form::label('price', 'Pret:')}}</span>
			        <span class="filed-adm">{{ Form::number('price', $offer->price, ['required', 'max'=>20000, 'min'=>10] ) }}</span>
				</div>

				<div class="col-md-3 offer">
		            <span class="label-adm">{{ Form::label('currency', 'Valuta:') }}</span>
			        <span class="filed-adm">{{ Form::select('currency', ["Euro", "RON"], $offer->currency, ['class' => '']) }}</span>
				</div>
				<div class="col-sm-6">
					<span class="label-adm">{{ Form::label('from', 'Plecare:')}}</span>
					<span class="filed-adm">{{ Form::text('from', $offer->from, ['maxlength'=>'80']) }}</span>
				</div>
			</div>
		</div>

		<div class="margin-top-20">
	        <div class="row">
	            <div class="col-sm-3">
		            <span class="label-adm">{{ Form::label('passport', 'Pasaport:') }}</span>
					<span class="filed-adm">{{ Form::select('passport', ["Nu", "Da"], $offer->passport, ['class' => '']) }} </span>
				</div>

                <div class="col-sm-3">
					<span class="label-adm">{{ Form::label('dinner', 'Masa:')}}</span>
					<span class="filed-adm">{{ Form::select('dinner', ["Neinclusa", "Inclusa"], $offer->dinner, ['class' => '']) }}</span> 
                </div>

                <div class="col-sm-3">
					<span class="label-adm">{{ Form::label('transport', 'Transport:')}}</span>
					<span class="filed-adm">{{ Form::select('transport', ["Neinclus", "Avion", "Autocar"], $offer->transport, ['class' => '']) }}</span> 
				</div>

                <div class="col-sm-3">
					<span class="label-adm">{{ Form::label('confort', 'Confort:') }}</span>
			    	<span class="filed-adm">{{ Form::select('confort', ['1'=>'1', '2'=>'2', '3'=>'3', '4'=>'4', '5'=>'5'], $offer->confort, ['class' => '']) }}</span>
		        </div>
			</div>
		</div>

		<div class="margin-top-20">
	        <div class="row">
	            <div class="col-sm-4">				
					<span class="label-adm">{{ Form::label('observation', 'Facilitati:') }}</span>
					<span class="filed-adm">{{ Form::textarea('observation', $offer->observation, ['class'=>'form-control', 'rows' => 2, 'cols' => 40]) }}</span> 
				</div>

                <div class="col-sm-4">
				    <span class="label-adm">{{ Form::label('includes', 'Include:')}}</span>
					<span class="filed-adm">{{ Form::textarea('includes', $offer->includes, ['class'=>'form-control', 'rows' => 2, 'cols' => 40]) }}</span>
			    </div>

                <div class="col-sm-4">
			    	<span class="label-adm">{{ Form::label('notIncludes', 'Nu include:')}}</span>
			     	<span class="filed-adm">{{ Form::textarea('notIncludes', $offer->notIncludes, ['class'=>'form-control', 'rows' => 2, 'cols' => 40]) }}</span>
				</div>
			</div> 
		</div>

	    <div class="fixed-footer edit">
		    <div class="relative-footer">
				<div class="margin-top-20 col-sm-2 right-btn">			
					{{ Form::submit('Salveaza', ['class' => 'btn btn-primary edit-offer']) }}
				</div>
			</div>
		</div>
			 
	{{ Form::close() }}
		
	@if($errors->any())
		    <div class="alert alert-danger">
		        @foreach($errors->all() as $error)
		            <p>{{ $error }}</p>
		        @endforeach
		    </div>
	@endif
    
    <div class="panel panel-days">	
	    <div class="panel-heading">
	      <div class="panel-title">
	        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
	          <label>Zile  <span class="icon-ArrowDown"></span></label>
	        </a>
	      </div>
	    </div> 

        <div id="collapseTwo" class="panel-collapse collapse">   
	        <div class="panel-body">
		        <div class="row">
					<div class="col-sm-12 clearfix margin-top-20">
						@if(isset($offer->offerDays[0]))
							@foreach($offer->offerDays->sortBy('day') as $day)
						        <label>Ziua{{$day->day}}</label>
						        <p>{{ $day->description }}</p> 
							@endforeach
							<a href="/admin/offers/{{$offer->id}}/days/edit" class="btn btn-primary right-btn">Editeaza zilele</a> 

							{{ Form::open(['method' => 'DELETE', 'route' => ['days.destroy', $offer->id]]) }}
				    			{{ csrf_field() }}
				    			<input type="hidden" value="{{$offer->id}}" name="id">
				    			<input type="submit" value="Sterge zilele" class="btn btn-primary small-btn m-right right-btn" style="padding: 5px;">
				    		{{ Form::close() }}
						@else
							<a href="/admin/offers/{{$offer->id}}/days/create" class="btn btn-primary right-btn ">Adauga descrierea zilelor</a>
						@endif
					</div>
			    </div>
	        </div>
	    </div>
    </div>

	<div class="row">
		<div class="col-sm-12 clearfix margin-top-20">
		    <div class="period-col clearfix">
				@if(isset($offer->offerPeriods[0]))
					@foreach($offer->offerPeriods as $period)
						</br><label class="bold black"> Period:</label>
						{!! Carbon\Carbon::parse($period->begin_date)->format('d.m.Y') !!} - 
						{!! Carbon\Carbon::parse($period->end_date)->format('d.m.Y') !!}
						<a href="/admin/offers/{{$period->offer->id}}/offer-periods/{{$period->id}}/edit" class="btn btn-primary">Editeaza perioda</a>
						{{ Form::open(['method' => 'DELETE', 'route' => ['offer-periods.destroy', $period->id], 'class'=>'clearfix inline-block']) }}
			    			{{ csrf_field() }}
			    			<input type="hidden" value="{{$period->id}}" name="id">
			    			<input type="submit" value="Sterge perioada" class="btn btn-primary small-btn2">
			    		{{ Form::close() }}			
					@endforeach
				@endif
				<br><a href="/admin/offers/{{$offer->id}}/offer-periods/create" class="btn btn-primary right-btn margin-top__15">Adauga perioada noua</a>
			</div>
		</div>
	</div>