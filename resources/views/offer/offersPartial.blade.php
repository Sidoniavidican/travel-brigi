			           	<div class="col-sm-6 col-md-4" id="offer{!!$offer->id!!}">
			                <div class="inner-content">
			                    <h1>	
								    <a href="/oferta/{{ $offer->id }}">
								          {!! str_limit($offer->name, 20)!!}
								    </a>
		                    	</h1>
			                    <p class="white">{!! Carbon\Carbon::parse($offer->begin_date)->format('d.m.Y') !!} - {!! Carbon\Carbon::parse($offer->end_date)->format('d.m.Y') !!} - {!!$offer->days!!} zile</p>
			                    
			                    @if(isset($offer->offerPeriods[0]))        
			                    	<p class="white">
			                    		{!!Carbon\Carbon::parse($offer->offerPeriods[0]->begin_date)->format('d.m.Y')!!} -
			                    		{!!Carbon\Carbon::parse($offer->offerPeriods[0]->end_date)->format('d.m.Y')!!} 
			                    	</p>
			                    @endif
			                    @if(isset($offer->offerPeriods[1]))           
			                    	<p class="white">...</p>
			                    @endif
			                    
			                 	@if($offer->from != null)
					                <div class="transport">
					                  <p>Plecarea din: {!!$offer->from!!}</p>
					                </div>
					            @endif
					            <div class="inner-right-text">
				                    @if(strchr($offer->type, ',3,'))
				                      <div class="right-text"><span>Circuit</span></div>
				                    @else
				                      @if(strchr($offer->type, ',4,'))
				                          <div class="right-text"><span>Sejur</span></div>
				                      @endif
				                    @endif
				                </div>
			                </div> 
			                <figure class="effect-julia">
			                    
			                    @if(isset($offer->photos[0]))
									<img src="/images/offers/{{ $offer->id }}/{{ $offer->photos[0]->path }}"/>
								@else 
									<img src="/images/poza3.jpg" alt="img21"/>
								@endif	
			                    <figcaption>
			                        <div>
										<p class="white">{!! str_limit($offer->description, 240)!!}
			                        </div>
			                    </figcaption>           
			                </figure>

			                <div class="inner-content-bottom">
							    @include('offer.offerPrice')
			                    @if(strchr($offer->type, ',1,') && Carbon\Carbon::now() > Carbon\Carbon::parse($offer->LM_date))
				                    <div class="early"><span>Last Minute</span></div>
				                @elseif(strchr($offer->type, ',2,') && Carbon\Carbon::now() < Carbon\Carbon::parse($offer->EB_date))
				                    <div class="early"><span>Early Booking</span></div>
				                @endif 	                    
			                </div> 

			            </div>