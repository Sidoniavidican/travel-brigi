
@if($offer->notIncludes!=null || $offer->includes!=null || $offer->observation!=null || isset($offer->offerHotels[0]))
    <div id="exTab1">	
        <ul class="tabs-offer">
	        @if($offer->includes!=null)	
				<li class="1">
		           	<a  href="#1a" data-toggle="tab">Pretul Include</a>
				</li>
			@endif
			@if($offer->notIncludes!=null)	
				<li class="2">
					<a href="#2a" data-toggle="tab">Pretul nu include</a>
				</li>
			@endif
			@if($offer->observation!=null)	
				<li class="3">
					<a href="#3a" data-toggle="tab">Facilitati</a>
				</li>
			@endif

			@if(isset($offer->offerHotels[0]))
				<li class="4">
				  <a href="#4a" data-toggle="tab">Hoteluri</a>
				</li>
			@endif
		</ul>
		<div class="tab-content clearfix tab-offer">
			@if($offer->includes!=null)	
			    <div class="tab-pane active" id="1a">
          		  <p class="regular">{!! $offer->includes !!}</p>
				</div>
			@endif
			@if($offer->notIncludes!=null)	
				<div class="tab-pane" id="2a">
                  <p class="regular">{!! $offer->notIncludes !!}</p>
				</div>
			@endif
			@if($offer->observation!=null)
				<div class="tab-pane" id="3a">
                  <p class="regular">{!! $offer->observation !!}</p>
				</div>
			@endif

			@if(isset($offer->offerHotels[0]))
				<div class="tab-pane" id="4a">
	                <table class="table-hotels period-offer">
						<thead> 
							 <tr>
								<th> Nume Hotel </th>
								<th> Confort </th>
								<th> Poza </th>
							</tr>
						</thead>
						
						@foreach($offer->offerHotels as $offerHotel)			
							<tbody>
								<tr class="tr-period">
									<td data-title="Perioada: ">
										{!!$offerHotel->hotel->name!!}
									</td> 
									<td data-title="Confort: ">
									    <span class="yell">
									    	@for($i=0; $i<$offerHotel->hotel->confort; $i++)
											<span class="icon-Star"></span>
											@endfor
										</span>           		
									</td>
									<td data-title="Poza: ">
										
											@if($offerHotel->hotel->path != null)
												<div class="modal-about" data-toggle="modal" data-target="#{!!$offerHotel->id!!}">    
		                                  			<img src="/images/hotels/{!!$offerHotel->hotel->id!!}/{!!$offerHotel->hotel->path!!}" alt="hotel">
		                                  		</div>
		                                  	@else
		                                  		<div class="modal-about no-cursor"> 
		                                  			<img src="/images/noimagehotel.png" alt="hotel">
		                                  		</div>
		                                  	@endif
	                                    </div>
								   </td>
									@if($offerHotel->hotel->path != null)
									    <div id="{!!$offerHotel->id!!}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										    <div class="modal-dialog">
										   		
										       		<img src="/images/hotels/{!!$offerHotel->hotel->id!!}/{!!$offerHotel->hotel->path!!}" alt="hotel">
										    </div>
	                                    </div>
                                    @endif
								</tr>				 
					        </tbody>
					    @endforeach
					</table>
				</div>
			@endif
		</div>
    </div>
@endif