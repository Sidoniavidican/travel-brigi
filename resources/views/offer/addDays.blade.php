@extends('layouts.app')
@section('content')

@if (Session::has('offer-success'))
    <p class="offer-success" name="{{ Session::get('offer-success') }}"></p>
@endif

<div class="relative adm-margin">  	
<div class="admin-offer clearfix">
	@if(isset($offer->offerDays[0]))

		{{ Form::open(['method' => 'PATCH', 'route' => ['days.update', $offer->id], 'id' => 'addDaysValidate']) }}
			{{ csrf_field() }}
			<input type="hidden" value="{{$offer->id}}" name="id">
			@foreach($offer->offerDays->sortBy('day') as $day)
			<div class="margin-top-20 add-adm-days">
				<label class="red">Ziua {{$day->day}}</label>
				<span class="filed-adm"><input type="text" name="{{$day->day}}" class="days" id="day{!! $loop->index !!}" required value="{{ $day->description }}" maxlength="1000"></span> 
			</div>
			@endforeach
			
			{{ Form::submit('Salveaza', ['class' => 'margin-top-20 small-btn right-btn addDays']) }}		 
		{{ Form::close() }}

	@else	

		{{ Form::open(['method' => 'POST', 'route' => ['days.store', $offer->id] , 'id' => 'addDaysValidate']) }}
			{{ csrf_field() }}
			
			<input type="hidden" name="offer_id" value="{!!$offer->id!!}">
			@for($i=1; $i<=$offer->days; $i++)
			<div class="margin-top-20 add-adm-days">
				<label class="red">Ziua {{$i}}</label>
				<span class="filed-adm"><input type="text" name="{{$i}}" class="days" id="day{{$i}}" required maxlength="1000"></span>
			</div>
			@endfor
			
			{{ Form::submit('Salveaza', ['class' => 'margin-top-20 small-btn2 right-btn addDays']) }}		 
		{{ Form::close() }}

		<a href="/admin/offers" class="margin-top-20 small-btn2 btn-primary right-btn m-right">Skip</a>

	@endif
	</div>
</div>
@endsection