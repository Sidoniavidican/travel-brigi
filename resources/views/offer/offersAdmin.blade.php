@extends('layouts.app')

@section('content')


@if (Session::has('offer-success'))
    <p class="offer-success" name="{{ Session::get('offer-success') }}"></p>
@endif

<div class="container">
    <div class="inner-space">
    <div class="content bottom-section relative clearfix">
    	@if(Auth::check() && auth()->user()->id=='1') 
	    	<a href="/admin/offers/create">
	    	    <button type="button" class="yellow-btn">  		
	    			<span class="icon-Plus" aria-hidden="true"></span>		
	    	    </button> 
	    	</a>
    	@endif
        <div class="grid">
           @foreach($offers as $offer) 
				@if($offer->type != ',0,')
	           	<div class="col-sm-6 col-md-4" id="offer{!!$offer->id!!}">
	                <div class="inner-content">
	                    <h1>	
						    <a href="/admin/offers/{{ $offer->id }}">
						      {!! str_limit($offer->name, 20)!!}
						    </a>
                    	</h1>
	                    <p class="white">{!! Carbon\Carbon::parse($offer->begin_date)->format('d.m.Y') !!} - {!! Carbon\Carbon::parse($offer->end_date)->format('d.m.Y') !!} - {!!$offer->days!!} zile</p>
	                 	@if($offer->from != null)
			                <div class="transport">
			                  <p>Plecarea din: {!!$offer->from!!}</p>
			                </div>
			            @endif
	                </div> 
	                <figure class="effect-julia">
	                    @if(isset($offer->photos[0]))
							<img src="/images/offers/{{ $offer->id }}/{{ $offer->photos[0]->path }}"/>
						@else 
							<img src="/images/poza3.jpg" alt="img21"/>
						@endif	
	                </figure>
                    <div class="">	
			            @include('offer.offerPrice') 	                    
	                </div> 
	                <div class="admin-home-offer">
		                <div class="inner-content-bottom"> 
		                	<p>
								{{ Form::submit('Delete', ['class' => 'btn-danger deleteOffer', 'id' => $offer->id]) }}				
								<a href='/admin/offers/{{$offer->id}}/edit' class="btn btn-primary edit-of">Edit Offer</a>
				            </p>
				        </div>
			        </div>	


	            </div>

	            @else
	           	<div class="col-sm-6 col-md-4" id="offer{!!$offer->id!!}">
	                <div class="inner-content">
	                    <h1>
	                    	<a href="/admin/offers/{{ $offer->id }}">
						          {!! str_limit($offer->name, 20)!!}
						    </a>
						</h1>
	                    @if(isset($offer->periods[0]))        
	                    	<p class="white">
	                    		{!!Carbon\Carbon::parse($offer->periods[0]->begin_date)->format('d.m.Y')!!} -
	                    		{!!Carbon\Carbon::parse($offer->periods[0]->end_date)->format('d.m.Y')!!}  - 
	                    		de la {!!$offer->periods[0]->single!!}
	                    	</p>
	                    @endif
	                   	@if(isset($offer->periods[1]))           
	                    	<p class="white">
	                    		{!!Carbon\Carbon::parse($offer->periods[1]->begin_date)->format('d.m.Y')!!} -
	                    		{!!Carbon\Carbon::parse($offer->periods[1]->end_date)->format('d.m.Y')!!}  - 
	                    		de la {!!$offer->periods[1]->single!!}
	                    	</p>
	                    @endif
	                    @if(isset($offer->periods[2]))           
	                    	<p class="white">...</p>
	                    @endif
	                   	<div class="inner-right-text">
		                    <div class="right-text"><span>Cazare<span></div>
	                    </div>
	                </div> 
	                <figure class="effect-julia">                    
	                    @if(isset($offer->photos[0]))
							<img src="/images/offers/{{ $offer->id }}/{{ $offer->photos[0]->path }}"/>
						@else 
							<img src="/images/poza3.jpg" alt="img21"/>
						@endif	
	                    <figcaption>
	                   
	                    </figcaption>           
	                </figure>
	                <div class="inner-content-bottom inner-cazari">
	                	<p>			 		
                            {{ Form::submit('Delete', ['class' => 'btn-danger deleteOffer', 'id' => $offer->id]) }}
							<a href='/admin/offers/{{$offer->id}}/edit' class="btn btn-primary edit-of">Edit Offer</a>
			            </p>
	                </div> 
	            </div>
	            @endif
            @endforeach
         </div>
        </div>
    </div>
</div>


@endsection 