
 {{ Form::open(['method' => 'PATCH', 'route' => ['offers.update', $offer->id], 'class'=>'dateRangeForm', 'id' => 'editOfferValidate']) }}
	    {{ csrf_field() }}
		{{ Form::hidden('id', $offer->id) }} 
        <div class="margin-top-20">
			<span class="label-adm">{{ Form::label('name', 'Titlu:') }}</span>
			<span class="filed-adm">{{ Form::text('name', $offer->name, ['required', 'maxlength'=>'80']) }}</span>
		</div>
        
        <div class="margin-top-20">
			<span class="label-adm">{{ Form::label('description', 'Descriere:') }}</span>
			<span class="filed-adm">{{ Form::textarea('description', $offer->description, ['class'=>'form-control', 'rows' => 2, 'cols' => 40]) }}</span>
		</div> 
        <div class="margin-top-20"> 
	        <div class="row">
	        	<div class="col-sm-6">
					<span class="label-adm">{{ Form::label('country[]', 'Tara:')}}</span>
					<span class="filed-adm">
						<select class="selectpicker" id="country"  title="Selecteaza Tara" multiple name="country[]" required data-selected-text-format="count">
							@foreach($countries as $country)
								@if(strpos($offerCountries, ','.(string)($country->id).',' )!== false)
							  		<option selected value="{!!$country->country!!}">{!!$country->country!!}</option>
							  	@else
							  		<option value="{!!$country->country!!}">{!!$country->country!!}</option>
							  	@endif
							@endforeach
						</select>
					</span>
				</div>
				<div class="col-sm-6">
					<span class="label-adm">{{ Form::label('hotel[]', 'Hotel:') }}</span>
					<span class="filed-adm">
						<select class="selectpicker"  title="Selecteaza Hotel" id="hotel" multiple name="hotel[]" data-selected-text-format="count">
							@foreach($hotels as $hotel)
								@if(strpos($offerHotels, ','.(string)($hotel->id).',' )!== false)
							  		<option selected value="{!!$hotel->id!!}">{!!$hotel->name!!}</option>
							  	@else
							  		<option value="{!!$hotel->id!!}">{!!$hotel->name!!}</option>
							  	@endif
							@endforeach
						</select>
					</span>
				</div>
			</div>
		</div> 
        
		<div class="margin-top-20">
	        <div class="row">
	            <div class="col-sm-4">
		            <span class="label-adm">{{ Form::label('passport', 'Pasaport:') }}</span>
					<span class="filed-adm">{{ Form::select('passport', ["Nu", "Da"], $offer->passport, ['class' => '']) }} </span>
				</div>

                    <div class="col-sm-4">
						<span class="label-adm">{{ Form::label('dinner', 'Masa:')}}</span>
						<span class="filed-adm">{{ Form::select('dinner', ["Neinclusa", "Inclusa"], $offer->dinner, ['class' => '']) }}</span>
					</div>

                <div class="col-sm-4">
					<span class="label-adm">{{ Form::label('confort', 'Confort:') }}</span>
			    	<span class="filed-adm">{{ Form::select('confort', ['1'=>'1', '2'=>'2', '3'=>'3', '4'=>'4', '5'=>'5'], $offer->confort, ['class' => '']) }}</span>
		        </div>
			</div>
		</div>

		<div class="margin-top-20">
	        <div class="row">
	            <div class="col-sm-4">				
					<span class="label-adm">{{ Form::label('observation', 'Facilitati:') }}</span>
					<span class="filed-adm">{{ Form::textarea('observation', $offer->observation, ['class'=>'form-control', 'rows' => 2, 'cols' => 40]) }}</span> 
				</div>

                <div class="col-sm-4">
				    <span class="label-adm">{{ Form::label('includes', 'Include:')}}</span>
					<span class="filed-adm">{{ Form::textarea('includes', $offer->includes, ['class'=>'form-control', 'rows' => 2, 'cols' => 40]) }}</span>
			    </div>

                <div class="col-sm-4">
			    	<span class="label-adm">{{ Form::label('notIncludes', 'Nu include:')}}</span>
			     	<span class="filed-adm">{{ Form::textarea('notIncludes', $offer->notIncludes, ['class'=>'form-control', 'rows' => 2, 'cols' => 40]) }}</span>
				</div>
			</div> 
		</div>

	    <div class="fixed-footer edit">
		    <div class="relative-footer">
				<div class="margin-top-20 col-sm-2 right-btn">			
					{{ Form::submit('Salveaza', ['class' => 'btn btn-primary edit-offer addOffer']) }}
				</div>
			</div>
		</div>
			 
	{{ Form::close() }}
		
	@if($errors->any())
		    <div class="alert alert-danger">
		        @foreach($errors->all() as $error)
		            <p>{{ $error }}</p>
		        @endforeach
		    </div>
	@endif
    
    <div class="row">
		<div class="col-sm-12 clearfix margin-top-20">
		    <div class="period-col clearfix">
				@if(isset($offer->periods[0]))
					@foreach($offer->periods as $period)
						</br><label class="bold black"> Period:</label>
						{!! Carbon\Carbon::parse($period->begin_date)->format('d.m.Y') !!} - 
						{!! Carbon\Carbon::parse($period->end_date)->format('d.m.Y') !!}
						<a href="/admin/offers/{{$period->offer->id}}/periods/{{$period->id}}/edit" class="btn btn-primary">Editeaza perioda</a>					
						{{ Form::open(['method' => 'DELETE', 'route' => ['periods.destroy', $period->id], 'class'=>'clearfix inline-block']) }}
			    			{{ csrf_field() }}
			    				<input type="submit" value="Sterge perioada" class="btn btn-primary small-btn2">
			    		{{ Form::close() }}
					@endforeach
				@endif
				<br><a href="/admin/offers/{{$offer->id}}/periods/create" class="btn btn-primary right-btn ">Adauga perioada noua</a>
			</div>
		</div>
	</div>




