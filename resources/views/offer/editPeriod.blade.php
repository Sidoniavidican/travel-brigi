@extends('layouts.app') 
@section('content')
<div class="relative adm-margin">  	
    <div class="admin-offer admin-offer-simple clearfix">
		<h2>Edit period for: {!!$period->offer->name!!}</h2>
		{{ Form::open(['method' => 'PATCH', 'route' => ['periods.update', $period->id], 'class'=>'dateRangeForm', 'id' => 'addPeriodValidate']) }}
					{{ csrf_field() }}
					
					<div class="row margin-top-30">
						<div class="col-sm-6 margin-top-20">
        				    <div class="input-group input-append date dateRangePicker">
        					    {{ Form::label('begin_date', 'Data de inceput:') }}     
        						<span class="filed-adm">
        							<input type="text" name="begin_date" id="begin_date" required value="{{$period->begin_date}}">
        						</span>
        				        <span class="input-group-addon add-on"></span>
        				    </div>
        				</div> 

                        <div class="col-sm-6 margin-top-20">
        					<div class="input-group input-append date dateRangePickerEnd">
        					    {{ Form::label('end_date', 'Data de sfarsit:') }}     
        						<span class="filed-adm">
        							<input type="text" name="end_date" id="end_date" required value="{{$period->end_date}}">
        						</span>
        				        <span class="input-group-addon add-on"></span>
        				    </div>
        				</div>
	                    
	                    <div class="col-sm-6 margin-top-20">
							<label>Single Price</label>
							<span class="filed-adm"><input type="number" name="single"  id="single" required value="{{$period->single}}" max="20000" min="5"></span>
						</div>
						<div class="col-sm-6 margin-top-20">
							<label>Double Price</label>
							<span class="filed-adm"><input type="number" name="double" id="double" required value="{{$period->double}}" max="20000" min="5"></span>
						</div>
					</div>

					<div class="row">
					<div class="col-sm-6 margin-top-20">
						<label>Twin Price</label>
						<span class="filed-adm"><input type="number" name="twin" id="twin" required value="{{$period->twin}}" max="20000" min="5"></span>
					</div>
                    <div class="col-sm-6 margin-top-20">
						<label>Triple Price</label>
						<span class="filed-adm"><input type="number" name="triple" id="triple" required value="{{$period->triple}}" max="20000" min="5"></span> 
					</div>
					</div>

					<div class="row">
						<div class="col-sm-3 margin-top-20">
			                {{ Form::label('currency', 'Valuta:') }}
				           	<span class="filed-adm">{{ Form::select('currency', ["Euro", "RON"], $period->currency, ['required', 'class' => '']) }}</span>
			           	</div>
					</div>
			{{ Form::submit('Edit', ['class' => 'btn btn-primary right-btn small-btn margin-top-20 addPeriod']) }}		 
		{{ Form::close() }}
		</div>
	</div>
@endsection