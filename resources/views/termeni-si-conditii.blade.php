@extends('layouts.app')
@section('title') Termeni si Conditii @stop 
@section('description') Brigitte Travel | Termeni si Conditii @stop 
@section('content')  
<div class="container">
  <div class="terms"> 
    <div class="section-about clearfix">
    <div class="col-sm-12">
      <h1 class="heading-small bold">Termeni si Conditii</h1>
      <p>Vizitarea, folosirea, sau comandarea produselor vizualizate pe site-ul www.digitalevent.ro implica acceptarea Conditiilor de utilizare ce vor fi detaliate mai jos. </p>
      <p>Pentru folosirea in bune conditii a site-ului, se recomanda citirea cu atentie a Termenilor si Conditiilor. www.brigittetravel.ro este administrat de S.C. Brigitte Travel S.R.L. cu sediul in Oradea, Str. Avram Iancu, inregistrata la Registrul Comertului cu nr J05/1863/1991, CUI 47445. </p>

      <p>Ne rezervam dreptul de a efectua modificari ale acestor Termeni si Conditii precum si orice modificari site-ului www.brigittetravel.ro fara o notificare prealabila. Accesand pagina Termeni si Conditii, puteti citi intotdeauna cea mai recenta versiune a acestor prevederi.</p>
      <p>Obligatiile dumneavoastra de inregistrare:</p>

      <p>Pentru a putea cumpara de pe www.brigittetravel.ro, trebuie sa introduceti datele dumneavoastra pe website-ul nostru. In utilizarea serviciului, declarati ca veti furniza informatii reale, corecte, actuale si complete despre dumneavoastra. In situatia in care consideram ca aceasta obligatie a fost incalcata, ne rezervam dreptul de a va bloca accesul la utilizarea serviciului, pe perioada determinata sau nedeterminata, fara nicio notificare prealabila.</p>

      <p>Pret si modalitati de plata:</p>

      <p>Pretul de achizitie prezentat pe factura va fi acelasi cu cel prezentat pe site la momentul achizitiei, pret la care se adauga si costurile de livrare. Plata produselor se efectueaza in numerar/cash, la livrarea produsului sau prin credit/ debit card la plasarea comenzii online.
      Drepturile de autor (Copyright)</p>

      <p>Intregul continut al site-ului www.brigittetravel.ro, incluzand, texte, imagini, grafice, elemente de grafica web, scripturi si orice alte date, este proprietatea S.C. Brigitte Travel S.R.L. sau a furnizorilor sai si este protejat conform Legii drepturilor de autor si legilor privind dreptul de proprietate intelectuala si industriala. Folosirea fara acordul scris a oricaror elemente de pe site-ul www.brigittetravel.ro, sau a oricaror elemente enumerate mai sus se pedepseste conform legilor in vigoare. Pentru a reclama drepturile de proprietate intelectuala puteti folosi adresa de mail info@brigittetravel.ro</p>

      <p>Politica de confidentialitate</p>

      <p>Datele cu caracter personal vor putea fi dezvaluite fara consimtamAcntul dumneavoastra A®n caz de litigii/dispute privind fraudele la plata catre urmatorii destinatari: Bancile implicate A®n platile online, Organismele Internationale de Carduri si Furnizori de servicii/produse, respectiv conform Legii, catre institutiile abilitate.</p>
      <p>In conformitate cu dispozitiile Legii nr.677/200, clientii nostril au urmatoarele drepturi,:</p>
      <ul>
       <li>1) Dreptul la informare (art.12)</li>
       <li>2) Dreptul la acces la date (art.13) - dreptul de a obtine, la cerere si A®n mod gratuit, pentru o solicitare pe an, confirmarea ca datele care o privesc sunt sau nu sunt prelucrate de operatorul de date.</li>
       <li>3) Dreptul la interventie (art.14) - dreptul de a solicita operatorului de date, printr-o cerere scrisa si A®n mod gratuit, urmatoarele:</li>
       <li>a) rectificarea, actualizarea, blocarea sau stergerea datelor incomplete, inexacte sau prelucrarilor nelegale;</li>
       <li>b) transformarea datelor personale nelegale A®n date anonime;</li>
       <li>c) notificarea tertilor cu privire la operatiunile prevazute la lit. a) si b).</li>
       <li>4) Dreptul la opozitie (art.15) - dreptul de a se opune, A®n mod gratuit si printr-o cerere scrisa, din motive A®ntemeiate si legitime legate de situatia sa particulara, ca datele care o vizeaza sa faca obiectul unei prelucrari.</li>
       <li>5) Dreptul de a se adresa justitiei (art.18) - dreptul de a se adresa justitiei pentru apararea drepturilor garantate de lege si care au fost A®ncalcate.</li>
       <li>6) Dreptul de a face plAcngere la autoritatea de supraveghere (art.25). Responsabilitati privind produsele </li>
      </ul>
      <p>S.C. App Generator .S.R.L. nu isi asuma responsabilitatea si nu poate fi facuta responsabila pentru orice pagube aparute prin folosirea produselor achizitionate prin intermediul acestui site, in alte scopuri decat cele mentionate de producator. Returnarea produselor se poate efectua in conformitate cu dispozitiile OG 130/2000.</p>
      <p>Limitari privind utilizarea anumitor tehnici de comunicatie la distanta</p>

     <p> Art. 1. - Urmatoarele tehnici de comunicatie la distanta necesita acordul prealabil al consumatorului:</p>
     <ul> 
       <li>a) sistem automatizat de apel fara interventie umana (automat de apel); </li>
       <li>b) telecopiator (fax)</li>
       <li>c) adresa de posta electronica (e-mail) </li>
      </ul>

      <p>Art. 2. - Utilizarea altor tehnici de comunicatie individuala la distanta in afara celor prevazute la art. 1 nu este permisa daca exista un refuz manifestat de consumator. </p>
      <p>Lista cuprinzand tehnicile de comunicatie la distanta</p>
      <ul>
      <li>a) imprimat neadresat;</li>
      <li>b) imprimat adresat; </li>
      <li>c) scrisoare tipizata;</li> 
      <li>d) publicitate tiparita cu bon de comanda; </li>
      <li>e) catalog; </li>
      <li>f) telefon cu interventie umana; </li>
      <li>g) telefon fara interventie umana (automat de apel, audiotext); </li>
      <li>h) radio; </li>
      <li>i) videofon (telefon cu imagine); </li>
      <li>j) videotext (microordinator, ecran Tv cu tastatura sau ecran tactil); </li>
      <li>k) posta electronica (e-mail); </li>
      <li>l) telecopiator (fax); </li>
      <li>m) televiziune (teleshopping).</li>
      </ul>

      <p>Prin folosirea,vizualizarea sau cumpararea produselor de pe acest site, utilizatorul a luat la cunostinta asupra faptului ca legile romane vor guverna Termenii si Conditiile de mai sus si orice disputa de orice fel care ar putea sa apara intre utilizator si S.C. Brigitte Tours S.R.L. In cazul unor eventuale conflicte intre S.C. Brigitte Tours S.R.L. si clientii sai, se va incerca mai intai rezolvarea acestora pe cale amiabila in minim 30 de zile lucratoare.</p>

      <p>Daca rezolvarea pe cale amiabila nu va fi posibila, conflictul va fi solutionat in instanta competenta in conformitate cu legile romane in vigoare.
      Diverse</p>

      <p>Daca oricare dintre clauzele de mai sus va fi gasita nula sau nevalida, indiferent de cauza, aceasta nu va afecta valabilitatea celorlalte clauze. Odata cu lansarea comenzii, clientul accepta fara obiectiuni Conditiile si termenii de utilizare, valoarea acestora fiind aceeasi cu un contract valabil incheiat. </p>

      <p>Fiind de acord cu acesti Termeni si Conditii clientul isi isi asuma in totalitate drepturile si obligatiile ce decurg din cumpararea din magazinul virtual www.brigittetravel.ro.

      </p>
      </div>
    </div>
  </div>
</div>
   
<div class="news">
@component('subscriber.subscribe')@endcomponent
</div> 
@endsection 
