<div class="tikets">
    <div class="contact modal-dialog pop-up-folder"  id="bilete" role="document">
        <div class="modal-content">
            <div class="modal-header"> 
                <h3 class="heading-popup" id="exampleModalLabel">Unde doriti sa zburati?</h3> 
                <span class="silver small">Completati formularul de mai jos cu toate datele necesare. Campurile cu steluta (*) sunt obligatorii.</span>
            </div>
            <div class="modal-body box box-form"> 
                <form action="/sendformbilete" method="post" id="popup" class="clearfix">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <span class="input input--hoshi">
                        <input class="input__field input__field--hoshi" type="text" name="name" id="input-1" />
                        <label class="input__label input__label--hoshi input__label--hoshi-color-1" for="name">
                            <span class="input__label-content input__label-content--hoshi silver
                            ">Nume &amp; Prenume *</span>
                        </label>
                    </span>
                    <span class="input input--hoshi">
                        <input class="input__field input__field--hoshi" type="text" name="email" id="input-3" />
                        <label class="input__label input__label--hoshi input__label--hoshi-color-1" for="email"> 
                            <span class="input__label-content input__label-content--hoshi silver">Email *</span>
                        </label>
                    </span>
                    <span class="input input--hoshi">
                        <input class="input__field input__field--hoshi" type="text" name="phone" id="input-4" />
                        <label class="input__label input__label--hoshi input__label--hoshi-color-1" for="phone"> 
                            <span class="input__label-content input__label-content--hoshi silver">Telefon *
                            <p class="small">Veti fi contactat telefonic pentru mai multe detalii</p></span>
                        </label>
                    </span>
                    <span class="input input--hoshi">
                        <textarea class="input__field input__field--hoshi" type="text" name="message" id="input-5"></textarea>
                        <label class="input__label input__label--hoshi input__label--hoshi-color-1" for="avability">
                            <span class="input__label-content input__label-content--hoshi silver">Destinatie/Perioada * <p class="small">Va rugam sa detaliati destanta si perioada dorita.</p></span>
                        </label>
                    </span>
                    <div class="clearfix right margin-top-10">
                        <button type="button" class="gray-bg btn btn-secondary " data-dismiss="modal">Anuleaza</button>
                        <button  type="submit" class="btn btn-primary red-bg" data-toggle="modal" data-target="#requestmodal">Cere Oferta</button> 
                    </div>
                </form>
            </div>
        </div>
    </div> 
</div>
@if (session('status'))
{{ session('status') }}
<script type="text/javascript">fbq('track', 'Request A Question Lead');</script>
@endif
  


