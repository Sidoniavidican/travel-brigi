@extends('layouts.app')
@section('content')

@if (Session::has('offer-success'))
    <p class="offer-success" name="{{ Session::get('offer-success') }}"></p>
@endif

<div class="relative adm-margin">
    <button type="button" class="yellow-btn">
    	<a href="/admin/albums/create">
    		<span class="icon-Plus" aria-hidden="true"></span>
    	</a>
    </button>


    <div class="albums-admin clearfix">
        @foreach($albums as $album)
		    <div class="col-sm-6 albums">
			    <div id="album{!!$album->id!!}">
			    	<div class=""><h1 class="heading-small bold"><a href="/album/{!!$album->id!!}"> {!!$album->name!!} </a> </h1></div>
			    	@if(isset($album->photos[0]))
						<img src="/images/gallery/{{$album->id}}/{{ $album->photos[0]->path }}"/>
					@else 
						<img src="/images/poza3.jpg" alt="img21"/>
					@endif	
					<div class="clearfix" style="float: right;">
						{{ Form::submit('Sterge', ['class' => 'btn btn-danger deleteAlbum', 'id' => $album->id]) }}
						<a href="/admin/albums/{!!$album->id!!}/edit" class="btn btn-primary">Editeaza</a>
					</div>
				</div>
			</div>
        @endforeach
	</div>
</div>
@endsection