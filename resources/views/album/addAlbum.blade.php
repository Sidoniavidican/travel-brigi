@extends('layouts.app')

@section('content')
  
<div class="relative adm-margin">  	
	<div class="admin-offer admin-offer-simple clearfix">
		<h2>New Album!</h2>

		{{ Form::open(['method' => 'POST', 'route' => ['albums.store'], 'id'=>'addAlbumValidate']) }}
		{{ csrf_field() }}
				<div class="margin-top-20">
				<span class="label-adm">{{ Form::label('name', 'Name:') }}</span>
				</div>
				<span class="filed-adm">{{ Form::text('name', null, ['required']) }} </span>
				<div class="fixed-footer">
			   		 <div class="relative-footer">
						 <div class="margin-top-20 right-btn m-right" style="padding-bottom: 20px;">
							{{ Form::submit('Continua', ['class' => 'btn btn-primary addOffer']) }}
						</div>
					</div>
				</div>
		{{ Form::close()}}
	
		<div class="margin-top-20"> 
			<label > Adauga Poze: </label> 
		    {{ Form::open(['method' => 'POST', 'route' => ['store-album-photo'], 'class' => 'dropzone', 'id' => 'update']) }}
				{{ csrf_field() }}
				<input name="id" value="temporary" type="hidden" id="id">		      
			{{ Form::close() }}
		</div>
	</div>


</div>
@endsection