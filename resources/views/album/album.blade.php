@extends('layouts.app')
@section('content')
<div class="container">
	<div class="album clearfix">
	    <h1 class="center heading-gray mt-o">{!!$album->name!!}</h1>
		<ul class="lb-album">
			@foreach($album->photos as $photo)
				<li class="col-sm-6 col-md-4">
					<a href="#image{{$photo->id}}">
						<img src="/images/gallery/{{$album->id}}/{{$photo->path}}"/>
						<span class="zoom icon-Zoom"></span>
					</a>
					<div class="lb-overlay" id="image{{$photo->id}}">
					   
						<a href="#page" class="lb-close"><span class="icon-Close"></span></a>
						<img src="/images/gallery/{{$album->id}}/{{$photo->path}}"/> 
						
					</div>
				</li>
			@endforeach
		</ul>
	</div>
</div>
@endsection 