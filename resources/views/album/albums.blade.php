@extends('layouts.app')
@section('title') Galerie Foto @stop 
@section('description')Vreti sa ne vedeti in excursii? Uitati-va prin albumele foto! @stop 
@section('content')  
  <div class="container">
    <div class="center">
      <h3 class="center heading-gray">Vreti sa ne vedeti in excursii? Uitati-va prin albumele foto!</h3>
    </div>
    <div class="inner-albums clearfix">
    @foreach($albums as $album)
      <div class="col-sm-6 albums" style="margin-bottom:30px">   
        <a href="album/{{$album->id}}">
          <div class="col-sm-6 no-padd">
          	@if(isset($album->photos[0]))
            	<img src="images/gallery/{{$album->id}}/{!!$album->photos[0]->path!!}">
            @else
            	<img src="/images/NoImage.png">
            @endif

            @if(isset($album->photos[1]))
            	<img src="images/gallery/{{$album->id}}/{!!$album->photos[1]->path!!}">
           	@else
            	<img src="/images/NoImage.png">
            @endif
          </div>
         
	        <div class="col-sm-wide">
	        @if(isset($album->photos[2]))
                <div class="col-sm-cover" style="background-image: url('/images/gallery/{{$album->id}}/{!!$album->photos[2]->path!!}')">
                </div>
	        @else
                <div class="col-sm-cover" style="background-image: url('/images/NoImage.png')">
                </div>
	        @endif
	        </div>
            <div class="button-upload">
              <button type=""><span class="icon-PhotoAlbum"></span></button>
            </div>
           <div class="layer-black"></div>
         </a>
      </div>
    @endforeach
    </div>
</div>
@endsection
