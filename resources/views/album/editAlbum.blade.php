@extends('layouts.app')

@section('content')
<div class="relative adm-margin">  	
	<div class="admin-offer admin-offer-simple clearfix">
		
		<h2>Editeaza Album {!!$album->name!!}!</h2>
		<div class="margin-top-20">
			{{ Form::open(['method' => 'PATCH', 'route' => ['albums.update', $album->id], 'id' => 'editAlbumValidate']) }}
				<span class="filed-label">{{ Form::label('name', 'Name:') }}</span>
				<span class="filed-adm">{{ Form::text('name', $album->name, ['required']) }}</span>
				{{ Form::submit('Salveaza', ['class' => 'btn margin-top-10 right-btn small-btn btn-primary edit-album']) }}
			{{ Form::close() }}
		</div>
		<div class="clearfix margin-top-40">
		   	<label > Adauga Poze: </label> 
		    <form action="/store-album-photo"  method="POST" class="dropzone" id="update">
				{{ csrf_field() }}
				<input name="id" value="{!!$album->id!!}" type="hidden" id="id">		      
			</form> 
		</div>
		<div class="row">
		  	@foreach($album->photos as $photo)
		  		<div class="col-md-3" id="photo{!!$photo->id!!}">
					{{ Form::submit('Sterge', ['class' => 'btn btn-danger deletePhotoAlbum', 'id' => $photo->id]) }}
					<img src="/images/gallery/{{$album->id}}/{{ $photo->path }}"/>
				</div>
			@endforeach
		</div>
	</div>
</div>
@endsection 