<footer>
  <div class="main-footer clearfix">
    <div class="container">
      <div class="main-footer-content">
        <div class="col-sm-5">
          <a href="/"><img src="/images/LogoBrigitte.svg" alt="logo Footer"></a> 
          <ul class="left-content">
            <li><span class="icon-Phone"></span><a href="tel: 0745 476 451">0745 476 451</a></li>
            <li class="left"><a href="tel: 0745 928 715">0745 928 715</a></li>
			<li class="left"><a href="tel: 0259 434 413">0259 434 413</a></li> 
            <li><span class="icon-Email"></span><a href="mailto: info@brigittetravel.ro">info@brigittetravel.ro</a></li>
          </ul>
        </div>
        <div class="col-sm-4">
          <div class="margin-top-30">
            <ul class="line">
              <li><a href="{{ url('/early-booking') }}">EARLY BOOKING</a></li>
              <li><a href="{{ url('/last-minute') }}">LAST MINUTE</a></li>
              <li><a href="{{ url('/exotic') }}">EXOTIC</a></li>
              <li><a href="{{ url('/senior-voyage') }}">SENIOR VOYAGE</a></li>
              <li><a href="{{ url('/city-break') }}">CITY BREAK</a></li>
              <li><a href="{{ url('/croaziere') }}">CROAZIERE</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="margin-top-30">
            <ul class="line">
              <li><a href="/despre-noi">DESPRE NOI</a></li>
              <li><a href="/termeni-si-conditii">TERMENI SI CONDITII</a></li>
              <li><a href="/confidentialitate">CONFIDENTIALITATE</a></li>
              <li><a href="/contact">CONTACT</a></li>
              <li class="icons"><a href="https://www.facebook.com/brigittetravel1/" target="_blank"><span class="white icon-Facebook"></span></a></li>
              <li class="icons"><a href="https://www.instagram.com/brigitte_travel/" target="_blank"><span class="white icon-Instagram"></span></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="layer-black"></div> 
  </div>
  <div class="container">
    <div class="designed center clearfix">
      <p>Designed by</p>
     <a class="powered" href="https://glabit.com/" target="_blank"> <img src="/images/glabit.png" alt="glabit logo"> GlabIT</a>
    </div>
  </div>
</footer> 
  