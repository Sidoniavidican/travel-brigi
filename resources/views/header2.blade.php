<div class="inner-cover-menu"> 
    <div class="container">
    <div class="navbar-header">
        <div class="logo-small"><a href="/"><img src="/images/LogoBrigitteMobile.svg" alt="logo"></a></div> 
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
        <div class="menu-all">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="main-menu clearfix">
                    <ul class="main-left">
                        <li><a href="{{ url('/cazari') }}"  class="{!! Request::is('cazari') ? 'curent__item__hero' : '' !!}">Cazari</a></li>
                        <li><a href="{{ url('/transporturi') }}"  class="{!! Request::is('transporturi') ? 'curent__item__hero' : '' !!}">Transporturi</a></li>
                        <li>
                            <a href="" data-toggle="modal" data-target="#bilete">
                            Bilete de Avion</a>
                        </li>
                        <div class="modal fade" tabindex="-1" role="dialog" id="bilete" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            @include('bileteavion') 
                        </div>
                    </ul>
                    <ul class="main-right">
                    	<li><a href="{{ url('/albume') }}"  class="{!! Request::is('albume') ? 'curent__item__hero' : '' !!}">Albume</a></li>
                        <li><a href="{{ url('/despre-noi') }}"  class="{!! Request::is('despre-noi') ? 'curent__item__hero' : '' !!}">Despre Noi</a></li>
                        <li><a href="{{ url('/contact') }}"  class="{!! Request::is('contact') ? 'curent__item__hero' : '' !!}">Contact</a></li>
                    </ul>
                </div>
                 <div class="inner-logo-brand">
                     <a href="/" class="embed-link">
                      <div class="svg"> 
                        <embed class="imageSVG" src="/images/photo.svg"/> 
                      </div>
                   
                    </a>
                </div>
                <form method="GET" action="/search" accept-charset="UTF-8" id="searchValidate">
                    <div class="search-wrapper clearfix">
                        <div class="input-holder">
                            <fieldset class="field-container">
                              <input type="text" name="search" placeholder="Cauta dupa destinatie" class="field filed-search-index" required /> 
                              <div class="icons-container">
                                <div class="icon-search"></div>
                                <div class="icon-close">
                                  <div class="x-up"></div>
                                  <div class="x-down"></div>
                                </div>
                              </div>
                            </fieldset>
                        </div>
                    </div>
                </form>

   
            <nav class="nav-categories navbar navbar-static-top">  
                <div class="navbar-header">
                    <ul class="clearfix">
                    <div class="c">
                       <div class="list-group list-group-horizontal">
                            <ul class="group-menu">
                                <li class="{!! Request::is('early-booking')|| str_contains(Route::currentRouteName(),'early-booking') ? 'curent__item_red' : '' !!}" id="button2"> 
                                    <a href="{{ url('/early-booking') }}" class="first-early list-group-item" >Early booking</a>
                                 </li>
                            
                                <li class="{!! Request::is('last-minute') || str_contains(Route::currentRouteName(),'last-minute')? 'curent__item_red' : '' !!}" id="button1"> 
                                    <a href="{{ url('/last-minute') }}" class="list-group-item">Last minute</a>
                                </li>

                                <li class="{!! Request::is('circuite') || str_contains(Route::currentRouteName(),'circuite')? 'curent__item_red' : '' !!}" id="button3">
                                    <a href="{{ url('/circuite') }}" class="list-group-item">Circuite</a>
                                </li>
                                
                                <li class="{!! Request::is('sejururi') || str_contains(Route::currentRouteName(),'sejururi')? 'curent__item_red' : '' !!}" id="button4">
                                    <a href="{{ url('/sejururi') }}" class="list-group-item">Sejururi</a>
                                </li> 

                                <li class="{!! Request::is('exotic') || str_contains(Route::currentRouteName(),'exotic')? 'curent__item_red' : '' !!}" id="button5">
                                    <a href="{{ url('/exotic') }}" class="list-group-item">Exotic</a>
                                </li>

                                <li class="{!! Request::is('city-break') || str_contains(Route::currentRouteName(),'city-break') ? 'curent__item_red' : '' !!}" id="button6">
                                    <a href="{{ url('/city-break') }}" class="list-group-item">City break</a>
                                </li>

                                <li class="{!! Request::is('croaziere') || str_contains(Route::currentRouteName(),'croaziere') ? 'curent__item_red' : '' !!}" id="button7">
                                    <a href="{{ url('/croaziere') }}" class="list-group-item">Croaziere</a>
                                </li>

                 

                                <li class="diverse">
                                  <div  href="" class="dropdown">
                                  <button class="btn-inner-menu dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                   <a href="#"> Diverse</a>
                                    <span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    @if(isset($types))
                                        @foreach($types as $type)
                                          @if ($type->id > 7)
                                                <li><a href="/{!!str_replace(' ', '-', strtolower($type->name))!!}">{!!$type->name!!}</a></li>
                                          @endif                                   
                                        @endforeach
                                    @endif

                                   </ul>
                            
                                </div>
                               </li> 
                            </ul> 
                        </div>
                    </div>
                </ul>
            </div> 

                </nav>
            </div>
        </div>
    </div> 
    <div class="gradient"></div>
</div>
       


