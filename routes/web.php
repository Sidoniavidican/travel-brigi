<?php
	
use App\Type;
use App\Offer;

$types = Type::all();

foreach ($types as $type) {
	$route = str_slug($type->name);
	$id = $type->id;

	Route::get($route, function () use ($id, $types) {
	    $offers = Offer::where('type', 'LIKE', '%,'.$id.',%')->get();
        $maxPrice = Offer::max('price');
        if($maxPrice < 2000)
            $maxPrice = 2000;
        return view('offer.offers', compact('offers', 'types', 'maxPrice'));
	});

	$routeAdmin = 'admin/'.$route;
	Route::get($routeAdmin, function () use ($id, $types) {
	    $offers = Offer::where('type', 'LIKE', '%,'.$id.',%')->get();
        return view('offer.offersAdmin', compact('offers', 'types'));
		})->middleware('admin');

	Route::get($route.'/{filtre}', 'HomeController@search')->name('filter')->name($route);
}


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* RUTE pt */


Route::get('logout', function(){
	Auth::logout();
	return redirect('/');
});

Auth::routes();

Route::get('contact', function () {
    return view('contact');
});
Route::get('/despre-noi', function () {
    return view('despre-noi'); 
});
Route::get('/tichete', function () {
	return view('tichete'); 
});
Route::get('/termeni-si-conditii', function () {
	return view('termeni-si-conditii'); 
});
Route::get('/confidentialitate', function () {
	return view('confidentialitate'); 
});

Route::get('/', 'HomeController@index')->name('/');

Route::post('sendform', 'FormsController@sendForm'); 
Route::post('sendformbilete', 'FormsController@sendFormBilete'); 
Route::post('sendformrequest', 'FormsController@sendFormRequest'); 

//home 
Route::get('oferta/{offer}', 'HomeController@showOffer');
Route::get('offer/{offer}', 'HomeController@showOffer');
Route::get('album/{album}', 'HomeController@showAlbum');
Route::get('transporturi', 'HomeController@showTransports');
Route::get('albume', 'HomeController@showAlbums');
Route::post('subscribe' , 'HomeController@subscribe')->name('subscribe');
Route::get('search', 'HomeController@search')->name('search');
Route::post('subscribe' , 'HomeController@subscribe')->name('subscribe');

//admin

//subscriber
Route::get('admin/subscribers', 'SubscriberController@index');
Route::delete('delete-subscriber/{subscriber}', 'SubscriberController@destroy')->name('delete-subscriber');
Route::post('edit-subscriber', 'SubscriberController@edit');

//newsletter
Route::post('getTypeOffers','NewsletterController@getTypeOffers')->name('getTypeOffers');
Route::post('sendCompaign','NewsletterController@sendCompaign')->name('sendCompaign');
Route::post('getOfferName','NewsletterController@getOfferName')->name('getOfferName');

//offer
Route::get('admin', 'OffersController@indexAdmin');
Route::get('admin/offers/expired', 'OffersController@old');

//offer photos
Route::post('store-photos', 'OfferPhotosController@store')->name('store-photos');
Route::delete('delete-photo/{photo}' , 'OfferPhotosController@destroy');
Route::delete('delete-offer-photo-temporary' , 'OfferPhotosController@destroyTemporary');

//album photos
Route::post('store-album-photo', 'AlbumPhotosController@store')->name('store-album-photo');
Route::delete('delete-album-photo/{photo}' , 'AlbumPhotosController@destroy');
Route::delete('delete-album-photo-temporary' , 'AlbumPhotosController@destroyTemporary');

//hotel photo
Route::post('store-hotel-photo', 'HotelsController@addHotelPhoto')->name('store-hotel-photo');
Route::delete('delete-hotel-photo-temporary', 'HotelsController@destroyTemporary');

//
Route::prefix('admin/')->group(function(){
	Route::resource('transports', 'TransportsController');
	Route::resource('albums', 'AlbumsController');
	Route::resource('hotels', 'HotelsController');
	Route::resource('offers', 'OffersController');

	Route::resource('periods', 'PeriodsController');
	Route::get('offers/{offer}/periods/create', 'PeriodsController@create');
	Route::get('offers/{offer}/periods/{period}/edit', 'PeriodsController@edit');

	Route::resource('offer-periods', 'OfferPeriodsController');
	Route::get('offers/{offer}/offer-periods/create', 'OfferPeriodsController@create');
	Route::get('offers/{offer}/offer-periods/{offer_period}/edit', 'OfferPeriodsController@edit');

	Route::resource('days', 'DaysController');
	Route::get('offers/{offer}/days/create', 'DaysController@create');
	Route::get('offers/{offer}/days/edit', 'DaysController@edit');

	Route::resource('newsletters', 'NewsletterController');
});
