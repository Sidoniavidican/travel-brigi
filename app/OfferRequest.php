<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferRequest extends Model
{
    //
    protected $fillable = [
        'offer_id', 'message', 'email', 'name', 'phone'
    ];

    public function offer()
    {
    	return $this->belongsTo(Offer::class);
	}
}
