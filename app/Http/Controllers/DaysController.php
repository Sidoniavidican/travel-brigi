<?php

namespace App\Http\Controllers;

use App\Offer;
use App\Day;
use Session;
use Illuminate\Http\Request;

class DaysController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function create(Offer $offer)
    {
        return view('offer.addDays', compact('offer'));
    }

    public function store(Request $request)
    {
        $offer = Offer::findorfail($request->offer_id);
        for($dayNumber = 1; $dayNumber <= $offer->days; $dayNumber++)
        {
            $newDay = new Day;
            $newDay->day = $dayNumber;
            $newDay->offer_id = $offer->id;
            $newDay->description = $request[$dayNumber];
            $newDay->save();
        }
        Session::flash('offer-success', "Zilele au fost adaugate cu success!");
        return redirect('admin/offers'); 
    }

    public function edit(Offer $offer)
    {
        return view('offer.addDays',compact('offer'));
    }

    public function update(Request $request)
    {
        $offer = Offer::findorfail($request->id); 
        for($dayNumber = 1; $dayNumber <= $offer->days; $dayNumber++)
        {
            $day = Day::where('offer_id', $offer->id)->where('day', $dayNumber)->first();
            $day->description = $request[$dayNumber];
            $day->save();
        }
        Session::flash('offer-success', "Zilele au fost modificate cu success!");
        return redirect('/admin/offers');
    }

    public function destroy(Request $request)
    {
        $offer = Offer::findorfail($request->id); 
        $offer->offerDays()->delete();
        Session::flash('offer-success', "Zilele au fost sterse cu success!");
        return redirect('/admin/offers'); 
    }
}
