<?php

namespace App\Http\Controllers;

use App\AlbumPhoto;
use App\Album;
use Carbon\Carbon;
use File;
use Storage;
use Image;
use Session;
use Illuminate\Http\Request;
use Facades\App\Services\StorePhotos;

class AlbumPhotosController extends Controller
{  
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function store(Request $request)
    {
        $file = $request->file('file'); 
        StorePhotos::store($file, 'gallery', $request->id);  
    } 

    public function destroyTemporary(Request $request)
    {
        File::cleanDirectory('images/gallery/temporary');
    }

    public function destroy(AlbumPhoto $photo)
    {     
        File::delete('images/gallery/'. $photo->album->id. '/'. $photo->path);
        $photo->delete();
    }
}
