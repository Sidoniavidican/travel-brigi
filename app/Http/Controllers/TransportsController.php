<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transport;
use App\Type;
use Session;

class TransportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function index()
    {
        $transports = Transport::all();
        return view('transport.transportsAdmin', compact('transports'));
    }
    public function create()
    {
        return view('transport.addTransport');
    }

    public function store(Request $request)
    {
        $tranport = Transport::create($request->all());
        Session::flash('offer-success', "Ai adaugat un tranport cu success!");
        return redirect('/admin/transports');
    }

    public function destroy(Transport $transport)
    {
        $transport->delete();
    }
}
