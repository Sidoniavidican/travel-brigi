<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\User;
use App\OfferRequest;
use App\FlyRequest;
use App\Notifications\Bilete;
use App\Notifications\Cerere; 

class FormsController extends Controller
{
   public function sendForm(Request $request){
        $form = $request->all();
            Mail::send('emails.contact', ['form' => $form], function ($m) use ($form) {  
            $m->from($form['email']);

            $m->to('daciana@glabit.com', 'Contact Form Travel')->subject('Mesaj Contact Page'); 
        });
        return redirect()->back()->with('success', true); 
    }
    public function sendFormBilete(Request $request){    
        $cerere = FlyRequest::create($request->all());  
        $form = $request->all();
        $user = User::first(); 
        $user->notify(new Bilete($request));

        return redirect()->back()->with('status', ''); 
    }
    public function sendFormCerere(Request $request){    

        $cerere = OfferRequest::create($request->all());  
        $form = $request->all();
        $user = User::first(); 
        $user->notify(new Cerere($request));

        return redirect()->back()->with('status', ''); 
    }
}
 