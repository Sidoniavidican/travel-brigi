<?php

namespace App\Http\Controllers;

use App\Period;
use App\Offer;
use Session;
use Illuminate\Http\Request;

class PeriodsController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function create(Offer $offer)
    {
        return view('offer.addPeriod', compact('offer'));
    }

    public function store(Request $request)
    {        
        $newPeriod = Period::create($request->all());
        Session::flash('offer-success', "Ai adaugat perioada cu success!");
        return redirect('/admin/offers'); 
    }

    public function edit(Offer $offer, Period $period)
    {
        return view('offer.editPeriod', compact('period'));
    }

    public function update(Request $request, Period $period)
    {
        $period->update($request->all());
        Session::flash('offer-success', "Ai editat perioada cu success!");
        return redirect('/admin/offers'); 
    }

    public function destroy(Period $period)
    {
        $period->delete();
        Session::flash('offer-success', "Ai sters perioada cu success!");
        return redirect('/admin/offers');
    }
}
