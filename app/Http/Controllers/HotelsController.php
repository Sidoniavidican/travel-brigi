<?php

namespace App\Http\Controllers;

use App\Hotel;
use File;
use Image;
use Session;
use Illuminate\Http\Request;

class HotelsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $hotels = Hotel::all();
        return view('hotel.hotels', compact('hotels'));
    }

    public function create()
    {
        return view('hotel.addHotel');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'confort' => 'required'
        ]);

        $newHotel = Hotel::create($request->all());

        $path = 'images/hotels/' . $newHotel->id;
        $files = File::allFiles('images/hotels/temporary');
        File::makeDirectory($path);
        foreach ($files as $file)
        {       
            $name = $file->getFilename();
            $newHotel->path = $name;
            $newHotel->save();
            Image::make($file)->save('images/hotels/' .$newHotel->id . '/'. $name);
        }
        File::cleanDirectory('images/hotels/temporary');
        Session::flash('offer-success', "Ai adaugat cu success un hotel!");
        return redirect('admin/hotels');
    }


    public function destroy(Hotel $hotel)
    {
        File::deleteDirectory('images/hotels/'. $hotel->id);
        $hotel->offerHotels()->delete();
        $hotel->delete();
    }

    public function addHotelPhoto(Request $request)
    {   
        $file = $request->file('file'); 
        $name = $file->getClientOriginalName();
        $img = Image::make($file);     
        $width = $img->width();
        $height = $img->height();
        if($width > 1400)
        $img->resize(1400, null, function ($constraint) {
            $constraint->aspectRatio();
        });      
        if($height > 1400)
        $img->resize(null, 1400, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save('images/hotels/temporary' . '/'. $name);
    }

    public function destroyTemporary(Request $request)
    {
        File::cleanDirectory('images/hotels/temporary');
    }
}
