<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Newsletter;
use Mailchimp;
use App\Subscriber;
use App\Newsletter as NewsletterBase;
use App\NewsletterOffer;
use App\Offer;
use App\Type;
use File;
use Storage;
use Carbon\Carbon;
use Image;
use Session;

class NewsletterController extends Controller
{ 
    public $api_key = 'cba2bdc6938891d2710c272026312bd8-us15';
    public $list = '3a03dcfd70';

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $newsletters = NewsletterBase::all();
        return view('subscriber.newsletters', compact('newsletters'));
    }

    public function show(NewsletterBase $newsletter)
    {
        return view('subscriber.newsletter', compact('newsletter'));
    }
    
    public function create()
    {
        $offers = Offer::all();
        $types = Type::all()->slice(1);
        return view('subscriber.addNewsletter', compact('offers', 'types'));
    }
 
    public function destroy(NewsletterBase $newsletter)
    {
        File::deleteDirectory('images/newsletters/'.$newsletter->id);
        $newsletter->newsletterOffers()->delete();
        $newsletter->delete();
    }

    public function getTypeOffers(Request $request)
    {
        if($request->ajax())
        {  
            $offers = Offer::where('type', 'LIKE', '%,'.$request->id.',%')->has('photos', '>', 0)->get();
            $typeOffers = [];
            foreach ($offers as $offer) 
            {
                array_push($typeOffers, $offer->id);
            }
            $typeOffers = implode(",", $typeOffers);
            return response()->json(['offers' => $typeOffers]);
        }        
    }

    public function getOfferName(Request $request)
    {
        $offer = Offer::findOrfail($request->id);
        return response()->json(['name' => $offer->name]);
    }

    public function addOfferToNewsletter(Offer $offer, $newsletterId)
    {
        $newsletterOffer = new NewsletterOffer;
        $newsletterOffer->offer_id = $offer->id;
        $newsletterOffer->name = $offer->name;

        $newsletterOffer->price = $offer->price;
        $newsletterOffer->begin_date = $offer->begin_date;
        $newsletterOffer->end_date = $offer->end_date;

        $newsletterOffer->currency = $offer->currency;
        $newsletterOffer->country = $offer->country;
        $newsletterOffer->newsletter_id = $newsletterId;
        $newsletterOffer->path = $offer->photos[0]->path;
        //for discount type and procent
        $now = Carbon::now()->toDateString();
        if($offer->EB_date >= $now && $offer->EB_date != null)
        {
            $newsletterOffer->discount_type = 2;
            $newsletterOffer->discount = $offer->EB_discount;
        }
        if($offer->LM_date <= $now && $offer->LM_date != null)
        {
            $newsletterOffer->discount_type = 1;
            $newsletterOffer->discount = $offer->LM_discount;
        }

        File::copy('images/offers/'. $offer->id. '/'. $offer->photos[0]->path, 'images/newsletters/'. $newsletterId. '/'. $offer->photos[0]->path);
        $newsletterOffer->save();
    }

    public function style()
    {
        return ' <img src = "http://www.brigittetravel.ro/images/offers/.'. $offer->id. '/'. $offer->photos[0]->path. '>.';
    }

    public function addofferToTemplate(Offer $offer)  
    {
        $ron = '';
        $euro = '';
        if($offer->currency == '1')
            $ron = 'RON';
        else
            $euro = '&#8364;';
        $begin_date = Carbon::parse($offer->begin_date)->format('d.m.Y');
        $end_date = Carbon::parse($offer->end_date)->format('d.m.Y');

        $now = Carbon::now()->toDateString();
        $discount = 0;
        $templateDiscount = '';
        $templatePrice = 
                        '<tr>
                            <td class="mcnTextContent" valign="top" style="padding:2px 9px 2px 9px;" width="164">
                                <span style="color:#F44336"> de la <strong style="font-size:16px;">'. $euro. $offer->price . ' '. $ron. '</strong> </span><br> 
                            </td>
                        </tr>';

        if($offer->EB_date >= $now && $offer->EB_date != null)
        {
            $discount_type = 'EARLY BOOKING';
            $discount = $offer->EB_discount;
            $newPrice = (int)($offer->price * (100 - $discount) /100);
        }
        if($offer->LM_date <= $now && $offer->LM_date != null)
        {
            $discount_type = 'LAST MINUTE';
            $discount = $offer->LM_discount;
            $newPrice = (int)($offer->price * (100 - $discount) /100);
        }

        if($discount > 0)
        {
            $templatePrice = ' 
              <table align="left" border="0" cellpadding="0" cellspacing="0" width="200" class="columnWrapper">  
                        <tr>
                            <td class="mcnTextContent" valign="top" style="padding:2px 9px 2px 9px;" width="164">
                                <span style=" color:#F44336"> de la 
                                    <strong style="font-size:16px;">'. 
                                         $euro. $newPrice . ' '. $ron. 
                                    '</strong> 
                                </span>
                                <span style="font-size:12px; color:#gray">
                                    <strike>'. $euro. $offer->price . ' '. $ron.  '</strike>
                                </span>
                                <br> 
                            </td>
                        </tr>';
            $templateDiscount = '     
                       <tr> 
                            <td align="left" valign="middle" class="mcnButtonContent" style="font-family: Arial; font-size: 16px;">
                                <a class="mcnButton " href="" style="font-weight:bold; margin-top: 5px; width:280px; display: inline-block; padding:10px; letter-spacing:normal; background:#F44336; line-height: 100%;   text-align: center; text-decoration: none; color: #FFFFFF;">'. 
                                  $discount_type.' -'. $discount .'%</a>
                            </td> 
                        </tr>
                    </table>';
        }

        $template ='
        <table align="left" border="0" cellpadding="0" cellspacing="0" width="200" class="columnWrapper">
            <tr>
                <td valign="top" class="columnContainer">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%"  class="mcnCaptionBlock">
                        <tbody class="mcnCaptionBlockOuter">
                            <tr>
                                <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">
                                    <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionBottomContent" width="false">
                                        <tbody>
                                            <tr>
                                                <td class="mcnCaptionBottomImageContent" align="center" valign="top" style="padding:0 9px 9px 9px;">  
                                                    <a href="http://www.brigittetravel.ro/offer/'.$offer->id .'">'.
                                                    $templateDiscount. '<br>
                                                        <img src = "http://brigittetravel.ro/images/offers/148/thailanda-1_1491989576_148.jpg" style="width:300px; height: 300px; ">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="mcnTextContent" valign="top" style="padding:2px 9px 2px 9px;" width="164">
                                                    <strong style="color:#F44336; font-size:18px;">'. $offer->name. '</strong><br>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="mcnTextContent" valign="top" style="padding:2px 9px 2px 9px;" width="164">
                                                    <span style="font-size:14px;">'. $begin_date. '-'. $end_date. '</span><br> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="mcnTextContent" valign="top" style="padding:2px 9px 2px 9px;" width="164">
                                                    <span style="font-size:14px;">'. str_replace(',', ', ', $offer->country). '</span><br>
                                                </td>
                                            </tr>'
                                            . 
                                                $templatePrice
                                            .'<tr>
                                                <td align="left" valign="middle" class="mcnButtonContent" style="font-family: Arial; display:block; margin-top:15px; font-size: 16px; padding:0 9px 0 9px;">
                                                    <a class="mcnButton " title="Vezi oferta" href="http://www.brigittetravel.ro/offer/'.$offer->id .'target="_blank" style="font-weight:bold; padding:15px; display: block; margin-bottom: 50px; width:120px; letter-spacing: normal; background:#F44336; line-height: 100%;text-align: center; text-decoration: none; color: #FFFFFF;">Vezi Oferta</a>
                                                </td> 
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>                                
            </tr>
        </table>
    ';
 return $template;
    }

    public function sendCompaign(Request $request)
    {
        $this->validate($request, [
            'subject' => 'required',
            'message' => 'required'
        ]);
        try 
        {
            $message = $request->message;
            $html = '<!DOCTYPE html>
                    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
                    <head>
                        <title>Brigitte Newsletter</title>
                        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
                        <meta charset="UTF-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="viewport" content="width=device-width, initial-scale=1">
                    </head>

                    <style type="text/css">
                    </style> 

                    <body>';
            $message = '<div class="text"><p>'. $message. '</p></div>';
            $html = $html. $message;
            $offers = $request->offers;
            $offers = explode(',', $offers);

            //for our database
            $newNewsletter = new NewsletterBase;
            $newNewsletter->name = $request->subject;
            $newNewsletter->message = $request->message;
            $newNewsletter->save();
            
            //
            $path = 'images/newsletters/'. $newNewsletter->id;
            File::makeDirectory($path);
            for($nrOffer =  0; $nrOffer < count($offers); $nrOffer++)
            {
                if($nrOffer != 0)
                {
                    $offer = Offer::findOrfail($offers[$nrOffer]);
                    //for our database
                    $make = $this->addOfferToNewsletter($offer, $newNewsletter->id);
                    //
                    $html = $html. $this->addofferToTemplate($offer);
                }    
            }
 
            $html = $html.  "</body></html>";
            $options = [
                'list_id'   => $this->list,
                'subject' => $request->subject,
                'from_name' => 'brigittetravel',
                'from_email' => 'brigittetravel@yahoo.com',
            ];

            $content = [
                'html' => $html,
                'text' => $request->message
            ];
            $this->mc = new Mailchimp($this->api_key, array('ssl_verifypeer' => false));
            $campaign = $this->mc->campaigns->create('regular', $options, $content);
            $this->mc->campaigns->send($campaign['id']);
            
            Session::flash('offer-success', "Ai adaugat cu success un newsletter!");
            return redirect('admin/newsletters');    
        } 
        catch (Exception $e) 
        {
            return redirect()->back()->with('error', 'Error from MailChimp');
        }
    }
}
