<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Offer;
use App\Type;
use App\Photo;
use App\Album;
use App\AlbumPhoto;
use App\Transport;
use App\Subscriber;
use Carbon\Carbon; 
use Illuminate\Support\Facades\DB;

use Newsletter;

class HomeController extends Controller
{

    public function isAccomodation($type)
    {
        if($type == '0')
            return true;
        return false;
    }
    public function index()
    {
        $offers = collect(new Offer);
        for($categorie = 1; $categorie < 9; $categorie++)
        {
            $offers2 = Offer::where('type', 'LIKE', '%,'. $categorie. ',%')
                        ->where(function ($query) use ($categorie) {
                            if($categorie == 1)
                                $query->where('LM_date', '<=', Carbon::now());
                            })
                        ->orderBy('important', 'DESC')
                        ->orderBy('created_at', 'DESC')
                        ->take(2)
                        ->get();
            $offers = $offers->merge($offers2)->unique('id');

            $nrOffers = 2 * $categorie + 2 - $offers->count();
            if($categorie == 8)
            {
                $nrOffers--;
            }
            if($offers->count() < (2*$categorie))
            {
                $offers2 = Offer::where('type', 'LIKE', '%,'. $categorie. ',%')
                            ->where(function ($query) use ($categorie) {
                                if($categorie == 1)
                                    $query->where('LM_date', '<=', Carbon::now());
                                })
                            ->orderBy('important', 'DESC')
                            ->orderBy('created_at', 'DESC')
                            ->take($nrOffers)
                            ->get();
                $offers = $offers->merge($offers2)->unique('id');
            }
        }

        $albums = Album::has('photos', '>', 2)->orderBy('updated_at', 'DESC')->take(2)->get();
        $types = Type::all();
        return view('home', compact('offers', 'types', 'albums'));
    }


    public function search(Request $request)
    {    
        $search = '%'.$request->search.'%';
        $period2 = '%'; 
        if(\Request::get('pret-minim'))
        {
            $price = \Request::get('pret-maxim');
            $minPrice = \Request::get('pret-minim');
            $duration = \Request::get('durata-maxima');;
            $minDuration = \Request::get('durata-minima');
            $type = $request->categorie;
            if($type == 'toate')
            {
                $type = '%';
            }
            $period = $request->perioada;
            if($period != 'toate')
            {   
                $month = $period;
                $period = Carbon::now()->addMonth($month+1)->day(1)->toDateString();
                $period2 = Carbon::now()->addMonth($month)->day(1)->toDateString();
            }
            else 
            {
                $period = '%';
            }
            $dinner = $request->masa;
            $transport = $request->transport;
            $stars = $request->stele;
            if($stars == 'toate')
            {
                $stars = '%';
            }
            else 
            {   
                $stars = $stars.'6';
                $stars = explode(',', $stars);       
            }   
        }
        else
        {
            $price = 20000;
            $minPrice = 5;
            $duration = 24;
            $minDuration = 1;
            $transport = '%';
            $dinner = '%';
            $period = '%';
            $type = '%';
            $stars = '%';
        }
        
        $value = DB::table('curencies')->first()->value;  

        if((!$this->isAccomodation($type)) || $type == '%')
        {
            $offers = Offer::where('type', 'LIKE', '%,'.$type.',%')
                    ->where(function ($query) use ($type) {
                                if($type == '1')
                                    $query->whereDate('LM_date', '<=', Carbon::now()->toDateString());
                                if($type == '2')
                                    $query->whereDate('EB_date', '>=', Carbon::now()->toDateString());
                            })
                    ->where(function ($query) use ($search) {
                                $query->where('name', 'LIKE', $search)
                                    ->orwhere('description', 'LIKE', $search)
                                    ->orwhere('country', 'LIKE', $search);
                                })
                     ->where(function ($q) use ($period, $period2) {
                            if($period != '%')
                                $q->where(function ($query) use ($period, $period2) {
                                        $query->whereDate('begin_date', '<=', $period)
                                              ->whereDate('end_date', '>=', $period2);
                                    })
                                  ->orwhereHas('offerPeriods', function ($query) use ($period, $period2) {
                                    $query->whereDate('begin_date', '<=', $period)
                                          ->whereDate('end_date', '>=', $period2);
                                    });
                            })                  
                    ->where(function ($query) use ($price, $value) {
                            $query->where('currency', '0')->where('price', '<=', $price)
                                  ->orwhere('currency', '1')->where('price', '<=', (int)($price*$value));
                            })
                    ->where(function ($query) use ($minPrice, $value) {
                            $query->where('currency', '0')->where('price', '>=', $minPrice)
                                  ->orwhere('currency', '1')->where('price', '>=', (int)($minPrice*$value));
                            })
                    ->where('days', '<=', $duration)  
                    ->where('days', '>=', $minDuration) 
                    ->where('dinner', 'LIKE', $dinner)                   
                    ->where(function ($query) use ($transport) {
                                if($transport == "1")
                                    $query->where('transport', '1')
                                         ->orwhere('transport', '2');          
                                if($transport == "0")
                                    $query->where('transport', '0');
                                })
                    ->where(function ($query) use ($stars) {
                                if($stars != '%')
                                    $query->whereIn('confort', $stars);    
                                })                            
                    ->get();
        }
        if($this->isAccomodation($type) || $type == '%')
        {
            $accomodations = Offer::where('type', 'LIKE', ','.$type.',')
                    ->has('periods', '>', 0)
                    ->where(function ($query) use ($search) {
                                $query->where('name', 'LIKE', $search)
                                    ->orwhere('description', 'LIKE', $search)
                                    ->orwhere('country', 'LIKE', '%'.$search.'%');
                                })
                    ->where(function ($q) use ($period, $period2) {
                                if($period != '%')
                                $q->whereHas('periods', function ($query) use ($period, $period2) {
                                    $query->whereDate('begin_date', '>=', $period)
                                          ->whereDate('end_date', '<=', $period2);
                                });
                            })
                    ->where(function ($q) use ($price, $value) {
                                $q->whereHas('periods', function ($query) use ($price, $value) {
                                    $query->where('currency', '0')->where('single', '<', $price)
                                          ->orwhere('currency', '1')->where('single', '<', (int)($price * $value));
                                });                           
                            }) 
                    ->where(function ($q) use ($minPrice, $value) {
                                $q->whereHas('periods', function ($query) use ($minPrice, $value) {
                                    $query->where('currency', '0')->where('single', '>', $minPrice)
                                          ->orwhere('currency', '1')->where('single', '>', (int)($minPrice * $value));
                                });                           
                            })  
                    ->where('dinner', 'LIKE', $dinner)                    
                    ->where(function ($query) use ($stars) {
                                if($stars != '%')
                                    $query->whereIn('confort', $stars);    
                                })                            
                    ->get();

        }

        $search = $request->search;
        if($this->isAccomodation($type))
            $offers = $accomodations;
        else if($type == '%' && $transport != '1')
            $offers = $offers->merge($accomodations);
       
        $maxPrice = Offer::max('price');
        if($maxPrice < 2000)
            $maxPrice = 2000;
        return view('offer.offers', compact('offers', 'search', 'maxPrice'));
    }


    public function subscribe(Request $request)
    {
        $subscriber = Subscriber::where('email', $request->email)->first();
        if($subscriber === null)
        {
            Newsletter::subscribe($request->email);
            $newSubscriber = Subscriber::create($request->all());
        }
        return back();   
    }

    public function showOffer(Offer $offer)
    {
        return view('offer.offer', compact('offer'));
    }

    public function showAlbum(Album $album)
    {
       return view('album.album', compact('album'));
    }

    public function showTransports()
    {
        $transports = Transport::all();
        return view('transport.transports', compact('transports'));
    }

    public function showAlbums()
    {
        $albums = Album::all();
        return view('album.albums', compact('albums'));
    } 
}
