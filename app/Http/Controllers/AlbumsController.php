<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AlbumPhoto;
use App\Album;
use Carbon\Carbon;
use File;
use Storage;
use Image;
use Session;

use Facades\App\Services\StorePhotos;

class AlbumsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $albums = Album::all();
        return view('album.albumsAdmin', compact('albums'));
    }

    public function create()
    {
        return view('album.addAlbum');
    }

    public function store(Request $request)
    {
        $this->validate($request, [   
            'name' => 'required'
        ]);
        $newAlbum = Album::create($request->all());
        StorePhotos::publish($newAlbum->id, 'gallery');
        Session::flash('offer-success', "Ai adaugat cu success un album!");
        return redirect('admin/albums');
    }

    public function edit(Album $album)
    {
        return view('album.editAlbum', compact('album'));
    } 

    public function update(Request $request, Album $album)
    {
        $this->validate($request, [   
            'name' => 'required'
        ]);
        $album->update($request->all());
        Session::flash('offer-success', "Ai modificat cu success un album!");
        return redirect('admin/albums');
    }

    public function destroy(Album $album)
    {
        File::deleteDirectory('images/gallery/'.$album->id);   
        $album->photos()->delete();   
        $album->delete();
    }


}
