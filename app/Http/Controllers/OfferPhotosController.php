<?php

namespace App\Http\Controllers;

use App\Photo;
use App\Offer;
use Carbon\Carbon;
use File;
use Image;
use Session;
use Illuminate\Http\Request;
use Facades\App\Services\StorePhotos;


class OfferPhotosController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function store(Request $request)
    {
        $file = $request->file('file'); 
        StorePhotos::store($file, 'offers', $request->id);      
    }

    public function destroy(Photo $photo)
    {
        File::delete('images/offers/'.$photo->offer->id.'/'.$photo->path);
        $photo->delete();      
    }

    public function destroyTemporary(Request $request)
    {
        File::cleanDirectory('images/offers/temporary');
    }
}
