<?php

namespace App\Http\Controllers;

use App\OfferPeriod;
use App\Offer;
use Session;
use Illuminate\Http\Request;

class OfferPeriodsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function create(Offer $offer)
    {
        return view('offer.addOfferPeriod', compact('offer'));
    }

    public function store(Request $request)
    {
        $newPeriod = OfferPeriod::create($request->all());
        Session::flash('offer-success', "Ai adaugat perioada cu success!");
        return redirect('/admin/offers'); 
    }

    public function edit(Offer $offer, OfferPeriod $offer_period)
    {
        return view('offer.editOfferPeriod',compact('offer_period'));
    }

    public function update(Request $request, OfferPeriod $offer_period)
    {
        $offer_period->update($request->all());
        Session::flash('offer-success', "Ai editat perioada cu success!");
        return redirect('/admin/offers'); 
    }

    public function destroy(Request $request, OfferPeriod $offer_period)
    {
        $offer_period->delete();
        Session::flash('offer-success', "Ai sters perioada cu success!");
        return redirect('/admin/offers');
    }
}
