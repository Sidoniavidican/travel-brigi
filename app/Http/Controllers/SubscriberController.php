<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Subscriber;
use Newsletter;
use Image;
use Session;
use Mailchimp;

class SubscriberController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $subscribers = Subscriber::all();
    	return view('subscriber.subscribers', compact('subscribers'));
    }

    public function destroy(Subscriber $subscriber)
    {
        Newsletter::unsubscribe($subscriber->email);
        Newsletter::delete($subscriber->email);
        $subscriber->delete();
        return back();
    }

    public function edit(Request $request)
    {
        $subscriber = Subscriber::findorfail($request->pk) -> update([$request->name => $request->value]);
    }

    
}
