<?php

namespace App\Http\Controllers;

use App\Offer;
use App\Photo;
use App\Day;
use App\Type;
use App\Hotel;
use App\OfferHotel;
use App\Period;
use App\Subscriber;
use App\OfferRequest;
use App\FlyRequest;
use File;
use Storage;
use Validator;
use Session;
use Carbon\Carbon;

use Mailchimp;
use Illuminate\Support\Facades\DB;

use Image;
use Illuminate\Http\Request;

use Facades\App\Services\StorePhotos;

class OffersController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $offers = Offer::orderBy('updated_at', 'DESC')->get();
        $types = Type::all();
        return view('offer.offersAdmin', compact('offers', 'types'));
    }

    public function indexAdmin()
    {
        $offersNr = Offer::count();
        $subscribers = Subscriber::count();
        $newSubscribers = Subscriber::where('created_at',  '>', Carbon::now()->subDays(1))->count();

        $offerRequests = OfferRequest::count();
        $newOfferRequests = OfferRequest::where('created_at',  '>', Carbon::now()->subDays(1))->count();

        $flyRequests = FlyRequest::count();
        $newFlyRequests = FlyRequest::where('created_at',  '>', Carbon::now()->subDays(1))->count(); 

        $oldOffers =  Offer::where(function ($q){
                            $q->whereHas('periods', function ($query){
                                    $query->whereDate('begin_date', '<', Carbon::now());                                      
                                })
                                ->orWhereHas('offerPeriods', function ($query){
                                    $query->whereDate('begin_date', '<', Carbon::now());                                      
                                });
                            })
                        ->orWhereDate('begin_date', '<', Carbon::now())
                        ->count();
        $bestOffersRequest = Offer::with('requests')->get()->sortBy(function($offer)
            {
                return $offer->requests->count();
            })->reverse()->take(4);

        return view ('homeAdmin', compact('offersNr', 'oldOffers', 'subscribers', 'newSubscribers', 'offerRequests' , 'newOfferRequests', 'flyRequests', 'newFlyRequests', 'bestOffersRequest'));
    }

    public function isAccomodation(Offer $offer)
    {
        if($offer->type == ",0,")
            return true;
        return false;
    }

    public function show($id)
    {
        $now = Carbon::now();
        $offer = Offer::findOrFail($id);
        return view('offer.offer', compact('offer'));
    }

    public function old()
    {
        $types = Type::all();
        $offers =  Offer::where(function ($q){
                             $q->whereHas('periods', function ($query){
                                    $query->whereDate('begin_date', '<', Carbon::now());                                      
                                })
                                ->orWhereHas('offerPeriods', function ($query){
                                    $query->whereDate('begin_date', '<', Carbon::now());                                      
                                });
                            })
                        ->orWhereDate('begin_date', '<', Carbon::now())
                        ->get();

        return view('offer.offersAdmin', compact('offers', 'types'));
    }
    
    public function create()
    {
        $types = Type::all();
        $hotels = Hotel::all();
        $countries = DB::table('countries')->get(); 
        return view('offer.addOffer', compact('types', 'countries', 'hotels'));
    }

    public function store(Request $request)
    {
        if($request->type[0] != '0')
        {
            $this->validate($request, [
                'name' => 'required',
                'price' => 'required|integer',
                'country' =>'required',
                'type' => 'required',
                'begin_date' => 'required',
                'end_date' => 'required',
                'transport' =>'required',
                'dinner' => 'required',
                'days' => 'required'
            ]);
        }
        else
        {
            $this->validate($request, [
                'type' => 'required',
                'name' => 'required',
                'country' => 'required',
                'dinner' => 'required'
            ]);
        }
        $newInfos = $request->all();   

        $typeOffer = '';
        foreach ($request->type as $type) 
        {
            $typeOffer = $typeOffer . ','. $type;
        }
        $newInfos['type'] = str_finish($typeOffer, ',');

        $countryOffer = '';
        foreach ($request->country as $country) 
        {
            $countryOffer = $countryOffer . ','. $country;
        }
        $newInfos['country'] = substr($countryOffer, 1, strlen($countryOffer));

        $newOffer = Offer::create($newInfos);

        StorePhotos::publish($newOffer->id, 'offers');

        $id = $newOffer->id;
        if($request->hotel != null)
        {
            foreach ($request->hotel as $hotel) 
            {
                $newOfferHotel = new OfferHotel;
                $newOfferHotel->offer_id = $id;
                $newOfferHotel->hotel_id = $hotel;
                $newOfferHotel->save();
            }
        }

        Session::flash('offer-success', "Ai adaugat cu success o oferta!");
        if($this->isAccomodation($newOffer))
            return redirect('admin/offers/'.$id.'/periods/create');
        else 
            return redirect('admin/offers/'.$id.'/days/create');
    }


    public function destroy(Offer $offer)
    {      
        File::deleteDirectory('images/offers/'.$offer->id);
        $offer->photos()->delete();
        $offer->offerDays()->delete();
        $offer->periods()->delete();
        $offer->offerPeriods()->delete();
        $offer->offerHotels()->delete();
        $offer->delete();
    }

    public function edit(Offer $offer)
    {
        $types = Type::all();
        $countries = DB::table('countries')->get(); 
        $selectedCountries = explode(",", $offer->country);
        $offerCountries ='';
        foreach ($selectedCountries as $country) 
        {
            $offerCountries  = $offerCountries. ','. DB::table('countries')->where('country', $country)->first()->id;
        }
        $offerCountries = $offerCountries. ',';
        
        $offerHotels = '';
        foreach($offer->offerHotels as $hotel)
        {
            $offerHotels = $offerHotels. ','. $hotel->hotel_id;
        }
        $offerHotels = $offerHotels.',';
        $hotels = Hotel::all();
        return view ('offer.editOffer', compact('offer', 'types', 'countries', 'offerCountries', 'hotels' , 'offerHotels'));
    }
    
    public function editDiscount(Offer $offer)
    {
        if(!str_contains($offer->type, ',1,'))
        {
            $offer->LM_date = null;
            $offer->LM_discount = null;
            $offer->save();
        }

        if(!str_contains($offer->type, ',2,'))
        {
            $offer->EB_date = null;
            $offer->EB_discount = null;
            $offer->save();
        }
    }

    public function update(Offer $offer, Request $request)
    {     
        if(!$this->isAccomodation($offer))
        {
            $this->validate($request, [
                'name' =>  'required',
                'price' => 'required|integer',
                'country' =>'required',
                'type' => 'required',
                'begin_date' => 'required',
                'end_date' => 'required',
                'transport' =>'required',
                'dinner' => 'required'
            ]);
        }
        else
        {
            $this->validate($request, [
                'name' => 'required',
                'country' =>'required',
                'dinner' => 'required'
            ]);
        }

        $newInfos = $request->all(); 
        if(!$this->isAccomodation($offer)) 
        {        
            $typeOffer = '';
            foreach ($request->type as $typeValue) {
                   $typeOffer = $typeOffer . ','. $typeValue;
            }
            $newInfos['type'] = str_finish($typeOffer, ',');
            $days = $request->days;
            if($offer->days != $days)
                $offer->offerDays()->delete();
        }
        else 
        {
            $newInfos['type'] = ',0,';
        }

        $countryOffer = '';
        foreach ($request->country as $country) 
        {
            $countryOffer = $countryOffer . ','. $country;
        }
        $newInfos['country'] = substr($countryOffer, 1, strlen($countryOffer));

        $offer->update($newInfos); 
        $editDisocunt = $this->editDiscount($offer);
        $offer->offerHotels()->delete();

        if($request->hotel != null)
        {
            foreach ($request->hotel as $hotel) 
            {
                $newOfferHotel = new OfferHotel;
                $newOfferHotel->offer_id = $request->id;
                $newOfferHotel->hotel_id = $hotel;
                $newOfferHotel->save();
            }
        }
        Session::flash('offer-success', "Ai edit cu success!");
        return redirect('/admin/offers');
    }


    public function getUniqueName(Request $request)
    {
        if($request->ajax())
        {  
            $offer = Offer::where('name', 'LIKE', $request->name)->first();
            if ($offer == null)
                $exist = 0;
            else
                $exist = 1;
            return response()->json(['exist' => $exist]);
        }        
    }


}
