<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    //
    protected $fillable = [
         'offer_id', 'begin_date', 'end_date', 'single', 'double', 'twin', 'triple', 'currency'
    ];
    public function offer()
    {
    	return $this->belongsTo(Offer::class);
	}
}
