<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsletterOffer extends Model
{
    //
    protected $fillable = [
        'name', 'newsletter_id', 'price', 'country', 'begin_date', 'end_date', 'path', 'discount_type', 'discount'
    ];

    public function newsletter()
    {
        return $this->belongsTo(Newsletter::class);
    }
}
