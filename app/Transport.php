<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transport extends Model
{
    //
    protected $fillable = [
        'from', 'to', 'date', 'price', 'description', 'hour'
    ];
}
