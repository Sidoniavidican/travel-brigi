<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferHotel extends Model
{
    
    public function offer()
    {
    	return $this->belongsTo(Offer::class);
	}

	public function hotel()
    {
    	return $this->belongsTo(hotel::class);
	}
}
