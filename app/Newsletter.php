<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    public function newsletterOffers()
    {
        return $this->hasMany(NewsletterOffer::class);
    }
}
