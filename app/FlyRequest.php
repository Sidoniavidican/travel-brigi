<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlyRequest extends Model
{
    //   
    protected $fillable = [
        'message', 'email', 'name', 'phone'
    ];
}
