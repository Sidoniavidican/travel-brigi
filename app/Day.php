<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    //
    public function offer()
    {
    	return $this->belongsTo(Offer::class);
	}

}
