<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferPeriod extends Model
{
    //
    protected $fillable = [
         'offer_id', 'begin_date', 'end_date', 'price', 'currency'
    ];
    public function offer()
    {
    	return $this->belongsTo(Offer::class);
	}
}
