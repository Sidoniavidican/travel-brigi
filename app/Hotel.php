<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    //
    protected $fillable = [
         'name', 'confort'
    ];

    public function offerHotels()
    {
        return $this->hasMany(OfferHotel::class);
    }

}
