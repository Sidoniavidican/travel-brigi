<?php

namespace App\Services;

use App\Photo;
use App\AlbumPhoto;
use App\ALbum;
use App\Offer;

use File;
use Validator;
use Session;
use Carbon\Carbon;
use Image;
use Illuminate\Http\Request;

class StorePhotos 
{
	public function publish($id, $type)
    {
        $now = Carbon::now()->timestamp;
        $path = 'images/'.$type.'/' . $id;
        $files = File::allFiles('images/'.$type.'/temporary');
        File::makeDirectory($path);
        foreach ($files as $file)
        {
        	if($type == "offers") 
        	{
        		$newPhoto = new Photo;
            	$newPhoto->offer_id = $id;
        	}
        	else
        	{
        		$newPhoto = new AlbumPhoto;
            	$newPhoto->album_id = $id;
        	}
            $name = $file->getFilename();
            $name = str_replace('.', '_'.$now.'_'.$id.'.', $name);
            $newPhoto->path = $name;
            $newPhoto->save();
            Image::make($file)->save('images/'.$type.'/' .$id . '/'. $name);
        }
        File::cleanDirectory('images/'.$type.'/temporary');
    }

    public function store($file, $type, $id)
    {
        $now = Carbon::now()->timestamp;
        $name = $file->getClientOriginalName();
  
        if( $id != 'temporary')
        {
            $name = str_replace('.', '_'. $now. '_'. $id. '.', $name);
            if($type == 'gallery')
            {
                $album = ALbum::findorfail($id);
                $newPhoto = new AlbumPhoto;
                $newPhoto->path = $name;
                $album->photos()->save($newPhoto);
            }
            elseif($type == 'offers')
            {
                $offer = Offer::findorfail($id);
                $newPhoto = new Photo;
                $newPhoto->path = $name;
                $offer->photos()->save($newPhoto);
            }
        }
      
        $img = Image::make($file);

        $width = $img->width();
        $height = $img->height();
        if($width > 1400)
        {
            $img->resize(1400, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        } 
        if($height > 1400)
        {
            $img->resize(null, 1400, function ($constraint) {
              $constraint->aspectRatio();
            });
        }      
        $img->save('images/'. $type .'/'. $id . '/'. $name);
    }
}