<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    //
    protected $fillable = [
        'name', 'description', 'price', 'country', 'observation', 'type', 'transport', 'dinner', 'begin_date', 'end_date', 'days', 'includes', 'notIncludes', 'confort', 'important', 'passport', 'from', 'currency', 'EB_discount', 'EB_date', 'LM_discount', 'LM_date'
    ];
    public function photos()
    {
        return $this->hasMany(Photo::class);
    }
    
    public function offerDays()
    {
        return $this->hasMany(Day::class);
    }

    public function periods()
    {
        return $this->hasMany(Period::class);
    }
    public function offerPeriods()
    {
        return $this->hasMany(OfferPeriod::class);
    }
    public function requests()
    {
        return $this->hasMany(OfferRequest::class);
    }

    public function offerHotels()
    {
        return $this->hasMany(OfferHotel::class);
    }
    

}
