<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accomodation extends Model
{
    //
    protected $fillable = [
        'name', 'description', 'country', 'city', 'dinner', 
    ];
}
